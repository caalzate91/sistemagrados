--Configs:
SET DATABASE SQL SYNTAX ORA TRUE

--SEQUENCES
CREATE SEQUENCE SEQ_CONSECUTIVO_PARAMETRO INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONS_PROGRAMA_ACADEMICO INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONS_ASIGNATURA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONSECUTIVO_ACTA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONSECUTIVO_DETALLE_ACTA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONSECUTIVO_APROBACION INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONSECUTIVO_TIPO_ACTA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONSECUTIVO_ESTADO_ACTA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONSECUTIVO_TIPO_DT_ACTA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONS_HISTORICO_APROBACION INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONS_HISTORICO_DT_ACTA INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_CONS_HISTORICO_ACTA INCREMENT BY 1 START WITH 1;
--SECUENCES
CREATE TABLE HMA_PARAMETRO (CONSECUTIVO NUMERIC primary key,
							CLAVE varchar(70) NOT NULL, 
							VALOR varchar(2000) NOT NULL, 
							DESCRIPCION varchar(2000),
							ORDENAMIENTO NUMERIC(10,0),
							ESTADO varchar(10));
							

CREATE TABLE HMA_PROGRAMA (CONSECUTIVO INT NOT NULL, CODIGO_SNIES VARCHAR(500) NOT NULL, DESCRIPCION VARCHAR(2000),
					   ESTADO VARCHAR(50), UNIVERSIDAD_EXT_ORG_ID VARCHAR(200), UNIVERSIDAD_EFFDT DATE,
					   PRIMARY KEY (CONSECUTIVO) );
					   
INSERT INTO HMA_PROGRAMA VALUES(1,'3eaq4','calculo 1','ACTIVO','1',TO_DATE('02-12-2014', 'DD-MM-YY'));
INSERT INTO HMA_PROGRAMA VALUES(2,'3eaq4','calculo 2','ACTIVO','1',TO_DATE('02-12-2014', 'DD-MM-YY'));


CREATE TABLE HMA_ASIGNATURA_UNIVERSIDAD (CONSECUTIVO INT NOT NULL, CONS_PROGRAMA INT NOT NULL, NOMBRE VARCHAR(100),
									CODIGO_SNIES VARCHAR(500), CREDITOS INT NOT NULL, UNIVERSIDAD_EXT_ORG_ID INT NOT NULL,
									UNIVERSIDAD_EFFDT DATE, ESTADO VARCHAR(100), PRIMARY KEY(CONSECUTIVO));

INSERT INTO HMA_ASIGNATURA_UNIVERSIDAD VALUES(1,1,'matematicas','13rRp',2,1,TO_DATE('02-12-2014', 'DD-MM-YY'),'ACTIVO');
INSERT INTO HMA_ASIGNATURA_UNIVERSIDAD VALUES(2,1,'religion','13rRp',2,1,TO_DATE('02-12-2014', 'DD-MM-YY'),'ACTIVO');


CREATE TABLE HMA_UNIVERSIDAD(EXT_ORG_ID VARCHAR(21), EFFDT DATE, NOMBRE VARCHAR(200),PRIMARY KEY(EXT_ORG_ID,EFFDT));

INSERT INTO HMA_UNIVERSIDAD VALUES('1',TO_DATE('02-12-2014', 'DD-MM-YY'),'UNIVERSIDAD DEL VALLE');

CREATE TABLE HMA_ACTA(CONSECUTIVO numeric(10) NOT NULL, NUMERO_ACTA varchar(12) NOT NULL, EMPLID varchar(11) NOT NULL, 
FECHA date NOT NULL, REALIZADA_POR varchar(11) NOT NULL, UBICACION_SEMESTRAL numeric(2) NOT NULL, 
TIPO_ACTA numeric NOT NULL, ESTADO_ACTA numeric NOT NULL, PROGRAMA_ACAD_PROG varchar(5) NOT NULL, 
PROGRAMA_EFFDT date NOT NULL, PROGRAMA_ORIGEN_ACAD_PROG varchar(5), PROGRAMA_ORIGEN_EFFDT date, 
PROGRAMA_ORIGEN_EXTERNO numeric, PERIODO_ACAD_CAREER varchar(5) NOT NULL, PERIODO_STRM varchar(4) NOT NULL, 
PERIODO_INSTITUTION varchar(5) NOT NULL, PRIMARY KEY (CONSECUTIVO));

CREATE TABLE HMA_DETALLE_ACTA (
  CONSECUTIVO               numeric(20) NOT NULL, 
  FECHA                     date NOT NULL, 
  VALOR                     varchar(100), 
  PASA_A_PEOPLE             varchar(1), 
  QUIEN_AUTORIZA            varchar(11) NOT NULL, 
  CREDITOS_APROBADOS        numeric(20) NOT NULL, 
  NOTA                      numeric(6, 3) NOT NULL, 
  CONS_ACTA                 numeric(20) NOT NULL, 
  TIPO                      numeric(20) NOT NULL, 
  COMENTARIO                varchar(2000) NOT NULL, 
  ASIGNATURA_CRSE_ID        varchar(6) NOT NULL, 
  ASIGNATURA_EFFDT          date NOT NULL, 
  ASIGNATURA_ORIGEN_CRSE_ID varchar(6), 
  ASIGNATURA_ORIGEN_EFFDT   date, 
  ASIGNATURA_ORIGEN_EXTERNA numeric(20), 
  PRIMARY KEY (CONSECUTIVO));
  
CREATE TABLE HMA_APROBACION (
  CONSECUTIVO numeric(20) NOT NULL, 
  ACTA        numeric(20) NOT NULL, 
  FECHA       date NOT NULL, 
  EMPLID      varchar(11) NOT NULL, 
  ROL_EMPLID  varchar(30) NOT NULL, 
  ESTADO      varchar(30) NOT NULL, 
  COMENTARIO  varchar(2000), 
  NOTIFICAR_A varchar(11) NOT NULL, 
  TOKEN       varchar(20) NOT NULL, 
  PRIMARY KEY (CONSECUTIVO));
  
CREATE TABLE HMA_CORREO_TOKEN (
  TOKEN        varchar(20) NOT NULL, 
  EMPLID       varchar(11) NOT NULL, 
  FECHA_CREA   date NOT NULL, 
  FECHA_EXPIRA date NOT NULL, 
  PRIMARY KEY (TOKEN));
  
CREATE TABLE HMA_TIPO_ACTA (
  CONSECUTIVO numeric(20) NOT NULL, 
  DESCRIPCION varchar(200) NOT NULL, 
  PRIMARY KEY (CONSECUTIVO));
  
CREATE TABLE HMA_ESTADO_ACTA (
  CONSECUTIVO numeric(20) NOT NULL, 
  DESCRIPCION varchar(200) NOT NULL, 
  PRIMARY KEY (CONSECUTIVO));
  
CREATE TABLE HMA_TIPO_DETALLE_ACTA (
  CONSECUTIVO numeric(20) NOT NULL, 
  DESCRIPCION varchar(200) NOT NULL, 
  PRIMARY KEY (CONSECUTIVO));
  
CREATE TABLE HMA_HISTORICO_APROBACION (
  CONSECUTIVO_HISTORICO number(20) NOT NULL, 
  CONSECUTIVO           number(20) NOT NULL, 
  ACTA                  number(20) NOT NULL, 
  FECHA                 date NOT NULL, 
  EMPLID                varchar2(11) NOT NULL, 
  ROL_EMPLID            varchar2(30) NOT NULL, 
  ESTADO                varchar2(30) NOT NULL, 
  COMENTARIO            varchar2(2000), 
  NOTIFICAR_A           varchar2(11) NOT NULL, 
  TOKEN                 varchar2(20) NOT NULL, 
  PRIMARY KEY (CONSECUTIVO_HISTORICO));
  
CREATE TABLE HMA_HISTORICO_DETALLE_ACTA (
  CONSECUTIVO_HISTORICO     number(20) NOT NULL, 
  CONSECUTIVO               number(20) NOT NULL, 
  FECHA                     date NOT NULL, 
  VALOR                     varchar2(100), 
  PASA_A_PEOPLE             varchar2(1), 
  QUIEN_AUTORIZA            varchar2(11) NOT NULL, 
  CREDITOS_APROBADOS        number(20) NOT NULL, 
  NOTA                      number(6, 3) NOT NULL, 
  CONS_ACTA                 number(20) NOT NULL, 
  TIPO                      number(20) NOT NULL, 
  COMENTARIO                varchar2(2000) NOT NULL, 
  ASIGNATURA_CRSE_ID        varchar2(6) NOT NULL, 
  ASIGNATURA_EFFDT          date NOT NULL, 
  ASIGNATURA_ORIGEN_CRSE_ID varchar2(6), 
  ASIGNATURA_ORIGEN_EFFDT   date, 
  ASIGNATURA_ORIGEN_EXTERNA number(20), 
  PRIMARY KEY (CONSECUTIVO_HISTORICO));
  
CREATE TABLE HMA_HISTORICO_ACTA (
  CONSECUTIVO_HISTORICO     number(20) NOT NULL, 
  CONSECUTIVO               number(20) NOT NULL, 
  NUMERO_ACTA               varchar2(12) NOT NULL, 
  EMPLID                    varchar2(11) NOT NULL, 
  FECHA                     date NOT NULL, 
  REALIZADA_POR             varchar2(11) NOT NULL, 
  UBICACION_SEMESTRAL       number(19) NOT NULL, 
  TIPO_ACTA                 number(20) NOT NULL, 
  ESTADO_ACTA               number(20) NOT NULL, 
  PROGRAMA_ACAD_PROG         varchar2(5) NOT NULL, 
  PROGRAMA_EFFDT             date NOT NULL, 
  PROGRAMA_ORIGEN_ACAD_PROG varchar2(5), 
  PROGRAMA_ORIGEN_EFFDT     date, 
  PROGRAMA_ORIGEN_EXTERNO   number(20), 
  PERIODO_ACAD_CAREER       varchar2(5) NOT NULL, 
  PERIODO_STRM              varchar2(4) NOT NULL, 
  PERIODO_INSTITUTION       varchar2(5) NOT NULL, 
  PRIMARY KEY (CONSECUTIVO_HISTORICO));
  
CREATE TABLE HMA_ASIGNATURA_INTERNA(
	CRSE_ID VARCHAR(6),
	EFFDT date,
	DESCRIPCION VARCHAR(200),
	CREDITOS NUMBER
		
)

CREATE TABLE HMA_PROGRAMA_INTERNO(
	ACAD_PROG VARCHAR(2),
	EFFDT DATE,
	DESCR VARCHAR(30),
	ACAD_CAREER VARCHAR(4),
	FACULTAD VARCHAR(5),
	DESCR_FACULTAD VARCHAR(50)
)

  
ALTER TABLE HMA_APROBACION ADD CONSTRAINT FK_ACTA_CONSECUTIVO_APROBACION FOREIGN KEY (ACTA) REFERENCES HMA_ACTA (CONSECUTIVO);
ALTER TABLE HMA_DETALLE_ACTA ADD CONSTRAINT FK_ACTA_DETALLE_ACTA FOREIGN KEY (CONS_ACTA) REFERENCES HMA_ACTA (CONSECUTIVO);
ALTER TABLE HMA_APROBACION ADD CONSTRAINT FK_CORREO_TOKEN_APROBACION FOREIGN KEY (TOKEN) REFERENCES HMA_CORREO_TOKEN (TOKEN);
ALTER TABLE HMA_ASIGNATURA_UNIVERSIDAD ADD CONSTRAINT FK_CONS_PROGRAMA_ASIGNATURA FOREIGN KEY (CONS_PROGRAMA) REFERENCES HMA_PROGRAMA (CONSECUTIVO);
ALTER TABLE HMA_ACTA ADD CONSTRAINT FK_TIPO_ACTA FOREIGN KEY (TIPO_ACTA) REFERENCES HMA_TIPO_ACTA (CONSECUTIVO);
ALTER TABLE HMA_ACTA ADD CONSTRAINT FK_ESTADO_ACTA FOREIGN KEY (ESTADO_ACTA) REFERENCES HMA_ESTADO_ACTA (CONSECUTIVO);
ALTER TABLE HMA_DETALLE_ACTA ADD CONSTRAINT FK_TIPO_DETALLE_ACTA FOREIGN KEY (TIPO) REFERENCES HMA_TIPO_DETALLE_ACTA (CONSECUTIVO);
ALTER TABLE HMA_DETALLE_ACTA ADD CONSTRAINT FK_ASIG_ORI_EXT_DETALLE_ACTA FOREIGN KEY (ASIGNATURA_ORIGEN_EXTERNA) REFERENCES HMA_ASIGNATURA_UNIVERSIDAD (CONSECUTIVO);
ALTER TABLE HMA_ACTA ADD CONSTRAINT FK_PROGRAMA_ORIGEN_EXT_ACTA FOREIGN KEY (PROGRAMA_ORIGEN_EXTERNO) REFERENCES HMA_PROGRAMA (CONSECUTIVO);
ALTER TABLE HMA_HISTORICO_APROBACION ADD CONSTRAINT FK_CONS_APROBACION_HISTORICO FOREIGN KEY (CONSECUTIVO) REFERENCES HMA_APROBACION (CONSECUTIVO);
ALTER TABLE HMA_HISTORICO_DETALLE_ACTA ADD CONSTRAINT FK_CONS_DETALLE_ACTA_HISTORICO FOREIGN KEY (CONSECUTIVO) REFERENCES HMA_DETALLE_ACTA (CONSECUTIVO);
ALTER TABLE HMA_HISTORICO_ACTA ADD CONSTRAINT FK_ACTA_CONSECUTIVO_HIST_ACTA FOREIGN KEY (CONSECUTIVO) REFERENCES HMA_ACTA (CONSECUTIVO);



INSERT INTO HMA_ESTADO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(1,'No Enviada');
INSERT INTO HMA_ESTADO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(2,'Pendiente Aprobaci�n');
INSERT INTO HMA_ESTADO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(3,'Cancelada');
INSERT INTO HMA_ESTADO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(4,'No Aprobada');
INSERT INTO HMA_ESTADO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(5,'Finalizada');

INSERT INTO HMA_TIPO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(1,'Interna');
INSERT INTO HMA_TIPO_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(2,'Externa');

INSERT INTO HMA_TIPO_DETALLE_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(1, 'Homologada');
INSERT INTO HMA_TIPO_DETALLE_ACTA(CONSECUTIVO, DESCRIPCION) VALUES(2, 'Reconocida');

INSERT INTO HMA_ACTA(CONSECUTIVO, NUMERO_ACTA, EMPLID, 
				FECHA, REALIZADA_POR, UBICACION_SEMESTRAL, 
				TIPO_ACTA, ESTADO_ACTA, 
				PROGRAMA_ACAD_PROG, PROGRAMA_EFFDT, 
				PROGRAMA_ORIGEN_ACAD_PROG, PROGRAMA_ORIGEN_EFFDT, 
				PROGRAMA_ORIGEN_EXTERNO, 
				PERIODO_ACAD_CAREER, PERIODO_STRM, PERIODO_INSTITUTION)
			VALUES(10923, 'AC009234', '0069017', TO_DATE('02-12-2014', 'DD-MM-YY'), '0014417',
			2, 1, 1, '70', TO_DATE('01-01-2000', 'DD-MM-YY'), 11001, TO_DATE('01-01-2000', 'DD-MM-YY'),
			2, 'UGRD', '0945', 'PUJCL');
			
insert into HMA_ASIGNATURA_INTERNA (CRSE_ID,EFFDT,DESCRIPCION,CREDITOS) values ('000292',to_date('01/01/80','DD/MM/RR'),'Politica Colombiana Contempora','2');
insert into HMA_ASIGNATURA_INTERNA (CRSE_ID,EFFDT,DESCRIPCION,CREDITOS) values ('000293',to_date('01/01/80','DD/MM/RR'),'Gobierno Local Y Participacion','2');
INSERT INTO HMA_DETALLE_ACTA(CONSECUTIVO,
							FECHA,VALOR,PASA_A_PEOPLE,
							QUIEN_AUTORIZA,CREDITOS_APROBADOS,
							NOTA,CONS_ACTA,TIPO,
							COMENTARIO,ASIGNATURA_CRSE_ID,
							ASIGNATURA_EFFDT,
							ASIGNATURA_ORIGEN_EFFDT,
							ASIGNATURA_ORIGEN_EXTERNA)
VALUES(1,TO_DATE('02-12-2014', 'DD-MM-YY'),'Valor','1','0004449',2,4.3,10923,1,'nada',1,TO_DATE('01-01-80', 'DD-MM-YY'),TO_DATE('01-01-80', 'DD-MM-YY'),1);

INSERT INTO HMA_PROGRAMA_INTERNO 
VALUES('70',TO_DATE('01-01-00', 'DD-MM-YY'),'Adminsitraci�n de Empresas','UGRD','CE','CIENCIAS ECON�MICAS Y ADMINISTRATIVAS');

--------------------------------------//------//--EMAIL ESTUDIANTE--------------------------------//
INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval, 
'EMAIL_ACTA_ESTUDIANTE_CABECERA_IMG',
'<meta charset="utf-8"><table align="center"><tr><td><img src="http://apps.premize.com/premize/public/assets/images/arriba.jpg" width="1000"></td></tr><tr><td>',
'Imagen de cabecera en el email',
1,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CABECERA_LUGAR_FECHA',
'<table><tr><td><font face="Corbel"> Santiago de Cali, :<P_EMAIL_FECHA></font></td></tr>',
'Lugar y fecha de envio del email',
2,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CABECERA_PREFIX_NAME',
'<tr><td ><font face="Corbel">Estimado :<P_EMAIL_NOMBRE_DESTINATARIO></font></td></tr><tr><td></td></tr>',
'Prefijo y nombre del destinatario',
3,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_MENSAJE',
'<tr><td><font face="Corbel"> <br><br><br>Le informamos
 que el proceso de autorizaci&oacute;n del acta solicitada ha comenzado, por favor verifique la correspondencia y gestione autorizaci&oacute;n de esta..</font></td></tr>',
'Mensaje principal del email',
4,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_LABEL_DATOS_ACTA',
'<tr><td><font face="Corbel">Estos son los datos b&aacute;sicos:</font></td></tr><tr><td>',
'Cabecera para mostrar los datos del acta',
5,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_ORDENABLE_NUMERO_ACTA',
'<table><tr><td><font face="Corbel"><b>Acta :</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_ACTA></font></td></tr>',
'Numero del acta a enviar en el email',
6,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_ORDENABLE_NOM_ESTUDIANTE',
'<tr><td><font face="Corbel"><b> Estudiante :</b></font></td><td><font face="Corbel"> :<P_EMAIL_TEXT_ESTUDIANTE></font></td></tr>',
'Estudiante asociado al Acta',
7,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_ORDENABLE_UNIVERSIDAD',
'<tr><td><font face="Corbel"><b>Universidad de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_UNIVERSIDAD></font></td></tr>',
'Universidad de procedencia del estudiante asociada al acta',
8,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_ORDENABLE_PROGRAMA_PROC',
'<tr><td><font face="Corbel"><b>Programa de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PROC></font></td></tr>',
'Programa de procedencia del estudiante asociado al acta',
9,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_ORDENABLE_PROGRAMA_PUJ',
'<tr><td><font face="Corbel"><b>Programa PUJ:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PUJ></font></td></tr>',
'Programa de destino del estudiante asociado al acta',
10,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_ORDENABLE_UBIC_SEM',
'<tr><td><font face="Corbel"><b>Ubicaci&oacute;n Semestral:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_UBICACION_SEM></font></td></tr></table>',
'Ubicacion semestral del estudiante asociado al acta',
11,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_COMENTARIO',
'<tr><td></td></tr><tr><td><font face="Corbel"><b> Comentario : </b></font></td></tr><tr><td><font face="Corbel">:<P_EMAIL_CUERPO_COMENTARIO_TEXT> </font></td></tr>',
'Comentario del email de acta',
12,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_MENSAJE_2',
'<tr><td></td></tr><tr><td><font face="Corbel">Por favor acceda al siguiente enlace, para inciar el proceso de Autorizaci&oacute;n o No autorizaci&oacute;n y ver la informaci&oacute;n del acta.</font></td></tr>',
'Mensaje de inicio de proceso de aprobacion del acta',
13,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_CUERPO_URL_ACTA',
'<tr><td><font face="Corbel"><a href=":<P_EMAIL_CUERPO_URL_ACTA>">:<P_EMAIL_URL_TEXT_ACTA></a></font></td></tr><tr><td></td></tr><tr><td></td></tr>',
'URL DEL ACTA A APROBAR O NO APROBAR',
14,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_PIE_PAG_MENSAJE_DESPEDIDA',
'<tr><td><font face="Corbel">Cordialmente</font></td></tr>',
'Mensaje de despedida en el email de mensaje',
15,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_PIE_PAG_REMITENTE',
'<tr><td><font face="Corbel">Oficina de Registro Academico</font></td></tr>',
'Remitente del Mensaje',
16,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_PIE_PAG_NOM_UNIVERSIDAD',
'<tr><td><font face="Corbel">Pontificia Universidad Javeriana</font></td></tr></table></td></tr>',
'Nombre de la universidad que envia el mensaje',
17,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_ESTUDIANTE_PIE_PAG_IMG',
'<tr><td><img src="http://apps.premize.com/premize/public/assets/images/abajo.jpg" width="1000"></td></tr></table>',
'Imagen del pie de pagina',
18,
'ACTIVO');
--------------------------------------//------//--EMAIL ESTUDIANTE--------------------------------//
--------------------------------------//------//--EMAIL SECRETARIA--------------------------------//
INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval, 
'EMAIL_ACTA_SECRETARIA_CABECERA_IMG',
'<meta charset="utf-8"><table align="center"><tr><td><img src="http://apps.premize.com/premize/public/assets/images/arriba.jpg" width="1000"></td></tr><tr><td>',
'Imagen de cabecera en el email',
1,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CABECERA_LUGAR_FECHA',
'<table><tr><td><font face="Corbel"> Santiago de Cali, :<P_EMAIL_FECHA></font></td></tr>',
'Lugar y fecha de envio del email',
2,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CABECERA_PREFIX_NAME',
'<tr><td ><font face="Corbel">Estimado :<P_EMAIL_NOMBRE_DESTINATARIO></font></td></tr><tr><td></td></tr>',
'Prefijo y nombre del destinatario',
3,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_MENSAJE',
'<tr><td><font face="Corbel"> <br><br><br>Le informamos
 que el proceso de autorizaci&oacute;n del acta solicitada ha comenzado, por favor verifique la correspondencia y gestione autorizaci&oacute;n de esta..</font></td></tr>',
'Mensaje principal del email',
4,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_LABEL_DATOS_ACTA',
'<tr><td><font face="Corbel">Estos son los datos b&aacute;sicos:</font></td></tr><tr><td>',
'Cabecera para mostrar los datos del acta',
5,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_ORDENABLE_NUMERO_ACTA',
'<table><tr><td><font face="Corbel"><b>Acta :</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_ACTA></font></td></tr>',
'Numero del acta a enviar en el email',
6,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_ORDENABLE_NOM_ESTUDIANTE',
'<tr><td><font face="Corbel"><b> Estudiante :</b></font></td><td><font face="Corbel"> :<P_EMAIL_TEXT_ESTUDIANTE></font></td></tr>',
'Estudiante asociado al Acta',
7,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_ORDENABLE_UNIVERSIDAD',
'<tr><td><font face="Corbel"><b>Universidad de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_UNIVERSIDAD></font></td></tr>',
'Universidad de procedencia del estudiante asociada al acta',
8,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_ORDENABLE_PROGRAMA_PROC',
'<tr><td><font face="Corbel"><b>Programa de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PROC></font></td></tr>',
'Programa de procedencia del estudiante asociado al acta',
9,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_ORDENABLE_PROGRAMA_PUJ',
'<tr><td><font face="Corbel"><b>Programa PUJ:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PUJ></font></td></tr>',
'Programa de destino del estudiante asociado al acta',
10,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_ORDENABLE_UBIC_SEM',
'<tr><td><font face="Corbel"><b>Ubicaci&oacute;n Semestral:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_UBICACION_SEM></font></td></tr></table>',
'Ubicacion semestral del estudiante asociado al acta',
11,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_COMENTARIO',
'<tr><td></td></tr><tr><td><font face="Corbel"><b> Comentario : </b></font></td></tr><tr><td><font face="Corbel">:<P_EMAIL_CUERPO_COMENTARIO_TEXT> </font></td></tr>',
'Comentario del email de acta',
12,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_MENSAJE_2',
'<tr><td></td></tr><tr><td><font face="Corbel">Por favor acceda al siguiente enlace, para inciar el proceso de Autorizaci&oacute;n o No autorizaci&oacute;n y ver la informaci&oacute;n del acta.</font></td></tr>',
'Mensaje de inicio de proceso de aprobacion del acta',
13,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_CUERPO_URL_ACTA',
'<tr><td><font face="Corbel"><a href=":<P_EMAIL_CUERPO_URL_ACTA>">:<P_EMAIL_URL_TEXT_ACTA></a></font></td></tr><tr><td></td></tr><tr><td></td></tr>',
'URL DEL ACTA A APROBAR O NO APROBAR',
14,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_PIE_PAG_MENSAJE_DESPEDIDA',
'<tr><td><font face="Corbel">Cordialmente</font></td></tr>',
'Mensaje de despedida en el email de mensaje',
15,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_PIE_PAG_REMITENTE',
'<tr><td><font face="Corbel">Oficina de Registro Academico</font></td></tr>',
'Remitente del Mensaje',
16,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_PIE_PAG_NOM_UNIVERSIDAD',
'<tr><td><font face="Corbel">Pontificia Universidad Javeriana</font></td></tr></table></td></tr>',
'Nombre de la universidad que envia el mensaje',
17,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_SECRETARIA_PIE_PAG_IMG',
'<tr><td><img src="http://apps.premize.com/premize/public/assets/images/abajo.jpg" width="1000"></td></tr></table>',
'Imagen del pie de pagina',
18,
'ACTIVO');
--------------------------------------//------//--EMAIL SECRETARIA--------------------------------//
--------------------------------------//------//--EMAIL REGISTRO ACADEMICO--------------------------------//
INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval, 
'EMAIL_ACTA_REGISTRO_ACADEMICO_CABECERA_IMG',
'<meta charset="utf-8"><table align="center"><tr><td><img src="http://apps.premize.com/premize/public/assets/images/arriba.jpg" width="1000"></td></tr><tr><td>',
'Imagen de cabecera en el email',
1,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CABECERA_LUGAR_FECHA',
'<table><tr><td><font face="Corbel"> Santiago de Cali, :<P_EMAIL_FECHA></font></td></tr>',
'Lugar y fecha de envio del email',
2,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CABECERA_PREFIX_NAME',
'<tr><td ><font face="Corbel">Estimado :<P_EMAIL_NOMBRE_DESTINATARIO></font></td></tr><tr><td></td></tr>',
'Prefijo y nombre del destinatario',
3,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_MENSAJE',
'<tr><td><font face="Corbel"> <br><br><br>Le informamos
 que el proceso de autorizaci&oacute;n del acta solicitada ha comenzado, por favor verifique la correspondencia y gestione autorizaci&oacute;n de esta..</font></td></tr>',
'Mensaje principal del email',
4,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_LABEL_DATOS_ACTA',
'<tr><td><font face="Corbel">Estos son los datos b&aacute;sicos:</font></td></tr><tr><td>',
'Cabecera para mostrar los datos del acta',
5,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_ORDENABLE_NUMERO_ACTA',
'<table><tr><td><font face="Corbel"><b>Acta :</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_ACTA></font></td></tr>',
'Numero del acta a enviar en el email',
6,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_ORDENABLE_NOM_ESTUDIANTE',
'<tr><td><font face="Corbel"><b> Estudiante :</b></font></td><td><font face="Corbel"> :<P_EMAIL_TEXT_ESTUDIANTE></font></td></tr>',
'Estudiante asociado al Acta',
7,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_ORDENABLE_UNIVERSIDAD',
'<tr><td><font face="Corbel"><b>Universidad de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_UNIVERSIDAD></font></td></tr>',
'Universidad de procedencia del estudiante asociada al acta',
8,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_ORDENABLE_PROGRAMA_PROC',
'<tr><td><font face="Corbel"><b>Programa de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PROC></font></td></tr>',
'Programa de procedencia del estudiante asociado al acta',
9,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_ORDENABLE_PROGRAMA_PUJ',
'<tr><td><font face="Corbel"><b>Programa PUJ:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PUJ></font></td></tr>',
'Programa de destino del estudiante asociado al acta',
10,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_ORDENABLE_UBIC_SEM',
'<tr><td><font face="Corbel"><b>Ubicaci&oacute;n Semestral:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_UBICACION_SEM></font></td></tr></table>',
'Ubicacion semestral del estudiante asociado al acta',
11,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_COMENTARIO',
'<tr><td></td></tr><tr><td><font face="Corbel"><b> Comentario : </b></font></td></tr><tr><td><font face="Corbel">:<P_EMAIL_CUERPO_COMENTARIO_TEXT> </font></td></tr>',
'Comentario del email de acta',
12,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_MENSAJE_2',
'<tr><td></td></tr><tr><td><font face="Corbel">Por favor acceda al siguiente enlace, para inciar el proceso de Autorizaci&oacute;n o No autorizaci&oacute;n y ver la informaci&oacute;n del acta.</font></td></tr>',
'Mensaje de inicio de proceso de aprobacion del acta',
13,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_CUERPO_URL_ACTA',
'<tr><td><font face="Corbel"><a href=":<P_EMAIL_CUERPO_URL_ACTA>">:<P_EMAIL_URL_TEXT_ACTA></a></font></td></tr><tr><td></td></tr><tr><td></td></tr>',
'URL DEL ACTA A APROBAR O NO APROBAR',
14,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_PIE_PAG_MENSAJE_DESPEDIDA',
'<tr><td><font face="Corbel">Cordialmente</font></td></tr>',
'Mensaje de despedida en el email de mensaje',
15,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_PIE_PAG_REMITENTE',
'<tr><td><font face="Corbel">Oficina de Registro Academico</font></td></tr>',
'Remitente del Mensaje',
16,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_PIE_PAG_NOM_UNIVERSIDAD',
'<tr><td><font face="Corbel">Pontificia Universidad Javeriana</font></td></tr></table></td></tr>',
'Nombre de la universidad que envia el mensaje',
17,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_REGISTRO_ACADEMICO_PIE_PAG_IMG',
'<tr><td><img src="http://apps.premize.com/premize/public/assets/images/abajo.jpg" width="1000"></td></tr></table>',
'Imagen del pie de pagina',
18,
'ACTIVO');

--------------------------------------//------//--EMAIL REGISTRO ACADEMICO--------------------------------//
--------------------------------------//------//--EMAIL DECANO ACADEMICO--------------------------------//
INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval, 
'EMAIL_ACTA_DECANO_ACADEMICO_CABECERA_IMG',
'<meta charset="utf-8"><table align="center"><tr><td><img src="http://apps.premize.com/premize/public/assets/images/arriba.jpg" width="1000"></td></tr><tr><td>',
'Imagen de cabecera en el email',
1,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CABECERA_LUGAR_FECHA',
'<table><tr><td><font face="Corbel"> Santiago de Cali, :<P_EMAIL_FECHA></font></td></tr>',
'Lugar y fecha de envio del email',
2,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CABECERA_PREFIX_NAME',
'<tr><td ><font face="Corbel">Estimado :<P_EMAIL_NOMBRE_DESTINATARIO></font></td></tr><tr><td></td></tr>',
'Prefijo y nombre del destinatario',
3,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_MENSAJE',
'<tr><td><font face="Corbel"> <br><br><br>Le informamos
 que el proceso de autorizaci&oacute;n del acta solicitada ha comenzado, por favor verifique la correspondencia y gestione autorizaci&oacute;n de esta..</font></td></tr>',
'Mensaje principal del email',
4,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_LABEL_DATOS_ACTA',
'<tr><td><font face="Corbel">Estos son los datos b&aacute;sicos:</font></td></tr><tr><td>',
'Cabecera para mostrar los datos del acta',
5,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_ORDENABLE_NUMERO_ACTA',
'<table><tr><td><font face="Corbel"><b>Acta :</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_ACTA></font></td></tr>',
'Numero del acta a enviar en el email',
6,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_ORDENABLE_NOM_ESTUDIANTE',
'<tr><td><font face="Corbel"><b> Estudiante :</b></font></td><td><font face="Corbel"> :<P_EMAIL_TEXT_ESTUDIANTE></font></td></tr>',
'Estudiante asociado al Acta',
7,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_ORDENABLE_UNIVERSIDAD',
'<tr><td><font face="Corbel"><b>Universidad de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_UNIVERSIDAD></font></td></tr>',
'Universidad de procedencia del estudiante asociada al acta',
8,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_ORDENABLE_PROGRAMA_PROC',
'<tr><td><font face="Corbel"><b>Programa de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PROC></font></td></tr>',
'Programa de procedencia del estudiante asociado al acta',
9,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_ORDENABLE_PROGRAMA_PUJ',
'<tr><td><font face="Corbel"><b>Programa PUJ:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PUJ></font></td></tr>',
'Programa de destino del estudiante asociado al acta',
10,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_ORDENABLE_UBIC_SEM',
'<tr><td><font face="Corbel"><b>Ubicaci&oacute;n Semestral:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_UBICACION_SEM></font></td></tr></table>',
'Ubicacion semestral del estudiante asociado al acta',
11,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_COMENTARIO',
'<tr><td></td></tr><tr><td><font face="Corbel"><b> Comentario : </b></font></td></tr><tr><td><font face="Corbel">:<P_EMAIL_CUERPO_COMENTARIO_TEXT> </font></td></tr>',
'Comentario del email de acta',
12,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_MENSAJE_2',
'<tr><td></td></tr><tr><td><font face="Corbel">Por favor acceda al siguiente enlace, para inciar el proceso de Autorizaci&oacute;n o No autorizaci&oacute;n y ver la informaci&oacute;n del acta.</font></td></tr>',
'Mensaje de inicio de proceso de aprobacion del acta',
13,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_CUERPO_URL_ACTA',
'<tr><td><font face="Corbel"><a href=":<P_EMAIL_CUERPO_URL_ACTA>">:<P_EMAIL_URL_TEXT_ACTA></a></font></td></tr><tr><td></td></tr><tr><td></td></tr>',
'URL DEL ACTA A APROBAR O NO APROBAR',
14,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_PIE_PAG_MENSAJE_DESPEDIDA',
'<tr><td><font face="Corbel">Cordialmente</font></td></tr>',
'Mensaje de despedida en el email de mensaje',
15,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_PIE_PAG_REMITENTE',
'<tr><td><font face="Corbel">Oficina de Registro Academico</font></td></tr>',
'Remitente del Mensaje',
16,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_PIE_PAG_NOM_UNIVERSIDAD',
'<tr><td><font face="Corbel">Pontificia Universidad Javeriana</font></td></tr></table></td></tr>',
'Nombre de la universidad que envia el mensaje',
17,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DECANO_ACADEMICO_PIE_PAG_IMG',
'<tr><td><img src="http://apps.premize.com/premize/public/assets/images/abajo.jpg" width="1000"></td></tr></table>',
'Imagen del pie de pagina',
18,
'ACTIVO');

--------------------------------------//------//--EMAIL DECANO ACADEMICO--------------------------------//
--------------------------------------//------//--EMAIL DIRECTOR DE CARRERA--------------------------------//
INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval, 
'EMAIL_ACTA_DIRECTOR_CARRERA_CABECERA_IMG',
'<meta charset="utf-8"><table align="center"><tr><td><img src="http://apps.premize.com/premize/public/assets/images/arriba.jpg" width="1000"></td></tr><tr><td>',
'Imagen de cabecera en el email',
1,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CABECERA_LUGAR_FECHA',
'<table><tr><td><font face="Corbel"> Santiago de Cali, :<P_EMAIL_FECHA></font></td></tr>',
'Lugar y fecha de envio del email',
2,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CABECERA_PREFIX_NAME',
'<tr><td ><font face="Corbel">Estimado :<P_EMAIL_NOMBRE_DESTINATARIO></font></td></tr><tr><td></td></tr>',
'Prefijo y nombre del destinatario',
3,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_MENSAJE',
'<tr><td><font face="Corbel"> <br><br><br>Le informamos
 que el proceso de autorizaci&oacute;n del acta solicitada ha comenzado, por favor verifique la correspondencia y gestione autorizaci&oacute;n de esta..</font></td></tr>',
'Mensaje principal del email',
4,
'ACTIVO');



INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_LABEL_DATOS_ACTA',
'<tr><td><font face="Corbel">Estos son los datos b&aacute;sicos:</font></td></tr><tr><td>',
'Cabecera para mostrar los datos del acta',
5,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_ORDENABLE_NUMERO_ACTA',
'<table><tr><td><font face="Corbel"><b>Acta :</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_ACTA></font></td></tr>',
'Numero del acta a enviar en el email',
6,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_ORDENABLE_NOM_ESTUDIANTE',
'<tr><td><font face="Corbel"><b> Estudiante :</b></font></td><td><font face="Corbel"> :<P_EMAIL_TEXT_ESTUDIANTE></font></td></tr>',
'Estudiante asociado al Acta',
7,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_ORDENABLE_UNIVERSIDAD',
'<tr><td><font face="Corbel"><b>Universidad de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_UNIVERSIDAD></font></td></tr>',
'Universidad de procedencia del estudiante asociada al acta',
8,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_ORDENABLE_PROGRAMA_PROC',
'<tr><td><font face="Corbel"><b>Programa de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PROC></font></td></tr>',
'Programa de procedencia del estudiante asociado al acta',
9,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_ORDENABLE_PROGRAMA_PUJ',
'<tr><td><font face="Corbel"><b>Programa PUJ:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PUJ></font></td></tr>',
'Programa de destino del estudiante asociado al acta',
10,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_ORDENABLE_UBIC_SEM',
'<tr><td><font face="Corbel"><b>Ubicaci&oacute;n Semestral:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_UBICACION_SEM></font></td></tr></table>',
'Ubicacion semestral del estudiante asociado al acta',
11,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_COMENTARIO',
'<tr><td></td></tr><tr><td><font face="Corbel"><b> Comentario : </b></font></td></tr><tr><td><font face="Corbel">:<P_EMAIL_CUERPO_COMENTARIO_TEXT> </font></td></tr>',
'Comentario del email de acta',
12,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_MENSAJE_2',
'<tr><td></td></tr><tr><td><font face="Corbel">Por favor acceda al siguiente enlace, para inciar el proceso de Autorizaci&oacute;n o No autorizaci&oacute;n y ver la informaci&oacute;n del acta.</font></td></tr>',
'Mensaje de inicio de proceso de aprobacion del acta',
13,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_CUERPO_URL_ACTA',
'<tr><td><font face="Corbel"><a href=":<P_EMAIL_CUERPO_URL_ACTA>">:<P_EMAIL_URL_TEXT_ACTA></a></font></td></tr><tr><td></td></tr><tr><td></td></tr>',
'URL DEL ACTA A APROBAR O NO APROBAR',
14,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_PIE_PAG_MENSAJE_DESPEDIDA',
'<tr><td><font face="Corbel">Cordialmente</font></td></tr>',
'Mensaje de despedida en el email de mensaje',
15,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_PIE_PAG_REMITENTE',
'<tr><td><font face="Corbel"> Oficina de Registro Academico </font></td></tr>',
'Remitente del Mensaje',
16,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_PIE_PAG_NOM_UNIVERSIDAD',
'<tr><td><font face="Corbel"> Pontificia Universidad Javeriana </font></td></tr></table></td></tr>',
'Nombre de la universidad que envia el mensaje',
17,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_PIE_PAG_IMG',
'<tr><td><img src="http://apps.premize.com/premize/public/assets/images/abajo.jpg" width="1000"></td></tr></table>',
'Imagen del pie de pagina',
18,
'ACTIVO');

--------------------------------------//------//--EMAIL DIRECTOR DE CARRERA--------------------------------//
--------------------------------------//------//--EMAIL DIRECTOR DE CARRERA ACTA NO APROBADA--------------------------------//
INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval, 
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CABECERA_IMG',
'<meta charset="utf-8"><table align="center"><tr><td><img src="http://apps.premize.com/premize/public/assets/images/arriba.jpg" width="1000"></td></tr><tr><td>',
'Imagen de cabecera en el email',
1,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CABECERA_LUGAR_FECHA',
'<table><tr><td><font face="Corbel"> Santiago de Cali, :<P_EMAIL_FECHA></font></td></tr>',
'Lugar y fecha de envio del email',
2,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CABECERA_PREFIX_NAME',
'<tr><td ><font face="Corbel">Estimado :<P_EMAIL_NOMBRE_DESTINATARIO></font></td></tr><tr><td></td></tr>',
'Prefijo y nombre del destinatario',
3,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_MENSAJE',
'<tr><td><font face="Corbel"> <br><br><br>Le informamos
 que la siguiente acta ha sido No Aprobada por el siguiente usuario :<P_EMAIL_NOMBRE_USUARIO> </font></td></tr>',
'Mensaje principal del email',
4,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_LABEL_DATOS_ACTA',
'<tr><td><font face="Corbel">Estos son los datos b&aacute;sicos:</font></td></tr><tr><td>',
'Cabecera para mostrar los datos del acta',
5,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_ORDENABLE_NUMERO_ACTA',
'<table><tr><td><font face="Corbel"><b>Acta :</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_ACTA></font></td></tr>',
'Numero del acta a enviar en el email',
6,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_ORDENABLE_NOM_ESTUDIANTE',
'<tr><td><font face="Corbel"><b> Estudiante :</b></font></td><td><font face="Corbel"> :<P_EMAIL_TEXT_ESTUDIANTE></font></td></tr>',
'Estudiante asociado al Acta',
7,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_ORDENABLE_UNIVERSIDAD',
'<tr><td><font face="Corbel"><b>Universidad de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_UNIVERSIDAD></font></td></tr>',
'Universidad de procedencia del estudiante asociada al acta',
8,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_ORDENABLE_PROGRAMA_PROC',
'<tr><td><font face="Corbel"><b>Programa de procedencia : </b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PROC></font></td></tr>',
'Programa de procedencia del estudiante asociado al acta',
9,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_ORDENABLE_PROGRAMA_PUJ',
'<tr><td><font face="Corbel"><b>Programa PUJ:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_CUERPO_ORDENABLE_PROGRAMA_PUJ></font></td></tr>',
'Programa de destino del estudiante asociado al acta',
10,
'ACTIVO');


INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_ORDENABLE_UBIC_SEM',
'<tr><td><font face="Corbel"><b>Ubicaci&oacute;n Semestral:</b></font></td><td><font face="Corbel">:<P_EMAIL_TEXT_UBICACION_SEM></font></td></tr></table>',
'Ubicacion semestral del estudiante asociado al acta',
11,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_COMENTARIO',
'<tr><td></td></tr><tr><td><font face="Corbel"><b> Comentario : </b></font></td></tr><tr><td><font face="Corbel">:<P_EMAIL_CUERPO_COMENTARIO_TEXT> </font></td></tr>',
'Comentario del email de acta',
12,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_MENSAJE_2',
'<tr><td></td></tr><tr><td><font face="Corbel">Por favor acceda al siguiente enlace, para inciar el proceso de Autorizaci&oacute;n o No autorizaci&oacute;n y ver la informaci&oacute;n del acta.</font></td></tr>',
'Mensaje de inicio de proceso de aprobacion del acta',
13,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_CUERPO_URL_ACTA',
'<tr><td><font face="Corbel"><a href=":<P_EMAIL_CUERPO_URL_ACTA>">:<P_EMAIL_URL_TEXT_ACTA></a></font></td></tr><tr><td></td></tr><tr><td></td></tr>',
'URL DEL ACTA A APROBAR O NO APROBAR',
14,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_PIE_PAG_MENSAJE_DESPEDIDA',
'<tr><td><font face="Corbel">Cordialmente</font></td></tr>',
'Mensaje de despedida en el email de mensaje',
15,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_PIE_PAG_REMITENTE',
'<tr><td><font face="Corbel"> Oficina de Registro Academico </font></td></tr>',
'Remitente del Mensaje',
16,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_PIE_PAG_NOM_UNIVERSIDAD',
'<tr><td><font face="Corbel"> Pontificia Universidad Javeriana </font></td></tr></table></td></tr>',
'Nombre de la universidad que envia el mensaje',
17,
'ACTIVO');

INSERT INTO HMA_PARAMETRO parametro(consecutivo,clave,valor,descripcion,ordenamiento,estado)
VALUES(SEQ_CONSECUTIVO_PARAMETRO.nextval,
'EMAIL_ACTA_DIRECTOR_CARRERA_NO_APROBO_PIE_PAG_IMG',
'<tr><td><img src="http://apps.premize.com/premize/public/assets/images/abajo.jpg" width="1000"></td></tr></table>',
'Imagen del pie de pagina',
18,
'ACTIVO');
--------------------------------------//------//--EMAIL DIRECTOR DE CARRERA ACTA NO APROBADA--------------------------------//
--ALTER TABLE HMA_PROGRAMA 
--ADD FOREIGN KEY(UNIVERSIDAD_EXT_ORG_ID,UNIVERSIDAD_EFFDT)
--REFERENCES HMA_UNIVERSIDAD(EXT_ORG_ID,EFFDT);

