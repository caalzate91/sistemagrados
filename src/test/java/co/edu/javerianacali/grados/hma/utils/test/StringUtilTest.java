/**
 * 
 */
package co.edu.javerianacali.grados.hma.utils.test;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import co.edu.javerianacali.grados.enums.ComparisonEnum;
import co.edu.javerianacali.grados.enums.RegexEnum;
import co.edu.javerianacali.grados.utils.StringUtil;

/**		
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
  
</a>	
 * @project HomologacionAsignaturas	
 * @class StringUtilTest	
 * @description	
 * @date 21/01/2015	
 *	
 */
public class StringUtilTest {
	

	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 26/02/2015	
	 * @description
	 */
	@Test
	public void test_converInList(){
		String string = "40&UGRD&0014417";
		String indexOf = RegexEnum.COMPOSITE_PK.getRegex();
		
		List<String > list = StringUtil.compositeKeyToList(string, indexOf);
		Assert.assertFalse(list.isEmpty());

	}
	
	@Test
	public void test_1() {
		Date d1 = new Date();
		System.out.println(d1);
		String result = "=";
		for (int i = 0; i < 1000000; i++) {
			try {
				ComparisonEnum e = ComparisonEnum.valueOf(ComparisonEnum.class, "GT");
				switch(e) {
				case EQ:
					result = ComparisonEnum.EQ.getValue();
					break;
				case LQ:
					result = ComparisonEnum.LQ.getValue();
					break;
				case LT:
					result = ComparisonEnum.LT.getValue();
					break;
				case GT:
					result = ComparisonEnum.GT.getValue();
					break;
				default:
					result = "=";
				}
			} catch (IllegalArgumentException e) {
				ComparisonEnum.valueOf(ComparisonEnum.class, "EQ");
			}
		}
		Date d2 = new Date();
		System.out.println(d2);
		System.out.println((d2.getSeconds() - d1.getSeconds()));
		System.out.println(result);
	}
	
	@Test
	public void test_2() {
		StringBuilder sb = new StringBuilder();
		sb.append("Alex");
		sb.append(",");
		sb.append("JaFc");
		sb.append(",");
		sb.append("Fili");
		sb.append(",");
		sb.deleteCharAt(sb.length()-1);
		System.out.println(sb.toString());
	}
	

}
