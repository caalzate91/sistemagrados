/**
 * 
 */
package co.edu.javerianacali.grados.hma.cripto.test;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import co.edu.javerianacali.grados.utils.CryptoUtil;

import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;

/**
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
 * 
 *         </a>
 * @project HomologacionAsignaturas
 * @class CriptoTest
 * @description
 * @date 3/03/2015
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/test-context.xml" })
@Transactional
public class CriptoTest {

	private static Cipher encryptor;
	private static Cipher decryptor;

	private static SecretKey key;

	private static final Logger LOG = Logger.getLogger(CriptoTest.class);

	@Test
	public void cryptoTest() throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {

		key = KeyGenerator.getInstance("DES").generateKey();
		encryptor = Cipher.getInstance("DES/ECB/PKCS5Padding");
		decryptor = Cipher.getInstance("DES/ECB/PKCS5Padding");
		encryptor.init(Cipher.ENCRYPT_MODE, key);
		decryptor.init(Cipher.DECRYPT_MODE, key);
		String consecutivoActa = "66";
		byte[] utf8 = consecutivoActa.getBytes("UTF8");
		byte[] enc = encryptor.doFinal(utf8); 
		 enc = BASE64EncoderStream.encode(enc);
		String crypt = new String(enc);
		byte[] dec = BASE64DecoderStream.decode(crypt.getBytes("UTF8"));
		// byte[] dec = crypt
		byte[] decUtf8 = decryptor.doFinal(dec);

		String decrytp = new String(decUtf8, "UTF8");
		LOG.info(decrytp); 
	}
	
	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 4/03/2015	
	 * @description	
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws UnsupportedEncodingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	@Test
	public void cryptoUtilTest() throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		Long cons = 66L;
		String consActa = String.valueOf(cons);
		String crypt = CryptoUtil.encryptString(consActa);
		String decrypt = CryptoUtil.decryptString(crypt);
		
		LOG.info(decrypt);
		

	}

}
