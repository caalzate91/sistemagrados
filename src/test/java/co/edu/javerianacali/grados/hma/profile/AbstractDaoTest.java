package co.edu.javerianacali.grados.hma.profile;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.junit.BeforeClass;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class AbstractDaoTest
 * @description
 * @date 27/11/2014
 */
public abstract class AbstractDaoTest extends
		AbstractTransactionalJUnit4SpringContextTests {

	private static EmbeddedDatabase database;
	private static JdbcTemplate jdbcTemplate;

	@BeforeClass
	public static void init() {
		database = (new EmbeddedDatabaseBuilder()).addScript(
				"classpath:/META-INF/script.sql").build();

		jdbcTemplate = new JdbcTemplate(database);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 4/02/2015
	 * @param dataSource
	 * @see org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests#setDataSource(javax.sql.DataSource)
	 */
	@Resource(name = "principalDataSource")
	@Override
	public void setDataSource(final DataSource dataSource) {
		jdbcTemplate.setDataSource(dataSource);
	}
}