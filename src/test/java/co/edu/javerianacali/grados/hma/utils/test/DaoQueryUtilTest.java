package co.edu.javerianacali.grados.hma.utils.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.edu.javerianacali.grados.utils.DaoQueryUtil;
import co.edu.javerianacali.grados.utils.gridview.SortField;
import co.edu.javerianacali.grados.utils.namedquery.INamedQuery;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class DaoQueryUtilTest
 * @description
 * @date 6/01/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/test-context.xml"})
public class DaoQueryUtilTest {
	
	private static final Logger LOG = Logger.getLogger(DaoQueryUtilTest.class);
	
	@Autowired
	private INamedQuery namedQuery;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/01/2015
	 * @description
	 */
	@Test
	public void test_getSortFields() {
		List<SortField> sortFields = new ArrayList<SortField>();
		sortFields.add(new SortField("property1", "ASC"));
		sortFields.add(new SortField("property2", "DESC"));
		sortFields.add(new SortField("property3", "ASC"));

		String result = "ORDER BY property1 ASC, property2 DESC, property3 ASC";
		String orderQuery = DaoQueryUtil.getSortFields(sortFields);

		Assert.assertTrue(result.equals(orderQuery));
	}
	
	

}
