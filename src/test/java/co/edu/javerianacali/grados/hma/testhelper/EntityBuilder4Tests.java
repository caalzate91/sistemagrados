package co.edu.javerianacali.grados.hma.testhelper;


/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class EntityBuilder4Tests
 * @description
 * @date 4/02/2015
 */
public final class EntityBuilder4Tests {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 4/02/2015
	 */
	private EntityBuilder4Tests() {
	}

}
