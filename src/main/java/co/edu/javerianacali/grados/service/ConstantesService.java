package co.edu.javerianacali.grados.service;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ConstantesService
 * @description
 * @date 8/07/2015
 */
public final class ConstantesService {

	/**
	 * Bundle error messages
	 */
	public static final String I18N_ERROR_MESSAGES = "i18n.error-messages";

	public static final String DAO_EXCEPTION = "i18n.error-messages";
	public static final String PARAMETRO_LIST_EMPTY = "PARAMETRO_LIST_EMPTY";
	public static final String ESTUDIANTE_EMPTY = "ESTUDIANTE_EMPTY";
	
	/**
	 * BTP = Buscar Tipo Parametro Error Code.
	 */
	public static final String BTP_VALUE_CRITERIA_NULL = "BTP_VALUE_CRITERIA_NULL";
	public static final String BTP_LIMIT_CRITERIA_NULL = "BTP_LIMIT_CRITERIA_NULL";

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 */
	private ConstantesService() {
	}

}
