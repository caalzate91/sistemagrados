package co.edu.javerianacali.grados.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.grados.annotations.AppTransactional;
import co.edu.javerianacali.grados.entities.Parametro;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.exception.core.ErrorCode.SeverityEnum;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.persistence.IParametroDao;
import co.edu.javerianacali.grados.interfaces.service.IParametroService;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ParametroService
 * @description
 * @date 8/07/2015
 */
@AppTransactional
@Service("parametroService")
public class ParametroService implements IParametroService {

	@Autowired
	private IParametroDao parametroDao;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#consultarParametros(co.edu.javerianacali.grados.utils.PagingCriteria)
	 */
	@Override
	public PagedList<Parametro> consultarParametros(PagingCriteria criteria)
			throws ServiceException {
		PagedList<Parametro> pagedList = null;
		try {
			pagedList = parametroDao.consultarParametros(criteria);
			if (pagedList.getRecords().isEmpty()) {
				throw new ServiceException("No se encontraron parametros",
						new ErrorCode<String>(
								ConstantesService.PARAMETRO_LIST_EMPTY,
								ConstantesService.I18N_ERROR_MESSAGES,
								SeverityEnum.WARNING));
			}
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}

		return pagedList;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 10/07/2015
	 * @param parametro
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#guardarParametro(co.edu.javerianacali.grados.entities.Parametro)
	 */
	@Override
	public void guardarParametro(Parametro parametro) throws ServiceException {
		if (null == parametro) {
			throw new IllegalArgumentException(
					"Error al guardar, la entidad parametro es nula");
		}
		try {
			parametroDao.guardarParametro(parametro);
			// parametroDao.save(parametro);
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés
	 *         Rodríguez</a>
	 * @date 28/08/2015
	 * @description
	 * @param parametro
	 * @throws ServiceException
	 */
	@Override
	public void actualizarParametro(Parametro parametro)
			throws ServiceException {
		if (null == parametro) {
			throw new IllegalArgumentException(
					"Error al actualizar, la entidad parametro es nula");
		}

		try {
			parametroDao.editarParametro(parametro);
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}

	}

	/**
	 * 
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés
	 *         Rodríguez</a>
	 * @date 31/08/2015
	 * @param id
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#eliminarParametro(java.lang.String)
	 */
	@Override
	public void eliminarParametro(String id) throws ServiceException {
		if (StringUtil.isEmpty(id)) {
			throw new IllegalArgumentException(
					"Error al eliminar, el id de la entidad parametro es nulo");
		}
		Long consecutivo = Long.parseLong(id);
		try {
			parametroDao.eliminarParametroById(consecutivo);
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}

	}

}
