package co.edu.javerianacali.grados.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.grados.annotations.AppTransactional;
import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.entities.TipoParametro;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.persistence.ITipoParametroDao;
import co.edu.javerianacali.grados.interfaces.service.ITipoParametroService;
import co.edu.javerianacali.grados.utils.StringUtil;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class TipoParametroService
 * @description
 * @date 9/07/2015
 */
@AppTransactional
@Service("tipoParametroService")
public class TipoParametroService implements ITipoParametroService {

	@Autowired
	private ITipoParametroDao tipoParametroDao;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @param searchCriteria
	 * @return
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.ITipoParametroService#buscarTipoParametros(co.edu.javerianacali.grados.entities.SearchCriteria)
	 */
	@Override
	public List<TipoParametro> buscarTipoParametros(
			SearchCriteria searchCriteria) throws ServiceException {
		if (null == searchCriteria) {
			throw new IllegalArgumentException(
					"buscarTipoParametros(...) searchCriteria es nulo");
		}

		Object[] params = {};
		if (StringUtil.isEmpty(searchCriteria.getValue())) {
			throw new ServiceException(
					"el valor de busqueda para el tipoParametro es nulo",
					new ErrorCode<String>(
							ConstantesService.BTP_VALUE_CRITERIA_NULL,
							ConstantesService.I18N_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		if (StringUtil.isEmpty(searchCriteria.getLimit())) {
			throw new ServiceException(
					"El limite para la busqueda de tipoParametro es nulo",
					new ErrorCode<String>(
							ConstantesService.BTP_LIMIT_CRITERIA_NULL,
							ConstantesService.I18N_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		List<TipoParametro> result = new ArrayList<>();
		try {
			result = tipoParametroDao.autocompleteSearch(searchCriteria);
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION,
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getSeverity(), ex.getErrorcode().getParams()));
		}

		return result;
	}

}
