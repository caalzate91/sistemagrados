package co.edu.javerianacali.grados.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.entities.UsuarioAutenticado;
import co.edu.javerianacali.grados.interfaces.service.ISecurityService;
import co.edu.javerianacali.grados.security.PeopleSoftDaoLocal;
import co.edu.javerianacali.grados.security.PortalEstudiante;
import co.edu.javerianacali.grados.security.PortalEstudianteDaoLocal;

@Service
public class SecurityService implements ISecurityService {
	@Autowired
	PeopleSoftDaoLocal peopleSoftDao;

	@Autowired
	PortalEstudianteDaoLocal portalEstudianteDao;

    public PortalEstudiante procesar(String p_idEntrada) {
        PortalEstudiante portal = this.portalEstudianteDao.read(p_idEntrada);
        if (portal != null) {
            if (portal.getActivo().equalsIgnoreCase("Si")) {
                this.portalEstudianteDao.updateTicket(portal.getEmplid());
                return portal;
            }
        }
        return null;
    }



    // Getters and Setters
    public PortalEstudianteDaoLocal getPortalEstudianteDao() {
        return this.portalEstudianteDao;
    }

    public void setPortalEstudianteDao(PortalEstudianteDaoLocal portalEstudianteDao) {
        this.portalEstudianteDao = portalEstudianteDao;
    }

    public String procesar(String usuario, int sec) {
        return this.peopleSoftDao.read(usuario, sec);
    }

    public PeopleSoftDaoLocal getPeopleSoftDao() {
        return this.peopleSoftDao;
    }

    public void setPeopleSoftDao(PeopleSoftDaoLocal peopleSoftDao) {
        this.peopleSoftDao = peopleSoftDao;
    }

    public List<String> Roles(String usuario) {
        // TODO Auto-generated method stub
        return this.peopleSoftDao.Roles(usuario);
    }

    public UsuarioAutenticado usuarioAutenticado(String usuario)
    {
        return this.peopleSoftDao.usuarioAutenticado(usuario);
    }

}