package co.edu.javerianacali.grados.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.grados.annotations.AppTransactional;
import co.edu.javerianacali.grados.entities.ProgramaEstudiante;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.exception.core.ErrorCode.SeverityEnum;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.persistence.IProgramaEstudianteDao;
import co.edu.javerianacali.grados.interfaces.service.IProgramaEstudianteService;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteService
 * @description
 * @date 8/07/2015
 */
@AppTransactional
@Service("programaEstudianteService")
public class ProgramaEstudianteService implements IProgramaEstudianteService {
  
	@Autowired
	private IProgramaEstudianteDao programaEstudianteDao;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#consultarParametros(co.edu.javerianacali.grados.utils.PagingCriteria)
	 */
	@Override
	public PagedList<ProgramaEstudiante> consultarProgramaEstudiante(PagingCriteria criteria)
			throws ServiceException {
		PagedList<ProgramaEstudiante> pagedList = null;/*
		try {
			pagedList = parametroDao.consultarParametros(criteria);
			if (pagedList.getRecords().isEmpty()) {
				throw new ServiceException("No se encontraron parametros",
						new ErrorCode<String>(
								ConstantesService.PARAMETRO_LIST_EMPTY,
								ConstantesService.I18N_ERROR_MESSAGES,
								SeverityEnum.WARNING));
			}
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}
*/
		return pagedList;
	}

	
	@Override
	public List<ProgramaEstudiante> consultarProgramaEstudiante(String empleidEstudiante) throws ServiceException {
		List<ProgramaEstudiante> programaEstudiante;
		try {
			
			programaEstudiante = programaEstudianteDao.consultarProgramaEstudiante(empleidEstudiante);
			
		if (null == programaEstudiante) {
			
			throw new ServiceException("el 'empleidEstudiante' para la busqueda del estudiante es nulo",
					new ErrorCode<String>(
							ConstantesService.ESTUDIANTE_EMPTY,
							ConstantesService.I18N_ERROR_MESSAGES,
							SeverityEnum.WARNING));
			
		}
			
			if (programaEstudiante == null) {
				throw new ServiceException(
						"No se encontraron datos al realizar la consulta",
						new ErrorCode<String>(
								ConstantesService.ESTUDIANTE_EMPTY,
								ConstantesService.I18N_ERROR_MESSAGES,
								SeverityEnum.WARNING));
			}
		} catch (DaoException e) {
			throw new ServiceException("Error al consultar los estudiantes",
					new ErrorCode<String>(
							ConstantesService.DAO_EXCEPTION,
							ConstantesService.I18N_ERROR_MESSAGES, SeverityEnum.ERROR), e);
		}
		return programaEstudiante;

	}

}

