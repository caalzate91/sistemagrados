package co.edu.javerianacali.grados.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.grados.annotations.AppTransactional;
import co.edu.javerianacali.grados.entities.Estudiante;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.exception.core.ErrorCode.SeverityEnum;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.persistence.IEstudianteDao;
import co.edu.javerianacali.grados.interfaces.service.IEstudianteService;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteService
 * @description
 * @date 8/07/2015
 */
@AppTransactional
@Service("estudianteService")
public class EstudianteService implements IEstudianteService {

	@Autowired
	private IEstudianteDao estudianteDao;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#consultarParametros(co.edu.javerianacali.grados.utils.PagingCriteria)
	 */
	@Override
	public PagedList<Estudiante> consultarEstudiantes(PagingCriteria criteria)
			throws ServiceException {
		PagedList<Estudiante> pagedList = null;/*
		try {
			pagedList = parametroDao.consultarParametros(criteria);
			if (pagedList.getRecords().isEmpty()) {
				throw new ServiceException("No se encontraron parametros",
						new ErrorCode<String>(
								ConstantesService.PARAMETRO_LIST_EMPTY,
								ConstantesService.I18N_ERROR_MESSAGES,
								SeverityEnum.WARNING));
			}
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}
*/
		return pagedList;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 10/07/2015
	 * @param parametro
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#guardarParametro(co.edu.javerianacali.grados.entities.Parametro)
	 */
	@Override
	public void guardarEstudiante(Estudiante estudiante) throws ServiceException {
		/*if (null == estudiante) {
			throw new IllegalArgumentException(
					"Error al guardar, la entidad parametro es nula");
		}
		try {
			parametroDao.guardarParametro(parametro);
			// parametroDao.save(parametro);
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}*/
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés
	 *         Rodríguez</a>
	 * @date 28/08/2015
	 * @description
	 * @param parametro
	 * @throws ServiceException
	 */
	@Override
	public void actualizarEstudiante(Estudiante estudiante)
			throws ServiceException {
	/*	if (null == parametro) {
			throw new IllegalArgumentException(
					"Error al actualizar, la entidad parametro es nula");
		}

		try {
			parametroDao.editarParametro(parametro);
		} catch (DaoException ex) {
			throw new ServiceException(ex.getMessage(), new ErrorCode<String>(
					ConstantesService.DAO_EXCEPTION, ex.getErrorcode()
							.getSeverity(),
					ConstantesService.I18N_ERROR_MESSAGES, ex.getErrorcode()
							.getParams()), ex);
		}*/

	}

	/**
	 * 
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés
	 *         Rodríguez</a>
	 * @date 31/08/2015
	 * @param id
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#eliminarParametro(java.lang.String)
	 */
	@Override
	public Estudiante consultarEstudiante(String empleidEstudiante) throws ServiceException {
		Estudiante estudiante = new Estudiante();
		if (null == empleidEstudiante) {
			
			throw new ServiceException("el 'empleidEstudiante' para la busqueda del estudiante es nulo",
					new ErrorCode<String>(
							ConstantesService.ESTUDIANTE_EMPTY,
							ConstantesService.I18N_ERROR_MESSAGES,
							SeverityEnum.WARNING));
			
		}

		try {
	
			estudiante = estudianteDao.consultarEstudiante(empleidEstudiante);
			if (estudiante == null) {
				throw new ServiceException(
						"No se encontraron datos al realizar la consulta",
						new ErrorCode<String>(
								ConstantesService.ESTUDIANTE_EMPTY,
								ConstantesService.I18N_ERROR_MESSAGES,
								SeverityEnum.WARNING));
			}
		} catch (DaoException e) {
			throw new ServiceException("Error al consultar los estudiantes",
					new ErrorCode<String>(
							ConstantesService.DAO_EXCEPTION,
							ConstantesService.I18N_ERROR_MESSAGES, SeverityEnum.ERROR), e);
		}
		return estudiante;

	}

}
