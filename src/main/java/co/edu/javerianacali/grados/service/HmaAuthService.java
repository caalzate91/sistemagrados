package co.edu.javerianacali.grados.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.entities.UsuarioAutenticado;
import co.edu.javerianacali.grados.annotations.AppTransactional;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.service.IHmaAuthService;
import co.edu.javerianacali.grados.utils.StringUtil;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class HmaAuthService
 * @description
 * @date 20/03/2015
 */
@Service
public class HmaAuthService {

	private static final Logger LOG = Logger.getLogger(HmaAuthService.class);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/03/2015
	 * @param hmaToken
	 * @return
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.hma.interfaces.service.IHmaAuthService#doHmaAuthentication(java.lang.String)
	 */
//	@HmaTransactional
//	@Override
//	public UsuarioAutenticado doHmaAuthentication(String hmaToken)
//			throws ServiceException {
//		if (StringUtil.isEmpty(hmaToken)) {
//			throw new IllegalArgumentException(
//					"doHmaAuthentication(...): hmaToken is null");
//		}
//
//		UsuarioAutenticado ua = null;
//		try {
//			CorreoToken correoToken = correoTokenDao.getCorreoToken(hmaToken);
//			if (correoToken != null) {
//				ua = correoTokenDao.getUserByEmplid(correoToken.getEmplid());
//				if (ua != null) {
//					ua.setRoles(correoTokenDao.getRolesByUser(ua.getUsuario()));
//				}
//			}
//		} catch (DaoException ex) {
//			LOG.error(ex.getMessage(), ex);
//		}
//
//		return ua;
//	}

	/**
	 * Dummy. USAR UNICAMENTE EN DESARROLLO.
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/02/2015
	 * @param id
	 * @return
	 * @see co.edu.javerianacali.grados.hma.interfaces.service.IHmaAuthService#getUsuarioRole(java.lang.String)
	 */
//	public Map<String, Object> getUsuarioRole(String id) {
//		if (null == id) {
//			id = "";
//		}
//		return parametroDao.getUsuarioRole(id);
//	}

	/**
	 * Dummy. USAR UNICAMENTE EN DESARROLLO.
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 24/03/2015
	 * @param emplid
	 * @return
	 * @see co.edu.javerianacali.grados.hma.interfaces.service.IHmaAuthService#getUsuarioRoleByEmplid(java.lang.String)
	 */
//	public Map<String, Object> getUsuarioRoleByEmplid(String emplid) {
//		if (null == emplid) {
//			emplid = "";
//		}
//		return parametroDao.getUsuarioRoleByEmplid(emplid);
//	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/03/2015
	 * @param usuarioAutenticado
	 * @return
	 * @see co.edu.javerianacali.grados.hma.interfaces.service.IHmaAuthService#getRoleUsuario(co.edu.javerianacali.grados.entities.UsuarioAutenticado)
	 */
//	@Override
//	public String getRoleUsuario(UsuarioAutenticado usuarioAutenticado) {
//
//		if (null == usuarioAutenticado) {
//			return null;
//		}
//
//		if (null == usuarioAutenticado.getRoles()) {
//			return null;
//		}
//
//		for (String role : usuarioAutenticado.getRoles()) {
//			switch (role) {
//			case RolesAprobacion.ROLE_DIRECTOR_CARRERA:
//				return role;
//			case RolesAprobacion.ROLE_ESTUDIANTE:
//				return role;
//			case RolesAprobacion.ROLE_SECRETARIA_FACULTAD:
//				return role;
//			case RolesAprobacion.ROLE_DECANO_ACADEMICO:
//				return role;
//			case RolesAprobacion.ROLE_REGISTRO_ACADEMICO:
//				return role;
//			}
//		}
//
//		return null;
//	}

}
