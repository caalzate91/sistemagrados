package co.edu.javerianacali.grados.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.javerianacali.grados.annotations.AppTransactional;
import co.edu.javerianacali.grados.entities.ProcesosGrado;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.exception.core.ErrorCode.SeverityEnum;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.persistence.IProcesosGradoDao;
import co.edu.javerianacali.grados.interfaces.service.IProcesosGradoService;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteService
 * @description
 * @date 8/07/2015
 */
@AppTransactional
@Service("procesosGradoService")
public class ProcesosGradoService implements IProcesosGradoService {
  
	@Autowired
	private IProcesosGradoDao procesosGradoDao;

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 8/07/2015
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 * @see co.edu.javerianacali.grados.interfaces.service.IParametroService#consultarParametros(co.edu.javerianacali.grados.utils.PagingCriteria)
	 */
	
	@Override
	public List<ProcesosGrado> consultarProcesosGrado() throws ServiceException {
		List<ProcesosGrado> procesosGrado;
		try {
			
			procesosGrado = procesosGradoDao.consultarProcesosGrado();
			
		if (null == procesosGrado) {
			
			throw new ServiceException("el 'empleidEstudiante' para la busqueda del estudiante es nulo",
					new ErrorCode<String>(
							ConstantesService.ESTUDIANTE_EMPTY,
							ConstantesService.I18N_ERROR_MESSAGES,
							SeverityEnum.WARNING));
			
		}
			
			if (procesosGrado == null) {
				throw new ServiceException(
						"No se encontraron datos al realizar la consulta",
						new ErrorCode<String>(
								ConstantesService.ESTUDIANTE_EMPTY,
								ConstantesService.I18N_ERROR_MESSAGES,
								SeverityEnum.WARNING));
			}
		} catch (DaoException e) {
			throw new ServiceException("Error al consultar los estudiantes",
					new ErrorCode<String>(
							ConstantesService.DAO_EXCEPTION,
							ConstantesService.I18N_ERROR_MESSAGES, SeverityEnum.ERROR), e);
		}
		return procesosGrado;

	}

}

