package co.edu.javerianacali.grados.exception.core;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class InvalidAttributeException
 * @description Excepcion para controlar el mapeo incorrecto de las entidades.
 * @date 13/07/2015
 */
public class InvalidAttributeException extends DaoException {

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 */
	private static final long serialVersionUID = -8845747435094782009L;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param systemMessage
	 * @param errorCode
	 */
	public InvalidAttributeException(String systemMessage,
			ErrorCode<String> errorCode) {
		super(systemMessage, errorCode);
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param systemMessage
	 * @param errorCode
	 * @param cause
	 */
	public InvalidAttributeException(String systemMessage,
			ErrorCode<String> errorCode, Throwable cause) {
		super(systemMessage, errorCode, cause);
	}

}
