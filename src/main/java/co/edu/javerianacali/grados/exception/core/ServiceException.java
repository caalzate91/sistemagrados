package co.edu.javerianacali.grados.exception.core;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ServiceException
 * @description
 * @date 27/11/2014
 */
public class ServiceException extends AppException {

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 */
	private static final long serialVersionUID = -7721225436504046590L;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param systemMessage
	 * @param errorCode
	 */
	public ServiceException(String systemMessage, ErrorCode<String> errorCode) {
		super(systemMessage, errorCode);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param systemMessage
	 * @param errorCode
	 * @param cause
	 */
	public ServiceException(String systemMessage, ErrorCode<String> errorCode,
			Throwable cause) {
		super(systemMessage, errorCode, cause);
	}

}
