package co.edu.javerianacali.grados.exception.core;

import java.util.Arrays;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ErrorCode
 * @description
 * @date 27/11/2014
 */
public class ErrorCode<T> {

	private T errorCode;
	private String messageBundle;
	private Object[] params;
	private SeverityEnum severity;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param errorCode
	 */
	public ErrorCode(T errorCode) {
		super();
		this.errorCode = errorCode;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param errorCode
	 * @param messageBundle
	 */
	public ErrorCode(T errorCode, String messageBundle) {
		super();
		this.errorCode = errorCode;
		this.messageBundle = messageBundle;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param errorCode
	 * @param messageBundle
	 * @param params
	 */
	public ErrorCode(T errorCode, String messageBundle, Object... params) {
		super();
		this.errorCode = errorCode;
		this.messageBundle = messageBundle;
		this.params = params;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/07/2015
	 * @param errorCode
	 * @param messageBundle
	 * @param severity
	 * @param params
	 */
	public ErrorCode(T errorCode, String messageBundle, SeverityEnum severity,
			Object... params) {
		super();
		this.errorCode = errorCode;
		this.messageBundle = messageBundle;
		this.params = params;
		this.severity = severity;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the errorCode
	 */
	public T getErrorCode() {
		return errorCode;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the messageBundle
	 */
	public final String getMessageBundle() {
		return messageBundle;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the params
	 */
	public final Object[] getParams() {
		return params;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/07/2015
	 * @description
	 * @return
	 */
	public final SeverityEnum getSeverityEnum() {
		return severity;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/07/2015
	 * @description
	 * @return
	 */
	public final String getSeverity() {
		if (severity != null) {
			return severity.name();
		}
		return null;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppErrorCode [errorCode=" + errorCode + ", messageBundle="
				+ messageBundle + ", params=" + Arrays.toString(params) + "]";
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @project SistemaGrados
	 * @class SeverityEnum
	 * @description
	 * @date 6/07/2015
	 */
	public static enum SeverityEnum {
		INFORMATION, WARNING, ERROR, FATAL
	}

}
