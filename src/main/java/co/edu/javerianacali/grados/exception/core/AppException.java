package co.edu.javerianacali.grados.exception.core;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class AppException
 * @description
 * @date 27/11/2014
 */
public class AppException extends Exception {

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 */
	private static final long serialVersionUID = 896713779826059910L;

	private ErrorCode<String> errorcode;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param systemMessage
	 * @param errorCode
	 */
	public AppException(String systemMessage, ErrorCode<String> errorCode) {
		super(systemMessage);
		this.errorcode = errorCode;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param systemMessage
	 * @param errorCode
	 * @param cause
	 */
	public AppException(String systemMessage, ErrorCode<String> errorCode,
			Throwable cause) {
		super(systemMessage, cause);
		this.errorcode = errorCode;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the errorcode
	 */
	public ErrorCode<String> getErrorcode() {
		return errorcode;
	}

}
