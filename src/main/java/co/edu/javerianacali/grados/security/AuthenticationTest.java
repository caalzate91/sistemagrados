package co.edu.javerianacali.grados.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.edu.javerianacali.entities.UsuarioAutenticado;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class AuthenticationTest
 * @description
 * @date 19/02/2015
 */
public final class AuthenticationTest {

	private static final Map<String, UsuarioAutenticado> data;
	private static final Map<String, String> appRoles;

	static {
		appRoles = new HashMap<String, String>();
		appRoles.put("ROLE_DC", "ROLE_DIRECTOR_CARRERA");
		appRoles.put("ROLE_ES", "ROLE_ESTUDIANTE");
		appRoles.put("ROLE_SA", "ROLE_SECRETARIA_FACULTAD");
		appRoles.put("ROLE_DA", "ROLE_DECANO_ACADEMICO");
		appRoles.put("ROLE_RA", "ROLE_REGISTRO_ACADEMICO");
		appRoles.put("ROLE_US", "ROLE_USER");
		appRoles.put("ROLE_AD", "ROLE_ADMIN");

		Map<String, UsuarioAutenticado> map = new HashMap<String, UsuarioAutenticado>();
		UsuarioAutenticado ua = new UsuarioAutenticado();

		ua.setEmplid("0014417"); 
		ua.setUsuario("lebela");
		List<String> roles = new ArrayList<String>();
		roles.add(appRoles.get("ROLE_DC"));
		ua.setRoles(roles);
		map.put("lebela_123", ua);

		ua.setEmplid("0220630");
		ua.setUsuario("saramaria");
		roles = new ArrayList<String>();
		roles.add(appRoles.get("ROLE_ES"));
		ua.setRoles(roles);
		map.put("saramaria_1234", ua);

		ua.setEmplid("0032395");
		ua.setUsuario("terodriguez");
		roles = new ArrayList<String>();
		roles.add(appRoles.get("ROLE_SA"));
		ua.setRoles(roles);
		map.put("terodriguez_12345", ua);

		ua.setEmplid("0034204");
		ua.setUsuario("jgtv");
		roles = new ArrayList<String>();
		roles.add(appRoles.get("ROLE_RA"));
		ua.setRoles(roles);
		map.put("jgtv_1234567", ua); 

		ua.setEmplid("0036836");
		ua.setUsuario("nolano");
		roles = new ArrayList<String>();
		roles.add(appRoles.get("ROLE_RA"));
		ua.setRoles(roles);
		map.put("nolano_123456", ua);

		ua.setEmplid("3355779");
		ua.setUsuario("jafc");
		roles = new ArrayList<String>();
		roles.add(appRoles.get("ROLE_AD"));
		ua.setRoles(roles);
		map.put("jafc_123456", ua);

		data = Collections.unmodifiableMap(map);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 19/02/2015
	 */
	private AuthenticationTest() {

	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 19/02/2015
	 * @description
	 * @param pIdEntrada
	 * @return
	 */
	public static UsuarioAutenticado autenticar(String pIdEntrada) {
		if (pIdEntrada != null) {
			return data.get(pIdEntrada);
		}

		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/02/2015
	 * @description
	 * @param data
	 * @return
	 */
	public static UsuarioAutenticado getUsuarioAutenticado(
			Map<String, Object> data) {
		if (data != null) {
			UsuarioAutenticado ua = new UsuarioAutenticado();
			ua.setUsuario((String) data.get("usuario"));
			ua.setRol((String) data.get("rol"));
			ua.setEmplid((String) data.get("emplid"));
			@SuppressWarnings("unchecked")
			List<String> roles = (List<String>) data.get("roles");
			ua.setRoles(roles);

			return ua;
		}
		return null;
	}

}
