package co.edu.javerianacali.grados.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import co.edu.javerianacali.entities.UsuarioAutenticado;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.service.IHmaAuthService;
import co.edu.javerianacali.grados.interfaces.service.ISecurityService;
import co.edu.javerianacali.utils.StringUtil;


public class SessionFilter extends OncePerRequestFilter
{

    @Autowired
    ISecurityService securityService;
    
//    @Autowired
//    private IHmaAuthService hmaAuthService;
    
    private static final Logger LOG = Logger.getLogger(SessionFilter.class);

    // private static final Logger logger = Logger.getRootLogger();
   


    @Override
    protected void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        // No la cree si no existe (false)
        HttpSession session = request.getSession(true);
        String strUsuario, secuencia, p_idEntrada, hmaToken;
        boolean autenticado = false;
        p_idEntrada = request.getParameter("p_idEntrada");
        strUsuario = request.getParameter("pp");
        secuencia = request.getParameter("pp1");
        hmaToken = request.getParameter("hmaToken");
        
        UsuarioAutenticado ua = null;

        if (!StringUtil.isEmptyOrNull(strUsuario) || !StringUtil.isEmptyOrNull(p_idEntrada)
        		|| !StringUtil.isEmptyOrNull(hmaToken))
        {
            session.removeAttribute("SPRING_SECURITY_CONTEXT");
        }


        SecurityContext ctx = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");
        if (ctx == null) {

            boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString()
                    .indexOf("-agentlib:jdwp") > 0;

//            isDebug = false;

            if (isDebug) {
                ua = new UsuarioAutenticado();
                ua.setEmplid("");
                ua.setUsuario("alex");
                List<String> roles = new ArrayList<String>();
                roles.add("ROLE_ADMIN"); //Could be "ROLE_ADMIN"
                ua.setRoles(roles);
                autenticado = true;
            	//TODO ALEX: LOGICA DESARROLLO PREMIZE:
            	/*if(!StringUtil.isEmptyOrNull(p_idEntrada)) {
            		ua = AuthenticationTest.getUsuarioAutenticado(hmaAuthService
    						.getUsuarioRole(p_idEntrada));
                	if(ua != null) {
                		autenticado = true;
                	}   
            	} else if(!StringUtil.isEmptyOrNull(hmaToken)) {
            		try {
                		ua = hmaAuthService.doHmaAuthentication(hmaToken);
                		if(ua != null) {
                    		@SuppressWarnings("unchecked")
							List<String> roles = (List<String>) hmaAuthService
                    				.getUsuarioRoleByEmplid(ua.getEmplid()).get("roles");
                    		ua.getRoles().addAll(roles);
                			autenticado = true;
                		}                		
                	} catch (ServiceException ex) {
                		LOG.error(ex.getMessage(), ex);
                	} catch (Exception ex) {
                		LOG.error(ex.getMessage(), ex);
                	}
            	}*/
            	             
            } else {

                if ((!StringUtil.isEmptyOrNull(strUsuario) && !StringUtil.isEmptyOrNull(secuencia))) {
                    int strSecuencia = Integer.valueOf(secuencia.trim());
                    String usuario = this.securityService.procesar(strUsuario.trim(), strSecuencia);
                    if (!StringUtil.isEmptyOrNull(usuario)) {
                        ua = this.securityService.usuarioAutenticado(usuario);
                        autenticado = true;
                    }

                }
                else if (!StringUtil.isEmptyOrNull(p_idEntrada))
                {
                	//TODO PRODUCCION:
                    PortalEstudiante portal = this.securityService.procesar(p_idEntrada.trim());
                    if (portal != null) {
                        ua = new UsuarioAutenticado();
                        ua.setEmplid(portal.getEmplid());
                        ua.setUsuario(portal.getUsuario());
                        ua.setRoles(this.securityService.Roles(portal.getUsuario()));
                        autenticado = true;
                    }
                	//TODO ALEX: DESARROLLO PREMIZE:
                	/*ua = AuthenticationTest.getUsuarioAutenticado(hmaAuthService
    						.getUsuarioRole(p_idEntrada));
                	autenticado = true;*/
                } /*else if(!StringUtil.isEmptyOrNull(hmaToken)) {//HMA TOKEN LOGIC
                	try {
                		ua = hmaAuthService.doHmaAuthentication(hmaToken);
                		if(ua != null) {
                			autenticado = true;
                		}                		
                	} catch (ServiceException ex) {
                		LOG.error(ex.getMessage(), ex);
                	} catch (Exception ex) {
                		LOG.error(ex.getMessage(), ex);
                	}
                }*/
            }
            session.setAttribute("ua", ua);
            if (autenticado && ua != null)
            {
                ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
                List<String> roles = ua.getRoles();
                for (String rol : roles) {
                    grantedAuthorities.add(new SimpleGrantedAuthority(rol));
                }

                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(ua.getUsuario(), null, grantedAuthorities);
                SecurityContext securityContext = SecurityContextHolder.getContext();
                securityContext.setAuthentication(auth);
                session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            }


            chain.doFilter(request, response);

        }
        else
        {

            chain.doFilter(request, response);
        }

    }

}