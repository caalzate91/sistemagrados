package co.edu.javerianacali.grados.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
*
* @author jmrincon
*/
public class ImageCacheControlFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse resp = (HttpServletResponse) response;
		resp.setHeader("Cache-Control",	"max-age=290304000");
		chain.doFilter(request, response);
	}
	
	public void init(FilterConfig config) throws ServletException {
	}
	
	public void destroy() {

	}

}