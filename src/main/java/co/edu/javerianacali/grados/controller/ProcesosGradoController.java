package co.edu.javerianacali.grados.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import co.edu.javerianacali.grados.entities.ProcesosGrado;
import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.service.IProcesosGradoService;
import co.edu.javerianacali.grados.utils.JsonResponse;
import co.edu.javerianacali.grados.utils.JsonUtil;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.GridParam;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteController
 * @description
 * @date 8/10/2015
 */
@RequestMapping(value = "/parametro/grados")
@Controller
public class ProcesosGradoController  extends BaseController   {

	private static final Logger LOG = Logger
			.getLogger(ProcesosGradoController.class);

	@Autowired
	private IProcesosGradoService procesosGradoService;

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 8/10/2015
	 * @description
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws ServletException {
		Map<String, Object> viewParams = new HashMap<String, Object>();
		return new ModelAndView("/grados/grados", viewParams);
	}



	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 13/10/2015
	 * @description
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping(value = "/procesosGrado", method = RequestMethod.GET, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String consultarProcesosGrado(@RequestParam Map<String, String> params,
			HttpSession session, HttpServletRequest request) {
		JsonResponse<List<ProcesosGrado>> jsonResponse = new JsonResponse<List<ProcesosGrado>>();
  
		try {
			System.out.println("Aquí vamos bien");
			List<ProcesosGrado> procesosGrado = procesosGradoService.consultarProcesosGrado();
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
			jsonResponse.setResult(procesosGrado);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}
	
}

