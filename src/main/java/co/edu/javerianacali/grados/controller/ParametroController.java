package co.edu.javerianacali.grados.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import co.edu.javerianacali.grados.entities.Parametro;
import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.entities.TipoParametro;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.service.IParametroService;
import co.edu.javerianacali.grados.interfaces.service.ITipoParametroService;
import co.edu.javerianacali.grados.utils.JsonResponse;
import co.edu.javerianacali.grados.utils.JsonUtil;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.GridParam;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ParametroController
 * @description
 * @date 8/07/2015
 */
@RequestMapping(value = "/parametro/principal")
@Controller
public class ParametroController extends BaseController {

	private static final Logger LOG = Logger
			.getLogger(ParametroController.class);

	@Autowired
	private IParametroService parametroService;

	@Autowired
	private ITipoParametroService tipoParametroService;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws ServletException {
		Map<String, Object> viewParams = new HashMap<String, Object>();
		return new ModelAndView("/parametros/parametros", viewParams);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param pagingCriteria
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/getPagedListParametros", method = RequestMethod.GET, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String getPagedListParametros(
			@GridParam PagingCriteria pagingCriteria,
			HttpServletRequest request, HttpSession session) {
		JsonResponse<PagedList<Parametro>> jsonResponse = new JsonResponse<>();
		try {
			PagedList<Parametro> pagedList = parametroService
					.consultarParametros(pagingCriteria);
			jsonResponse.setResult(pagedList);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description
	 * @param params
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/buscarTipoParametros", method = RequestMethod.GET, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String buscarTipoParametros(
			@RequestParam Map<String, String> params,
			HttpServletRequest request, HttpSession session) {
		JsonResponse<List<TipoParametro>> jsonResponse = new JsonResponse<>();
		try {
			SearchCriteria criteria = new SearchCriteria();
			criteria.setValue(params.get("query"));
			criteria.setLimit(params.get("limit"));
			List<TipoParametro> result = tipoParametroService
					.buscarTipoParametros(criteria);
			jsonResponse.setResult(result);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 10/07/2015
	 * @description
	 * @param params
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/guardarParametro", method = RequestMethod.POST, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String guardarParametro(@RequestParam Map<String, String> params,
			HttpServletRequest request, HttpSession session) {
		JsonResponse<String> jsonResponse = new JsonResponse<>();
		try {
			Parametro parametro = new Parametro();
			BeanUtils.populate(parametro, params);
			parametroService.guardarParametro(parametro);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés
	 *         Rodríguez</a>
	 * @date 28/08/2015
	 * @description
	 * @param params
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/actualizarParametro", method = RequestMethod.POST, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String actualizarParametro(@RequestParam Map<String, String> params,
			HttpServletRequest request, HttpSession session) {
		JsonResponse<String> jsonResponse = new JsonResponse<>();
		try {
			Parametro parametro = new Parametro();
			BeanUtils.populate(parametro, params);
			parametroService.actualizarParametro(parametro);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);

		} catch (ServiceException e) {
			handleAppException(e, jsonResponse, LOG);
		} catch (Exception e) {
			handleException(e, jsonResponse, LOG);
		}
		return JsonUtil.toJson(jsonResponse);
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés
	 *         Rodríguez</a>
	 * @date 31/08/2015
	 * @description
	 * @param params
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/eliminarParametro", method = RequestMethod.POST, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String eliminarParametro(@RequestParam Map<String, String> params,
			HttpServletRequest request, HttpSession session) {
		JsonResponse<String> jsonResponse = new JsonResponse<>();
		try {
			String id = params.get("consecutivo");
			parametroService.eliminarParametro(id);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException e) {
			handleAppException(e, jsonResponse, LOG);
		} catch (Exception e) {
			handleException(e, jsonResponse, LOG);
		}
		return JsonUtil.toJson(jsonResponse);
	}
	


}
