package co.edu.javerianacali.grados.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ConstantesRM
 * @description Almacena las constantes para usar con {@link RequestMapping}
 * @date 8/07/2015
 */
public final class ConstantesRM {

	public static final String P_PRODUCES = "application/json; charset=utf-8";

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 */
	private ConstantesRM() {
	}

}
