package co.edu.javerianacali.grados.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import co.edu.javerianacali.grados.entities.Estudiante;
import co.edu.javerianacali.grados.entities.ProgramaEstudiante;
import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.interfaces.service.IEstudianteService;
import co.edu.javerianacali.grados.interfaces.service.IProgramaEstudianteService;
import co.edu.javerianacali.grados.utils.JsonResponse;
import co.edu.javerianacali.grados.utils.JsonUtil;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.GridParam;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteController
 * @description
 * @date 8/10/2015
 */
@RequestMapping(value = "/parametro/estudiante")
@Controller
public class EstudianteController  extends BaseController   {

	private static final Logger LOG = Logger
			.getLogger(EstudianteController.class);

	@Autowired
	private IEstudianteService estudianteService;
	@Autowired
	private IProgramaEstudianteService programaEstudianteService;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 8/10/2015
	 * @description
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws ServletException {
		Map<String, Object> viewParams = new HashMap<String, Object>();
		return new ModelAndView("/estudiante/estudiante", viewParams);
	}


	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 8/10/2015
	 * @description
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping(value = "/consultarEstudiante", method = RequestMethod.GET, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String consultarEstudiante(@RequestParam Map<String, String> params,
			HttpSession session, HttpServletRequest request) {
		JsonResponse<Estudiante> jsonResponse = new JsonResponse<Estudiante>();

		try {
			Estudiante estudiante = estudianteService.consultarEstudiante(
					params.get("empleidEstudiante"));
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
			jsonResponse.setResult(estudiante);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 13/10/2015
	 * @description
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping(value = "/consultarProgramaEstudiante", method = RequestMethod.GET, produces = ConstantesRM.P_PRODUCES)
	@ResponseBody
	public String consultarProgramaEstudiante(@RequestParam Map<String, String> params,
			HttpSession session, HttpServletRequest request) {
		JsonResponse<List<ProgramaEstudiante>> jsonResponse = new JsonResponse<List<ProgramaEstudiante>>();

		try {

			List<ProgramaEstudiante> programaEstudiante = programaEstudianteService.consultarProgramaEstudiante(
					params.get("empleidEstudiante"));
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
			jsonResponse.setResult(programaEstudiante);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}
	
	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan
	 *        Almache Montoya</a>
	 * @date 16/01/2015
	 * @description Guarda el estudiante para
	 *              el proceso de inscripción de candidato a grado
	 * @param emplid
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/guardarEstudiante", method = RequestMethod.POST)
	@ResponseBody
	public String guadarEstudiante(@RequestParam Map<String, String> params,
			HttpSession session, HttpServletRequest request) {
		JsonResponse<Estudiante> jsonResponse = new JsonResponse<Estudiante>();
		System.out.println("VAMOOOOOS");
		System.out.println(params.get("empleidEstudiante"));
		try {
			Estudiante estudiante = estudianteService.consultarEstudiante(
					params.get("empleidEstudiante"));
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
			jsonResponse.setResult(estudiante);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);
		} catch (ServiceException ex) {
			handleAppException(ex, jsonResponse, LOG);
		} catch (Exception ex) {
			handleException(ex, jsonResponse, LOG);
		}

		return JsonUtil.toJson(jsonResponse);
	}
	
}

