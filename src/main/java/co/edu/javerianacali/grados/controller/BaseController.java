package co.edu.javerianacali.grados.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.ModelAndView;

import co.edu.javerianacali.entities.UsuarioAutenticado;
import co.edu.javerianacali.grados.exception.core.AppException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.utils.BundleLoader;
import co.edu.javerianacali.grados.utils.JsonResponse;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class BaseController
 * @description
 * @date 16/01/2015
 */
public abstract class BaseController {

	private static final String USUARIO_AUTENTICADO = "ua";

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 21/01/2015
	 * @description
	 * @param errorCode
	 * @return
	 */
	protected String getServiceExceptionMessage(ErrorCode<String> errorCode) {
		return BundleLoader.getMessage(errorCode.getMessageBundle(),
				errorCode.getErrorCode(), errorCode.getParams(),
				LocaleContextHolder.getLocale());
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 26/02/2015
	 * @description
	 * @param request
	 * @return
	 */
	protected String getApplicationUrl(HttpServletRequest request) {
		StringBuffer appUri = new StringBuffer();
		appUri.append(request.getScheme());
		appUri.append("://");
		appUri.append(request.getServerName());
		appUri.append(":");
		appUri.append(request.getServerPort());
		appUri.append(request.getContextPath());
		appUri.append("/");
		return appUri.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/02/2015
	 * @description
	 * @param session
	 * @return
	 */
	protected UsuarioAutenticado getUsuarioAutenticado(HttpSession session) {
		if (session != null) {
			Object value = session.getAttribute(USUARIO_AUTENTICADO);
			if (value != null && (value instanceof UsuarioAutenticado)) {
				return (UsuarioAutenticado) value;
			}
		}
		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param session
	 * @return
	 */
	protected String getEmplidUsuarioAutenticado(HttpSession session) {
		if (session != null) {
			Object value = session.getAttribute(USUARIO_AUTENTICADO);
			if (value != null && (value instanceof UsuarioAutenticado)) {
				return ((UsuarioAutenticado) value).getEmplid();

			}
		}
		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 11/03/2015
	 * @description
	 * @param session
	 * @return
	 */
	protected String getRoleUsuarioAutenticado(HttpSession session) {
		if (session != null) {
			Object value = session.getAttribute(USUARIO_AUTENTICADO);
			if (value != null && (value instanceof UsuarioAutenticado)) {
				return ((UsuarioAutenticado) value).getRol();

			}
		}
		return null;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 10/03/2015
	 * @description
	 * @param titulo
	 * @param labelMessage
	 * @param message
	 * @return
	 */
	protected ModelAndView createErrorPage(String titulo, String labelMessage,
			String message) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("titulo", titulo);
		params.put("labelMessage", labelMessage);
		params.put("message", message);

		return new ModelAndView("errorPdf", params);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param fileName
	 * @param contentDisposition
	 * @param response
	 * @return
	 */
	protected HttpServletResponse setDefaultHeaderForPdf(String fileName,
			String contentDisposition, HttpServletResponse response) {
		StringBuilder content = new StringBuilder();
		content.append(contentDisposition);
		content.append("; filename=\"");
		content.append(fileName);
		content.append("\"");

		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setHeader("Content-Disposition", content.toString());

		return response;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 17/03/2015
	 * @description
	 * @param title
	 * @param message
	 * @return
	 */
	protected ModelAndView getErrorPageFragment(String title, String message) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		params.put("message", message);
		return new ModelAndView("errorFragment", params);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description Registra la excepción en el log y carga el argumento
	 *              <code>jsonResponse</code> con el mensaje de respuesta.
	 * @param exception
	 * @param jsonResponse
	 * @param log
	 */
	protected <T extends AppException> void handleAppException(T exception,
			JsonResponse<?> jsonResponse, Logger log) {
		log.error(exception.getMessage(), exception);
		jsonResponse.setStatus(JsonResponse.STATUS_ERROR);
		jsonResponse.setSuccess(false);
		jsonResponse.setExceptionMessage(getServiceExceptionMessage(exception
				.getErrorcode()));
		jsonResponse.setSeverity(exception.getErrorcode().getSeverity());
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description Registra la excepción en el log y carga el argumento
	 *              <code>jsonResponse</code> con el mensaje de respuesta.
	 * @param exception
	 * @param jsonResponse
	 * @param log
	 */
	protected <T extends Exception> void handleException(T exception,
			JsonResponse<?> jsonResponse, Logger log) {
		log.error(exception.getMessage(), exception);
		jsonResponse.setStatus(JsonResponse.STATUS_ERROR);
		jsonResponse.setSuccess(false);
		jsonResponse.setExceptionMessage(BundleLoader.getMessage(
				ConstantesController.I18N_ERROR_MESSAGES,
				ConstantesController.GENERAL_EXCEPTION_MSG, null,
				LocaleContextHolder.getLocale()));
		jsonResponse.setSeverity(ErrorCode.SeverityEnum.ERROR.name());
	}

}
