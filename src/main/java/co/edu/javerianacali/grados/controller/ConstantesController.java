package co.edu.javerianacali.grados.controller;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class ConstantesController
 * @description
 * @date 5/01/2015
 */
public final class ConstantesController {

	public static final String I18N_ERROR_MESSAGES = "i18n.error-messages";
	public static final String ILLEGAL_ARGUMENT_EXCEPTION_MSG = "ILLEGAL_ARGUMENT_EXCEPTION_MSG";
	public static final String GENERAL_EXCEPTION_MSG = "GENERAL_EXCEPTION_MSG";
	public static final String ESTUDIANTE_SESSION_ATTRIBUTE = "ESTUDIANTE_EMPLID";
	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/02/2015
	 */
	private ConstantesController() {
	}

}
