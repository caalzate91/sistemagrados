package co.edu.javerianacali.grados.persistence.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.edu.javerianacali.grados.entities.Estudiante;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class ParametroMapper
 * @description
 * @date 08/10/2015
 */
public class EstudianteMapper implements RowMapper<Estudiante> {

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 08/10/2015
	 * @param rs
	 * @param rowNum
	 * @return
	 * @throws SQLException
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	@Override
	public Estudiante mapRow(ResultSet rs, int rowNum) throws SQLException {
		Estudiante estudiante = new Estudiante();
		estudiante.setNombres(rs.getString("first_name"));
		estudiante.setApellidos(rs.getString("last_name"));
		estudiante.setDocumento(rs.getString("national_id"));
		estudiante.setTipoDocumento(rs.getString("national_id_type"));
		estudiante.setCorreo(rs.getString("correo"));
		estudiante.setTelefonoFijo(rs.getString("telefonoFijo"));
		estudiante.setDireccion(rs.getString("direccion"));
		estudiante.setCelular(rs.getString("celular"));
		estudiante.setEmpresa(rs.getString("empresa"));
		estudiante.setCargo(rs.getString("cargo"));		
		estudiante.setCorreoEmpresa(rs.getString("correoEmpresa"));	
		estudiante.setTelefonoOficina(rs.getString("telefonoOficina"));	
		
		return estudiante;
	}

}


