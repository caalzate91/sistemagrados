package co.edu.javerianacali.grados.persistence.mappers;

import java.sql.Types;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class EMapperFactory
 * @description Fabrica de mapeo de entidades - {@link EntityMapper}.
 * @date 11/01/2015
 */
public final class EMapperFactory {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 11/01/2015
	 */
	private EMapperFactory() {
	}

	private static final EntityMapper PARAMETRO;
	private static final EntityMapper TIPO_PARAMETRO;
	private static final EntityMapper ESTUDIANTE;
	private static final EntityMapper PROGRAMA_ESTUDIANTE;	
	private static final EntityMapper PROCESOS_GRADO;	
	
	static {
		// Mapping Parametro
		Map<String, AttributeMapping> map = new HashMap<String, AttributeMapping>();
		map.put("consecutivo", new AttributeMapping("consecutivo",
				"consecutivo", Types.NUMERIC));
		map.put("clave", new AttributeMapping("clave", "clave", Types.VARCHAR));
		map.put("valor", new AttributeMapping("valor", "valor", Types.VARCHAR));
		map.put("descripcion", new AttributeMapping("descripcion",
				"descripcion", Types.VARCHAR));
		map.put("ordenamiento", new AttributeMapping("ordenamiento",
				"ordenamiento", Types.NUMERIC));
		map.put("estado", new AttributeMapping("estado", "estado",
				Types.VARCHAR));
		map.put("tipoParametro", new AttributeMapping("tipoParametro",
				"tipo_parametro", Types.NUMERIC));
		PARAMETRO = new EntityMapper("PARAMETRO",
				Collections.unmodifiableMap(map));
		// Mapping TipoParametro
		map = new HashMap<String, AttributeMapping>();
		map.put("consecutivo", new AttributeMapping("consecutivo",
				"consecutivo", Types.NUMERIC));
		map.put("nombre", new AttributeMapping("nombre", "nombre",
				Types.VARCHAR));
		TIPO_PARAMETRO = new EntityMapper("TIPO_PARAMETRO",
				Collections.unmodifiableMap(map));
		// Mapping Estudiante
				map = new HashMap<String, AttributeMapping>();
				map.put("nombres", new AttributeMapping("nombres",
						"first_name", Types.VARCHAR));
				map.put("apellidos", new AttributeMapping("apellidos", "last_name",
						Types.VARCHAR));
				map.put("documento", new AttributeMapping("documento",
						"national_id", Types.VARCHAR));
				map.put("tipoDocumento", new AttributeMapping("tipoDocumento", "national_id_type",
						Types.VARCHAR));				
		ESTUDIANTE = new EntityMapper("ESTUDIANTE",
						Collections.unmodifiableMap(map));
		// Mapping ProgramaEstudiantes
		map = new HashMap<String, AttributeMapping>();
		map.put("nombrePrograma", new AttributeMapping("nombrePrograma",
				"prg_codigo", Types.VARCHAR));
		map.put("creditosRequeridos", new AttributeMapping("creditosAprobados", "creditos_aprobados",
				Types.INTEGER));
		map.put("creditosAprobados", new AttributeMapping("creditosRequeridos",
				"creditos_requeridos", Types.INTEGER));		
		PROGRAMA_ESTUDIANTE = new EntityMapper("PROGRAMAESTUDIANTE",
				Collections.unmodifiableMap(map));	
		// Mapping ProcesosGrado	
		map = new HashMap<String, AttributeMapping>();		
		map.put("id", new AttributeMapping("descripcion", "descripcion",
				Types.INTEGER));	
		map.put("descripcion", new AttributeMapping("descripcion",
				"descripcion", Types.VARCHAR));				
		PROCESOS_GRADO = new EntityMapper("PROCESOSGRADO",
				Collections.unmodifiableMap(map));

	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description AM = Attribute Mapping.
	 * @param attribute
	 *            - entity attribute
	 * @return {@link AttributeMapping}
	 */
	public static AttributeMapping getParametroAM(String attribute) {
		return PARAMETRO.getAttributeMapping().get(attribute);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description AM = Attribute Mapping.
	 * @param attribute
	 *            - entity attribute
	 * @return {@link AttributeMapping}
	 */
	public static AttributeMapping getTipoParametroAM(String attribute) {
		return TIPO_PARAMETRO.getAttributeMapping().get(attribute);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @return
	 */
	public static EntityMapper getParametroMap() {
		return PARAMETRO;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return the tipoParametroTable
	 */
	public static EntityMapper getTipoParametroMap() {
		return TIPO_PARAMETRO;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache
	 *        Montoya</a>
	 * @date 13/07/2015
	 * @return the tipoParametroTable
	 */
	public static EntityMapper getEstudianteMap() {
		return ESTUDIANTE;
	}
	
	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache
	 *        Montoya</a>
	 * @date 13/07/2015
	 * @return the tipoParametroTable
	 */
	public static EntityMapper getProgramaEstudianteMap() {
		return PROGRAMA_ESTUDIANTE;
	}
	
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *        Montoya</a>
	 * @date 19/10/2015
	 * @return the tipoParametroTable
	 */
	public static EntityMapper getProcesosGradoMap() {
		return PROCESOS_GRADO;
	} 
	
	
}
