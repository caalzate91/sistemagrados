package co.edu.javerianacali.grados.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import co.edu.javerianacali.grados.entities.Parametro;
import co.edu.javerianacali.grados.enums.DatabaseEnum;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.interfaces.persistence.IParametroDao;
import co.edu.javerianacali.grados.persistence.mappers.AttributeMapping;
import co.edu.javerianacali.grados.persistence.mappers.EMapperFactory;
import co.edu.javerianacali.grados.persistence.mappers.EntityMapper;
import co.edu.javerianacali.grados.persistence.mappers.ParametroMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;
import co.edu.javerianacali.grados.utils.gridview.SearchFilter;
import co.edu.javerianacali.grados.utils.gridview.SortField;
import co.edu.javerianacali.grados.utils.namedquery.INamedQuery;
import co.edu.javerianacali.grados.utils.namedquery.NamedTable;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ParametroDao
 * @description
 * @date 3/07/2015
 */
@Repository
public class ParametroDao extends Dao<Parametro, ParametroMapper> implements
		 IParametroDao {

	@Autowired
	private INamedQuery namedQuery;

	private EntityMapper entityMapper = EMapperFactory.getParametroMap();

	@Override
	public void guardarParametro(Parametro parametro) throws DaoException {
		/*
		 * List<NamedTable> namedTable = new ArrayList<>(); namedTable.add(new
		 * NamedTable(":parametro", DatabaseEnum.TABLE_PARAMETRO.getValue()));
		 * String queryString = namedQuery.getNamedQuery(
		 * NQConstants.NQ_INSERT_PARAMETRO, namedTable); MapSqlParameterSource
		 * namedParameters = new MapSqlParameterSource();
		 */
		Long consecutivo = (Long) getSequenceNextValOracle(
				DatabaseEnum.SEQUENCE_PARAMETRO.getValue(), Long.class);
		parametro.setConsecutivo(consecutivo);
		/*
		 * namedParameters.addValue("consecutivo", consecutivo);
		 * namedParameters.addValue("clave", parametro.getClave());
		 * namedParameters.addValue("valor", parametro.getValor());
		 * namedParameters.addValue("descripcion", parametro.getDescripcion());
		 * namedParameters.addValue("ordenamiento",
		 * parametro.getOrdenamiento()); namedParameters.addValue("estado",
		 * parametro.getEstado()); namedParameters.addValue("tipoParametro",
		 * parametro.getTipoParametro());
		 */

		save(parametro);
		// save(queryString, namedParameters);
	}

	@Override
	public void editarParametro(Parametro parametro) throws DaoException {
		List<NamedTable> namedTable = new ArrayList<>();
		namedTable.add(new NamedTable(":parametro",
				DatabaseEnum.TABLE_PARAMETRO.getValue()));
		String queryString = namedQuery.getNamedQuery(
				NQConstants.NQ_UPDATE_PARAMETRO, namedTable);
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("consecutivo", parametro.getConsecutivo());
		namedParameters.addValue("clave", parametro.getClave());
		namedParameters.addValue("valor", parametro.getValor());
		namedParameters.addValue("descripcion", parametro.getDescripcion());
		namedParameters.addValue("ordenamiento", parametro.getOrdenamiento());
		namedParameters.addValue("estado", parametro.getEstado());
		namedParameters.addValue("tipoParametro", parametro.getTipoParametro());

		update(queryString, namedParameters);
	}

	@Override
	public void eliminarParametroById(Long id) throws DaoException {
		List<NamedTable> namedTable = new ArrayList<>();
		namedTable.add(new NamedTable(":parametro",
				DatabaseEnum.TABLE_PARAMETRO.getValue()));
		String queryString = namedQuery.getNamedQuery(
				NQConstants.NQ_DELETE_PARAMETRO, namedTable);
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("consecutivo", id);

		update(queryString, namedParameters);
	}

	@Override
	public PagedList<Parametro> consultarParametros(
			PagingCriteria pagingCriteria) throws DaoException {
		List<NamedTable> namedTables = new ArrayList<>();
		namedTables.add(new NamedTable(":tParametro",
				DatabaseEnum.TABLE_PARAMETRO.getValue(), ":parametro",
				"parametro"));
		namedTables.add(new NamedTable(":tTtipoParametro",
				DatabaseEnum.TABLE_TIPO_PARAMETRO.getValue(), ":tipoParametro",
				"tipoParametro"));
		return getPagedList(
				namedQuery.getNamedQuery(NQConstants.NQ_CONSULTA_PARAMETRO),
				namedTables, pagingCriteria, new ParametroMapper());
	}

	@Override
	public String createQueryCriteria(String namedQueryString,
			MapSqlParameterSource queryParameters, PagingCriteria pagingCriteria)
			throws DaoException {
		StringBuilder queryString = new StringBuilder();
		queryString.append(namedQueryString);

		List<SortField> sortList = pagingCriteria.getSortFields();
		List<SearchFilter> filterList = pagingCriteria.getSearchFilters();
		for (SearchFilter filter : filterList) {
			queryString.append(buildFilter(":parametro", "AND", entityMapper, filter,
					queryParameters));
			/*
			 * if (!StringUtil.isEmpty(filter.getValue())) {
			 * queryString.append(" AND ");
			 * queryString.append(" LOWER(:parametro.");
			 * queryString.append(EMapperFactory.getParametroColumn(filter
			 * .getProperty())); queryString
			 * .append(String.format(") LIKE LOWER('%' || :%s || '%') ",
			 * filter.getProperty()));
			 * queryParameters.addValue(filter.getProperty(),
			 * filter.getValue()); }
			 */
		}

		if (!StringUtil.isEmpty(pagingCriteria.getGlobalSearch())) {
			queryParameters
					.addValue("search", pagingCriteria.getGlobalSearch());
			queryString
					.append(" AND (LOWER(:parametro.clave) LIKE LOWER('%' || :search || '%') ");
			queryString
					.append("  OR LOWER(:parametro.valor) LIKE LOWER('%' || :search || '%') ");
//			queryString.append("  OR LOWER(:parametro.consecutivo) LIKE LOWER('%' || :search || '%') ");
			queryString
					.append("  OR LOWER(:parametro.descripcion) LIKE LOWER('%' || :search || '%')) ");
		}

		queryString.append(getSortFields(sortList));

		return queryString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#getEntityMapper()
	 */
	@Override
	public EntityMapper getEntityMapper() {
		return entityMapper;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#entityWhereClause()
	 */
	@Override
	public String entityWhereClause() {
		AttributeMapping attr = entityMapper.getAtributeMapping("consecutivo");
		if (null == attr) {
			return "";
		}
		StringBuilder where = new StringBuilder();
		where.append(" WHERE ");
		where.append(attr.getColumn());
		where.append(" = :");
		where.append(attr.getAttribute());
		return where.toString();
	}

	// public String getLikeSqlCaseSensitive(String column, String value) {
	// column.
	//
	// }

}
