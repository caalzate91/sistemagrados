package co.edu.javerianacali.grados.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import co.edu.javerianacali.grados.entities.Estudiante;
import co.edu.javerianacali.grados.enums.DatabaseEnum;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.interfaces.persistence.IEstudianteDao;
import co.edu.javerianacali.grados.persistence.mappers.AttributeMapping;
import co.edu.javerianacali.grados.persistence.mappers.EMapperFactory;
import co.edu.javerianacali.grados.persistence.mappers.EntityMapper;
import co.edu.javerianacali.grados.persistence.mappers.EstudianteMapper;
import co.edu.javerianacali.grados.persistence.mappers.TipoParametroMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;
import co.edu.javerianacali.grados.utils.gridview.SearchFilter;
import co.edu.javerianacali.grados.utils.gridview.SortField;
import co.edu.javerianacali.grados.utils.namedquery.INamedQuery;
import co.edu.javerianacali.grados.utils.namedquery.NamedTable;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteDao
 * @description
 * @date 8/10/2015
 */
@Repository
public class EstudianteDao extends Dao<Estudiante, EstudianteMapper> implements
		IEstudianteDao {

	@Autowired
	private INamedQuery namedQuery;

	private EntityMapper entityMapper = EMapperFactory.getEstudianteMap();

	@Override
	public void guardarEstudiante(Estudiante estudiante) throws DaoException {
		/*
		 * Long consecutivo = (Long) getSequenceNextValOracle(
		 * DatabaseEnum.SEQUENCE_PARAMETRO.getValue(), Long.class);
		 * estudiante.setConsecutivo(consecutivo);
		 */

		save(estudiante);
	}

	@Override
	public void editarEstudiante(Estudiante estudiante) throws DaoException {
		/*
		 * List<NamedTable> namedTable = new ArrayList<>(); namedTable.add(new
		 * NamedTable(":parametro", DatabaseEnum.TABLE_PARAMETRO.getValue()));
		 * String queryString = namedQuery.getNamedQuery(
		 * NQConstants.NQ_UPDATE_PARAMETRO, namedTable); MapSqlParameterSource
		 * namedParameters = new MapSqlParameterSource();
		 * namedParameters.addValue("consecutivo", parametro.getConsecutivo());
		 * namedParameters.addValue("clave", parametro.getClave());
		 * namedParameters.addValue("valor", parametro.getValor());
		 * namedParameters.addValue("descripcion", parametro.getDescripcion());
		 * namedParameters.addValue("ordenamiento",
		 * parametro.getOrdenamiento()); namedParameters.addValue("estado",
		 * parametro.getEstado()); namedParameters.addValue("tipoParametro",
		 * parametro.getTipoParametro());
		 * 
		 * update(queryString, namedParameters);
		 */
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         </a>
	 * @date 8/10/2015
	 * @description
	 * @param descripcion
	 * @return
	 * @throws DaoException
	 */
	@Override
	public Estudiante consultarEstudiante(String empleidEstudiante)
			throws DaoException {
		String queryString = namedQuery
				.getNamedQuery(NQConstants.NQ_CONSULTA_ESTUDIANTE);

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("empleidEstudiante", empleidEstudiante);

		List<Estudiante> result = getNamedParameterJdbcTemplate().query(
				queryString, namedParameters, new EstudianteMapper());
		if (!result.isEmpty()) {
			return result.get(0);
		}
 
		return null;

	}

	@Override
	public String createQueryCriteria(String namedQueryString,
			MapSqlParameterSource queryParameters, PagingCriteria pagingCriteria)
			throws DaoException {
		StringBuilder queryString = new StringBuilder();
		queryString.append(namedQueryString);

		List<SortField> sortList = pagingCriteria.getSortFields();
		List<SearchFilter> filterList = pagingCriteria.getSearchFilters();
		for (SearchFilter filter : filterList) {
			queryString.append(buildFilter(":parametro", "AND", entityMapper,
					filter, queryParameters));
			/*
			 * if (!StringUtil.isEmpty(filter.getValue())) {
			 * queryString.append(" AND ");
			 * queryString.append(" LOWER(:parametro.");
			 * queryString.append(EMapperFactory.getParametroColumn(filter
			 * .getProperty())); queryString
			 * .append(String.format(") LIKE LOWER('%' || :%s || '%') ",
			 * filter.getProperty()));
			 * queryParameters.addValue(filter.getProperty(),
			 * filter.getValue()); }
			 */
		}

		if (!StringUtil.isEmpty(pagingCriteria.getGlobalSearch())) {
			queryParameters
					.addValue("search", pagingCriteria.getGlobalSearch());
			queryString
					.append(" AND (LOWER(:parametro.clave) LIKE LOWER('%' || :search || '%') ");
			queryString
					.append("  OR LOWER(:parametro.valor) LIKE LOWER('%' || :search || '%') ");
			// queryString.append("  OR LOWER(:parametro.consecutivo) LIKE LOWER('%' || :search || '%') ");
			queryString
					.append("  OR LOWER(:parametro.descripcion) LIKE LOWER('%' || :search || '%')) ");
		}

		queryString.append(getSortFields(sortList));

		return queryString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#getEntityMapper()
	 */
	@Override
	public EntityMapper getEntityMapper() {
		return entityMapper;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#entityWhereClause()
	 */
	@Override
	public String entityWhereClause() {
		AttributeMapping attr = entityMapper.getAtributeMapping("consecutivo");
		if (null == attr) {
			return "";
		}
		StringBuilder where = new StringBuilder();
		where.append(" WHERE ");
		where.append(attr.getColumn());
		where.append(" = :");
		where.append(attr.getAttribute());
		return where.toString();
	}

	// public String getLikeSqlCaseSensitive(String column, String value) {
	// column.
	//
	// }

}
