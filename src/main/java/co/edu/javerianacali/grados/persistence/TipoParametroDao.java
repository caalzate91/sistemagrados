package co.edu.javerianacali.grados.persistence;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.entities.TipoParametro;
import co.edu.javerianacali.grados.enums.DatabaseEnum;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.interfaces.persistence.ITipoParametroDao;
import co.edu.javerianacali.grados.persistence.mappers.EMapperFactory;
import co.edu.javerianacali.grados.persistence.mappers.EntityMapper;
import co.edu.javerianacali.grados.persistence.mappers.TipoParametroMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.namedquery.INamedQuery;
import co.edu.javerianacali.grados.utils.namedquery.NamedQueryEnum;
import co.edu.javerianacali.grados.utils.namedquery.NamedTable;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class TipoParametroDao
 * @description
 * @date 9/07/2015
 */
@Repository
public class TipoParametroDao extends Dao<TipoParametro, TipoParametroMapper>
		implements ITipoParametroDao {

	@Autowired
	private INamedQuery namedQuery;

	private EntityMapper entityMapper = EMapperFactory.getTipoParametroMap();

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @param searchCriteria
	 * @return
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.ITipoParametroDao#autoCompleteSearch(co.edu.javerianacali.grados.entities.SearchCriteria)
	 */
	@Override
	public List<TipoParametro> autocompleteSearch(SearchCriteria searchCriteria)
			throws DaoException {
		List<NamedTable> namedTables = new ArrayList<>();
		namedTables.add(new NamedTable(":tTipoParametro",
				DatabaseEnum.TABLE_TIPO_PARAMETRO.getValue(), ":tipoParametro",
				"tipoParametro"));
		String nquery = namedQuery.getNamedQuery(
				NQConstants.NQ_CONSULTA_TIPO_PARAMETRO,
				NamedQueryEnum.SELECT_FROM_CLAUSE);
		StringBuilder queryString = new StringBuilder();
		queryString.append(nquery);
		queryString
				.append("WHERE (LOWER(:tipoParametro.nombre) LIKE LOWER('%' || :value || '%')) ");
		// + "--(:tipoParametro.consecutivo LIKE LOWER('%'||:value||'%')");
		// queryString
		// .append(" --OR :tipoParametro.nombre LIKE LOWER('%'||:value||'%'))");

		StringBuilder finalQuery = new StringBuilder();
		if (!StringUtil.isEmpty(searchCriteria.getLimit())) {
			finalQuery.append(getLimitQuery(queryString.toString()));
		} else {
			finalQuery.append(queryString.toString());
		}

		nquery = namedQuery.getQueryFromNamedQuery(finalQuery.toString(),
				namedTables);

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("value", searchCriteria.getValue(),
				Types.VARCHAR);
		namedParameters.addValue("limit", searchCriteria.getLimit(),
				Types.NUMERIC);

		return getNamedParameterJdbcTemplate().query(nquery, namedParameters,
				new TipoParametroMapper());
	}

	@Override
	public String createQueryCriteria(String namedQueryString,
			MapSqlParameterSource queryParameters, PagingCriteria pagingCriteria)
			throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#getEntityMapper()
	 */
	@Override
	public EntityMapper getEntityMapper() {
		return entityMapper;
	}

	@Override
	public String entityWhereClause() {
		// TODO Auto-generated method stub
		return null;
	}

}
