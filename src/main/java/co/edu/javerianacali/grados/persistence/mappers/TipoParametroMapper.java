package co.edu.javerianacali.grados.persistence.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.edu.javerianacali.grados.entities.TipoParametro;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class TipoParametroMapper
 * @description
 * @date 9/07/2015
 */
public class TipoParametroMapper implements RowMapper<TipoParametro> {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @param rs
	 * @param rowNum
	 * @return
	 * @throws SQLException
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	@Override
	public TipoParametro mapRow(ResultSet rs, int rowNum) throws SQLException {
		TipoParametro tipoParametro = new TipoParametro();
		tipoParametro.setConsecutivo(rs.getLong("consecutivo"));
		tipoParametro.setNombre(rs.getString("nombre"));
		return tipoParametro;
	}

}
