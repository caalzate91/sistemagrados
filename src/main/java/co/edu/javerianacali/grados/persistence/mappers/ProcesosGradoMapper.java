package co.edu.javerianacali.grados.persistence.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.edu.javerianacali.grados.entities.ProcesosGrado;

public class ProcesosGradoMapper implements RowMapper<ProcesosGrado> {

	@Override
	public ProcesosGrado mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProcesosGrado procesosGrado = new ProcesosGrado();
		procesosGrado.setIdProcesoGrado(rs.getInt("consecutivo"));
		procesosGrado.setDescripcion(rs.getString("descripcion"));
	
		return procesosGrado;
	}
}

