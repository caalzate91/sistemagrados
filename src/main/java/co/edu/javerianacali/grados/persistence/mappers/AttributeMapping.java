package co.edu.javerianacali.grados.persistence.mappers;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class AttributeMapping
 * @description
 * @date 13/07/2015
 */
public class AttributeMapping {

	private final String attribute;
	private final String column;
	private final int sqlType;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param attribute
	 * @param column
	 * @param sqlType
	 */
	public AttributeMapping(String attribute, String column, int sqlType) {
		super();
		this.attribute = attribute;
		this.column = column;
		this.sqlType = sqlType;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return the column
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return the sqlType
	 */
	public int getSqlType() {
		return sqlType;
	}

}
