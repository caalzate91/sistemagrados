package co.edu.javerianacali.grados.persistence.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.edu.javerianacali.grados.entities.ProgramaEstudiante;

public class ProgramaEstudianteMapper implements RowMapper<ProgramaEstudiante> {

	@Override
	public ProgramaEstudiante mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProgramaEstudiante programaEstudiante = new ProgramaEstudiante();
		programaEstudiante.setNombrePrograma(rs.getString("prg_codigo"));		
		programaEstudiante.setCreditosAprobados(rs.getInt("creditos_aprobados"));
		programaEstudiante.setCreditosRequeridos(rs.getInt("creditos_requeridos"));
		
		/// Alex.
		programaEstudiante.setFlagInscripcion(rs.getBoolean("flagInscripcion"));
		
		return programaEstudiante;
	}
}
