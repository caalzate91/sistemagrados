package co.edu.javerianacali.grados.persistence;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class NQConstants
 * @description Clase para almacenar los nombres de los querys nombrados.
 * @date 4/02/2015
 */
public final class NQConstants {

	public static final String NQ_INSERT_PARAMETRO = "insertParametro";
	public static final String NQ_UPDATE_PARAMETRO = "updateParametro";
	public static final String NQ_DELETE_PARAMETRO = "deleteParametro";
	public static final String NQ_CONSULTA_PARAMETRO = "consultaParametro";
	public static final String NQ_CONSULTA_TIPO_PARAMETRO = "consultaTipoParametro";
	public static final String NQ_CONSULTA_ESTUDIANTE = "consultaEstudiante";
	public static final String NQ_CONSULTA_PROGRAMA_ESTUDIANTE = "consultaProgramaEstudiante";
	public static final String NQ_CONSULTA_PROCESO_GRADO = "consultaProcesosGrado";
	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 4/02/2015
	 */
	private NQConstants() {
	}
}
