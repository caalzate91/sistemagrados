/**
 * 
 */
package co.edu.javerianacali.grados.persistence;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import co.edu.javerianacali.grados.enums.ComparisonEnum;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.exception.core.ErrorCode;
import co.edu.javerianacali.grados.exception.core.InvalidAttributeException;
import co.edu.javerianacali.grados.exception.core.ErrorCode.SeverityEnum;
import co.edu.javerianacali.grados.interfaces.persistence.IDao;
import co.edu.javerianacali.grados.persistence.mappers.AttributeMapping;
import co.edu.javerianacali.grados.persistence.mappers.EntityMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;
import co.edu.javerianacali.grados.utils.gridview.SearchFilter;
import co.edu.javerianacali.grados.utils.gridview.SortField;
import co.edu.javerianacali.grados.utils.namedquery.INamedQuery;
import co.edu.javerianacali.grados.utils.namedquery.NamedTable;
import co.edu.javerianacali.jdbc.NamedJdbcDao;

/**
 * 
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class Dao
 * @description
 * @date 6/07/2015
 */
@Transactional(value = "appTransactionManager", propagation = Propagation.MANDATORY)
public abstract class Dao<T, M extends RowMapper<T>> extends NamedJdbcDao
		implements IDao<T, M> {

	private static final String I18N_DAO_ERROR_MESSAGES = "18n.dao-error-messages";
	private static final String EC_DAO_SAVE_QUERY_NULL = "DAO_SAVE_QUERY_NULL";
	private static final String EC_DAO_SAVE_ENTITY_NULL = "DAO_SAVE_ENTITY_NULL";
	private static final String EC_DAO_SAVE_INVALID_INSERT = "DAO_SAVE_INVALID_INSERT";
	private static final String EC_DAO_SAVE_ENTITY_NOT_INSERTED = "DAO_SAVE_ENTITY_NOT_INSERTED";
	private static final String EC_DAO_UPDATE_ENTITY_NULL = "EC_DAO_UPDATE_ENTITY_NULL";
	private static final String EC_DAO_UPDATE_ENTITY_NOT_UPDATED = "DAO_UPDATE_ENTITY_NOT_UPDATED";
	private static final String EC_DAO_DELETE_ENTITY_NOT_DELETED = "DAO_UPDATE_ENTITY_NOT_DELETED";
	private static final String EC_DAO_DELETE_ENTITY_NULL = "EC_DAO_DELETE_ENTITY_NULL";
	private static final String EC_DAO_SELECT_ENTITY_NULL = "EC_DAO_SELECT_ENTITY_NULL";
	private static final String EC_DAO_INVALID_WHERE_CLAUSE = "EC_DAO_INVALID_WHERE_CLAUSE";
	private static final String EC_DAO_DATA_ACCESS_EX = "EC_DAO_DATA_ACCESS_EX";
	private static final String EC_DAO_INVALID_ATTRIBUTE = "EC_DAO_INVALID_ATTRIBUTE";
	private static final String SQL_INSERT = "insert";
	private static final String SQL_WHERE = "where";
	// private static final String SQL_UPDATE = "update";

	private static final Logger LOG = Logger.getLogger(Dao.class);

	private String insertStatement;
	private String updateStatement;
	private String deleteStatement;
	private String selectStatement;

	@Autowired
	private INamedQuery namedQuery;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param entity
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#save(java.lang.Object)
	 */
	public void save(T entity) throws DaoException {
		Object[] params = {};
		if (null == entity) {
			throw new DaoException(
					"Error al guardar la entidad, el objeto 'entity' es nulo",
					new ErrorCode<String>(EC_DAO_SAVE_ENTITY_NULL,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					createInsertStatement(getEntityMapper()), namedParameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al guardar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_SAVE_ENTITY_NOT_INSERTED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description Construye la sentencia sql insert. La sentencia se construye
	 *              una sola vez y de ahi en adelante reutiliza la sentencia.
	 * @param entityMapper
	 * @return
	 */
	private String createInsertStatement(EntityMapper entityMapper) {
		if (null == insertStatement) {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO ");
			sql.append(entityMapper.getTable());
			sql.append("(");
			StringBuilder columns = new StringBuilder();
			StringBuilder namedValues = new StringBuilder();
			for (Map.Entry<String, AttributeMapping> entry : entityMapper
					.getAttributeMapping().entrySet()) {
				columns.append(entry.getValue().getColumn());
				columns.append(",");

				namedValues.append(":");
				namedValues.append(entry.getValue().getAttribute());
				namedValues.append(",");
			}
			columns.deleteCharAt(columns.length() - 1);
			namedValues.deleteCharAt(namedValues.length() - 1);
			sql.append(columns);
			sql.append(") VALUES (");
			sql.append(namedValues);
			sql.append(")");
			insertStatement = sql.toString();
		}
		return insertStatement;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param sql
	 * @param entity
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#save(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public void save(String sql, T entity) throws DaoException {
		Object[] params = {};
		if (null == entity) {
			throw new DaoException(
					"Error al guardar la entidad, el objeto 'entity' es nulo",
					new ErrorCode<String>(EC_DAO_SAVE_ENTITY_NULL,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		if (StringUtil.isEmpty(sql)) {
			throw new DaoException(
					"Error al guardar la entidad, la cadena sql es nula",
					new ErrorCode<String>(EC_DAO_SAVE_QUERY_NULL,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		if (!sql.toLowerCase().contains(SQL_INSERT)) {
			throw new DaoException(
					"Error al guardar la entidad, el sql no es valido :" + sql,
					new ErrorCode<String>(EC_DAO_SAVE_INVALID_INSERT,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			int rowsAffected = getNamedParameterJdbcTemplate().update(sql,
					namedParameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al guardar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_SAVE_ENTITY_NOT_INSERTED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param sql
	 * @param parameters
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#save(java.lang.String,
	 *      org.springframework.jdbc.core.namedparam.SqlParameterSource)
	 */
	@Override
	public void save(String sql, SqlParameterSource parameters)
			throws DaoException {
		Object[] params = {};

		if (StringUtil.isEmpty(sql)) {
			throw new DaoException(
					"Error al guardar la entidad, la cadena sql es nula",
					new ErrorCode<String>(EC_DAO_SAVE_QUERY_NULL,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		if (!sql.toLowerCase().contains(SQL_INSERT)) {
			throw new DaoException(
					"Error al guardar la entidad, el sql no es valido :" + sql,
					new ErrorCode<String>(EC_DAO_SAVE_INVALID_INSERT,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		try {
			int rowsAffected = getNamedParameterJdbcTemplate().update(sql,
					parameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al guardar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_SAVE_ENTITY_NOT_INSERTED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * Actualiza una entidad de tipo {@link T}.
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param entity
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#update(java.lang.Object)
	 */
	@Override
	public void update(T entity) throws DaoException {
		Object[] params = {};
		if (null == entity) {
			throw new DaoException(
					"Error al actualizar la entidad, el objeto 'entity' es nulo",
					new ErrorCode<String>(EC_DAO_UPDATE_ENTITY_NULL,
							I18N_DAO_ERROR_MESSAGES,
							ErrorCode.SeverityEnum.ERROR, params));
		}

		if (StringUtil.isEmpty(entityWhereClause())
				|| !entityWhereClause().contains(SQL_WHERE)) {
			throwInvalidWhereCluaseException("Actualizar");
		}

		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					createUpdateStatement(getEntityMapper()), namedParameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al actualizar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_UPDATE_ENTITY_NOT_UPDATED,
								I18N_DAO_ERROR_MESSAGES, SeverityEnum.ERROR,
								params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param entityMapper
	 * @return
	 */
	private String createUpdateStatement(EntityMapper entityMapper) {
		if (null == updateStatement) {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE ");
			sql.append(entityMapper.getTable());
			sql.append(" SET (");
			StringBuilder columns = new StringBuilder();
			for (Map.Entry<String, AttributeMapping> entry : entityMapper
					.getAttributeMapping().entrySet()) {
				columns.append(entry.getValue().getColumn());
				columns.append(" = :");
				columns.append(entry.getValue().getAttribute());
				columns.append(", ");
			}
			// Elimina la ultuma (,)
			columns.deleteCharAt(columns.length() - 1);

			sql.append(columns);
			sql.append(") ");
			sql.append(entityWhereClause());
			updateStatement = sql.toString();
		}
		return updateStatement;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param sqlUpdate
	 * @param parameters
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#update(java.lang.String,
	 *      org.springframework.jdbc.core.namedparam.SqlParameterSource)
	 */
	@Override
	public void update(String sqlUpdate, SqlParameterSource parameters)
			throws DaoException {
		Object[] params = {};
		try {
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					sqlUpdate, parameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al actualizar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_UPDATE_ENTITY_NOT_UPDATED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param sqlUpdate
	 * @param entity
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#update(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public void update(String sqlUpdate, T entity) throws DaoException {
		Object[] params = {};
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					sqlUpdate, namedParameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al actualizar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_UPDATE_ENTITY_NOT_UPDATED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param entity
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#delete(java.lang.Object)
	 */
	@Override
	public void delete(T entity) throws DaoException {
		Object[] params = {};
		if (null == entity) {
			throw new DaoException(
					"Error al eliminar la entidad, el objeto 'entity' es nulo",
					new ErrorCode<String>(EC_DAO_DELETE_ENTITY_NULL,
							I18N_DAO_ERROR_MESSAGES, SeverityEnum.ERROR, params));
		}

		if (StringUtil.isEmpty(entityWhereClause())
				|| !entityWhereClause().contains(SQL_WHERE)) {
			throwInvalidWhereCluaseException("Eliminar");
		}

		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					createDeleteStatement(getEntityMapper()), namedParameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al eliminar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_DELETE_ENTITY_NOT_DELETED,
								I18N_DAO_ERROR_MESSAGES, SeverityEnum.ERROR,
								params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description Crea la sentecia de eliminar.
	 * @param entityMapper
	 * @return
	 */
	private String createDeleteStatement(EntityMapper entityMapper) {
		if (null == deleteStatement) {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM ");
			sql.append(entityMapper.getTable());
			sql.append(" ");
			sql.append(entityWhereClause());
			deleteStatement = sql.toString();
		}
		return deleteStatement;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param sqlDelete
	 * @param entity
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#delete(java.lang.String,
	 *      java.lang.Object)
	 */
	@Override
	public void delete(String sqlDelete, T entity) throws DaoException {
		Object[] params = {};
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					sqlDelete, namedParameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al eliminar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_DELETE_ENTITY_NOT_DELETED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param sqlDelete
	 * @param parameters
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#delete(java.lang.String,
	 *      org.springframework.jdbc.core.namedparam.SqlParameterSource)
	 */
	@Override
	public void delete(String sqlDelete, SqlParameterSource parameters)
			throws DaoException {
		Object[] params = {};
		try {
			int rowsAffected = getNamedParameterJdbcTemplate().update(
					sqlDelete, parameters);
			if (rowsAffected == 0) {
				throw new DaoException(
						"Error al eliminar la entidad, filas afectadas = 0",
						new ErrorCode<String>(EC_DAO_DELETE_ENTITY_NOT_DELETED,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR, params));
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					ErrorCode.SeverityEnum.ERROR, params), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param entity
	 * @param mapper
	 * @return
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#getSimple(java.lang.Object,
	 *      org.springframework.jdbc.core.RowMapper)
	 */
	@Override
	public T getSimple(T entity, M mapper) throws DaoException {
		Object[] params = {};
		if (null == entity) {
			throw new DaoException(
					"Error al consultar la entidad, el objeto 'entity' es nulo",
					new ErrorCode<String>(EC_DAO_SELECT_ENTITY_NULL,
							I18N_DAO_ERROR_MESSAGES, SeverityEnum.ERROR, params));
		}

		if (StringUtil.isEmpty(entityWhereClause())
				|| !entityWhereClause().contains(SQL_WHERE)) {
			throwInvalidWhereCluaseException("Consultar");
		}

		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(
					entity);
			List<T> result = getNamedParameterJdbcTemplate().query(
					createSelectStatement(getEntityMapper()), namedParameters,
					mapper);
			if (!result.isEmpty()) {
				return result.get(0);
			}
		} catch (DataAccessException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new DaoException(ex.getMessage(), new ErrorCode<String>(
					EC_DAO_DATA_ACCESS_EX, I18N_DAO_ERROR_MESSAGES,
					SeverityEnum.ERROR, params), ex);
		}
		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param entityMapper
	 * @return
	 */
	private String createSelectStatement(EntityMapper entityMapper) {
		if (null == selectStatement) {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			StringBuilder columns = new StringBuilder();
			for (Map.Entry<String, AttributeMapping> entry : entityMapper
					.getAttributeMapping().entrySet()) {
				columns.append(entry.getValue().getColumn());
				columns.append(" ");
				columns.append(entry.getValue().getAttribute());
				columns.append(", ");
			}
			// Elimina la ultuma (,)
			columns.deleteCharAt(columns.length() - 1);

			sql.append(columns);
			sql.append("FROM ");
			sql.append(entityMapper.getTable());
			sql.append(" ");
			sql.append(entityWhereClause());
			selectStatement = sql.toString();
		}

		return selectStatement;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param query
	 * @param parameters
	 * @param mapper
	 * @return
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#get(java.lang.String,
	 *      org.springframework.jdbc.core.namedparam.SqlParameterSource,
	 *      org.springframework.jdbc.core.RowMapper)
	 */
	@Override
	public T get(String query, SqlParameterSource parameters, M mapper)
			throws DaoException {
		List<T> result = getNamedParameterJdbcTemplate().query(query,
				parameters, mapper);
		if (!result.isEmpty()) {
			return result.get(0);
		}

		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param query
	 * @param parameters
	 * @param mapper
	 * @return
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#getEntity(java.lang.String,
	 *      org.springframework.jdbc.core.namedparam.SqlParameterSource,
	 *      org.springframework.jdbc.core.RowMapper)
	 */
	@Override
	public <E extends RowMapper<T>> T getEntity(String query,
			SqlParameterSource parameters, E mapper) throws DaoException {
		List<T> result = getNamedParameterJdbcTemplate().query(query,
				parameters, mapper);
		if (!result.isEmpty()) {
			return result.get(0);
		}

		return null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description Lanza una exepcion del tipo {@link DaoException} cuando la
	 *              clausula where es invalida al realizar las operaciones CRUD:
	 *              Read, Update o Delete.
	 * @param operation
	 * @throws DaoException
	 */
	private void throwInvalidWhereCluaseException(String operation)
			throws DaoException {
		Object[] params = { operation, entityWhereClause() };
		throw new DaoException(String.format(
				"Error al %s la entidad, clausula where invalida, '%s'",
				operation, entityWhereClause()), new ErrorCode<String>(
				EC_DAO_INVALID_WHERE_CLAUSE, I18N_DAO_ERROR_MESSAGES,
				SeverityEnum.ERROR, params));
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/07/2015
	 * @param query
	 * @param namedTables
	 * @param pagingCriteria
	 * @param mapper
	 * @return {@link PagedList}
	 * @throws DaoException
	 * @see co.edu.javerianacali.grados.interfaces.persistence.IDao#getPagedList(java.lang.String,
	 *      java.util.List,
	 *      co.edu.javerianacali.grados.utils.PagingCriteria,
	 *      org.springframework.jdbc.core.RowMapper)
	 */
	@Override
	public PagedList<T> getPagedList(String query,
			List<NamedTable> namedTables, PagingCriteria pagingCriteria,
			M mapper) throws DaoException {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("start", pagingCriteria.getStart());
		namedParameters.addValue("limit", pagingCriteria.getLimit());
		String nqueryString = createQueryCriteria(query, namedParameters,
				pagingCriteria);
		String baseQuery = namedQuery.getQueryFromNamedQuery(nqueryString,
				namedTables);

		List<T> records = getNamedParameterJdbcTemplate().query(
				createPagedQuery(baseQuery, pagingCriteria.getStart(),
						pagingCriteria.getLimit(), false), namedParameters,
				mapper);

		Long count = getNamedParameterJdbcTemplate().queryForObject(
				createCountQuery(baseQuery), namedParameters, Long.class);

		return new PagedList<T>(records, records.size(), count);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/07/2015
	 * @description
	 * @param namedQueryString
	 * @param pagingCriteria
	 * @return
	 * @throws DaoException
	 */
	public abstract String createQueryCriteria(String namedQueryString,
			MapSqlParameterSource queryParameters, PagingCriteria pagingCriteria)
			throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @return
	 */
	public abstract EntityMapper getEntityMapper();

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description condicion where para utilizar el las operaciones de
	 *              actualizar, eliminar y consultar una entidad
	 * @return
	 */
	public abstract String entityWhereClause();

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sequenceName
	 * @return
	 */
	// public abstract Number getSequenceNextVal(String sequenceName);

	/**
	 * 
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/01/2015
	 * @description
	 * @param sortFields
	 * @return
	 */
	public String getSortFields(List<SortField> sortFields) {
		StringBuilder querySort = new StringBuilder();
		if (sortFields != null && !sortFields.isEmpty()) {
			querySort.append(" ORDER BY ");
			for (int i = 0; i < sortFields.size(); i++) {
				querySort.append(sortFields.get(i).getProperty());
				querySort.append(" ");
				querySort.append(sortFields.get(i).getDirection());
				if ((i + 1) != sortFields.size()) {
					querySort.append(", ");
				}
			}
		}
		return querySort.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 14/01/2015
	 * @description
	 * @param query
	 * @return
	 */
	public static String createCountQuery(String query) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT COUNT(*) FROM (");
		queryString.append(query);
		queryString.append(") ");
		return queryString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 14/01/2015
	 * @description
	 * @param query
	 * @param start
	 * @param limit
	 * @return
	 */
	public String createPagedQuery(String query, Integer start, Integer limit,
			boolean forFilter) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT * FROM ");
		queryString.append(" (SELECT A.*, ROWNUM rnum FROM (");
		queryString.append(query);
		queryString.append(") A WHERE ROWNUM <= :limit ");
		// queryString.append(limit);
		queryString.append(") ");
		queryString.append(" WHERE (rnum > :start) ");
		// queryString.append(start);
		// if (forFilter) {
		// queryString.append(" OR ");
		// } else {
		// queryString.append(" AND ");
		// }
		// queryString.append(" rnum <= :limit ");
		// queryString.append(limit);
		// queryString.append(")");
		return queryString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 10/07/2015
	 * @description
	 * @param sequenceName
	 *            - Nombre de la secuencia
	 * @param type
	 *            - Valor numerico de la secuencia del tipo {@link Number}
	 * @return sequence nextval
	 */
	public <D extends Number> Number getSequenceNextValOracle(
			String sequenceName, Class<D> type) {
		String queryString = String.format("SELECT %s FROM dual",
				getSqlSequenceNextVal(sequenceName));
		return this.getJdbcTemplate().queryForObject(queryString, type);
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez </a>
	 * @date 8/01/2015
	 * @description
	 * @param sequence
	 *            - Nombre de la secuencia
	 * @return sequence .NEXTVAL
	 */
	public String getSqlSequenceNextVal(String sequence) {
		StringBuilder sequencesString = new StringBuilder();
		sequencesString.append(sequence);
		sequencesString.append(".NEXTVAL");

		return sequencesString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description Crea un query con limite.
	 * @param query
	 * @return
	 */
	public String getLimitQuery(String query) {
		StringBuilder qbuilder = new StringBuilder();
		qbuilder.append("SELECT * FROM (");
		qbuilder.append(query);
		qbuilder.append(") WHERE ROWNUM <= :limit ");

		return qbuilder.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param tableAlias
	 * @param andOr
	 *            - SQL 'AND', 'OR'
	 * @param entityMapper
	 * @param filter
	 * @param parameters
	 * @return
	 * @throws InvalidAttributeException
	 */
	public String buildFilter(String tableAlias, String andOr,
			EntityMapper entityMapper, SearchFilter filter,
			MapSqlParameterSource parameters) throws InvalidAttributeException {
		if (null == filter) {
			throw new IllegalArgumentException(
					"buildFilter(...) : filter es nulo");
		}
		if (null == entityMapper) {
			throw new IllegalArgumentException(
					"buildFilter(...) : entityMapper es nulo");
		}
		StringBuilder sql = new StringBuilder();
		if (!StringUtil.isEmpty(filter.getProperty())
				&& !StringUtil.isEmpty(filter.getValue())) {
			sql.append(" ");
			sql.append(andOr);
			sql.append(" ");
			AttributeMapping attributeMapping = entityMapper
					.getAttributeMapping().get(filter.getProperty());
			if (null == attributeMapping) {
				throw new InvalidAttributeException(
						String.format(
								"El atributo '%s' no es un atributo valido de la entidad",
								filter.getProperty()), new ErrorCode<String>(
								EC_DAO_INVALID_ATTRIBUTE,
								I18N_DAO_ERROR_MESSAGES,
								ErrorCode.SeverityEnum.ERROR));
			}

			if (attributeMapping.getSqlType() == Types.VARCHAR) {
				sql.append("LOWER(");
				sql.append(tableAlias);
				sql.append(".");
				sql.append(attributeMapping.getColumn()); // TODO Validate null
															// cases.
				sql.append(") ");
				sql.append(getSqlComparison(filter));
				sql.append(" ('%' || ");
				sql.append(":" + filter.getProperty());
				sql.append(" || '%') ");
			} else {
				sql.append(tableAlias);
				sql.append(".");
				sql.append(attributeMapping.getColumn()); // TODO Validate null
															// cases.
				sql.append(" ");
				sql.append(getSqlComparison(filter));
				sql.append(" ");
				sql.append(":" + filter.getProperty());
				sql.append(" ");
			}

			parameters.addValue(filter.getProperty(), filter.getValue()
					.toLowerCase());
		}
		return sql.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/07/2015
	 * @description
	 * @param filter
	 * @return
	 */
	private String getSqlComparison(SearchFilter filter) {
		String result = ComparisonEnum.EQ.getValue();
		if (filter != null && !StringUtil.isEmpty(filter.getComparison())) {
			try {
				switch (ComparisonEnum.valueOf(filter.getComparison()
						.toUpperCase())) {
				case LQ:
					result = ComparisonEnum.LQ.getValue();
					break;
				case GT:
					result = ComparisonEnum.GT.getValue();
					break;
				case LT:
					result = ComparisonEnum.LT.getValue();
					break;
				default:
					break;
				}
			} catch (IllegalArgumentException ex) {
				LOG.error(ex.getMessage(), ex);
			}
		}

		return result;
	}

}
