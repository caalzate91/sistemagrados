package co.edu.javerianacali.grados.persistence.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import co.edu.javerianacali.grados.entities.Parametro;
import co.edu.javerianacali.grados.entities.TipoParametro;
import co.edu.javerianacali.grados.enums.EstadoParametroEnum;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class ParametroMapper
 * @description
 * @date 27/11/2014
 */
public class ParametroMapper implements RowMapper<Parametro> {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param rs
	 * @param rowNum
	 * @return
	 * @throws SQLException
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	@Override
	public Parametro mapRow(ResultSet rs, int rowNum) throws SQLException {
		Parametro parametro = new Parametro();
		parametro.setConsecutivo(rs.getLong("consecutivo"));
		parametro.setClave(rs.getString("clave"));
		parametro.setValor(rs.getString("valor"));
		parametro.setDescripcion(rs.getString("descripcion"));
		parametro.setOrdenamiento(rs.getInt("ordenamiento"));
		parametro.setEstadoEnum(EstadoParametroEnum.valueOf(
				EstadoParametroEnum.class, rs.getString("estado")));
		parametro.setTipoParametro(rs.getLong("tipoParametro"));
		parametro.setEstado(parametro.getEstadoEnum().name());
		
		TipoParametro tipoParametro = new TipoParametro();
		tipoParametro.setConsecutivo(parametro.getTipoParametro());
		tipoParametro.setNombre(rs.getString("tipoParametroNombre"));
		parametro.setEntityTipoParametro(tipoParametro);
		
		return parametro;
	}

}
