package co.edu.javerianacali.grados.persistence;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import co.edu.javerianacali.grados.entities.ProcesosGrado;
import co.edu.javerianacali.grados.enums.DatabaseEnum;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.interfaces.persistence.IProcesosGradoDao;
import co.edu.javerianacali.grados.persistence.mappers.AttributeMapping;
import co.edu.javerianacali.grados.persistence.mappers.EMapperFactory;
import co.edu.javerianacali.grados.persistence.mappers.EntityMapper;
import co.edu.javerianacali.grados.persistence.mappers.ProcesosGradoMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.gridview.PagedList;
import co.edu.javerianacali.grados.utils.gridview.SearchFilter;
import co.edu.javerianacali.grados.utils.gridview.SortField;
import co.edu.javerianacali.grados.utils.namedquery.INamedQuery;
import co.edu.javerianacali.grados.utils.namedquery.NamedTable;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class EstudianteDao
 * @description
 * @date 8/10/2015
 */
@Repository
public class ProcesosGradoDao extends Dao<ProcesosGrado, ProcesosGradoMapper> implements
		IProcesosGradoDao {

	@Autowired
	private INamedQuery namedQuery;

	private EntityMapper entityMapper = EMapperFactory.getProcesosGradoMap();
	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 13/10/2015
	 * @param rs
	 * @param rowNum
	 * @return
	 * @throws SQLException
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	@Override
	public List <ProcesosGrado> consultarProcesosGrado() throws DaoException{
		String queryString = namedQuery
				.getNamedQuery(NQConstants.NQ_CONSULTA_PROCESO_GRADO);

		MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		List<ProcesosGrado> result = getNamedParameterJdbcTemplate().query(
				queryString, namedParameters, new ProcesosGradoMapper());
		if (!result.isEmpty()) {
			return result;
		}
 
		return null;
		
	}
	
	@Override
	public String createQueryCriteria(String namedQueryString,
			MapSqlParameterSource queryParameters, PagingCriteria pagingCriteria)
			throws DaoException {
		StringBuilder queryString = new StringBuilder();
		queryString.append(namedQueryString);

		List<SortField> sortList = pagingCriteria.getSortFields();
		List<SearchFilter> filterList = pagingCriteria.getSearchFilters();
		for (SearchFilter filter : filterList) {
			queryString.append(buildFilter(":parametro", "AND", entityMapper,
					filter, queryParameters));
			/*
			 * if (!StringUtil.isEmpty(filter.getValue())) {
			 * queryString.append(" AND ");
			 * queryString.append(" LOWER(:parametro.");
			 * queryString.append(EMapperFactory.getParametroColumn(filter
			 * .getProperty())); queryString
			 * .append(String.format(") LIKE LOWER('%' || :%s || '%') ",
			 * filter.getProperty()));
			 * queryParameters.addValue(filter.getProperty(),
			 * filter.getValue()); }
			 */
		}

		if (!StringUtil.isEmpty(pagingCriteria.getGlobalSearch())) {
			queryParameters
					.addValue("search", pagingCriteria.getGlobalSearch());
			queryString
					.append(" AND (LOWER(:parametro.clave) LIKE LOWER('%' || :search || '%') ");
			queryString
					.append("  OR LOWER(:parametro.valor) LIKE LOWER('%' || :search || '%') ");
			// queryString.append("  OR LOWER(:parametro.consecutivo) LIKE LOWER('%' || :search || '%') ");
			queryString
					.append("  OR LOWER(:parametro.descripcion) LIKE LOWER('%' || :search || '%')) ");
		}

		queryString.append(getSortFields(sortList));

		return queryString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#getEntityMapper()
	 */
	@Override
	public EntityMapper getEntityMapper() {
		return entityMapper;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return
	 * @see co.edu.javerianacali.grados.persistence.Dao#entityWhereClause()
	 */
	@Override
	public String entityWhereClause() {
		AttributeMapping attr = entityMapper.getAtributeMapping("consecutivo");
		if (null == attr) {
			return "";
		}
		StringBuilder where = new StringBuilder();
		where.append(" WHERE ");
		where.append(attr.getColumn());
		where.append(" = :");
		where.append(attr.getAttribute());
		return where.toString();
	}
	
}

