package co.edu.javerianacali.grados.persistence.mappers;

import java.util.Map;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class EntityMapper
 * @description
 * @date 13/07/2015
 */
public class EntityMapper {

	private final String table;
	private final Map<String, AttributeMapping> attributeMapping;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @param table
	 * @param attributeMapping
	 */
	public EntityMapper(String table,
			Map<String, AttributeMapping> attributeMapping) {
		this.table = table;
		this.attributeMapping = attributeMapping;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @return the attributeMapping
	 */
	public Map<String, AttributeMapping> getAttributeMapping() {
		return attributeMapping;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param attribute
	 * @return
	 */
	public AttributeMapping getAtributeMapping(String attribute) {
		return attributeMapping.get(attribute);
	}

}
