package co.edu.javerianacali.grados.interfaces.service;

import java.util.Map;

import co.edu.javerianacali.entities.UsuarioAutenticado;
import co.edu.javerianacali.grados.exception.core.ServiceException;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class IHmaAuthService
 * @description
 * @date 20/03/2015
 */
public interface IHmaAuthService {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/03/2015
	 * @description
	 * @param hmaToken
	 * @return
	 * @throws ServiceException
	 */
	UsuarioAutenticado doHmaAuthentication(String hmaToken)
			throws ServiceException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/02/2015
	 * @description Dummy. USAR UNICAMENTE EN DESARROLLO.
	 * @param id
	 * @return
	 */
	Map<String, Object> getUsuarioRole(String id);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana Cardona</a> 
	 * @date 24/03/2015
	 * @description Dummy. USAR UNICAMENTE EN DESARROLLO.
	 * @param emplid
	 * @return
	 */
	Map<String, Object> getUsuarioRoleByEmplid(String emplid);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/03/2015
	 * @description
	 * @param usuarioAutenticado
	 * @return
	 */
	String getRoleUsuario(UsuarioAutenticado usuarioAutenticado);

}
