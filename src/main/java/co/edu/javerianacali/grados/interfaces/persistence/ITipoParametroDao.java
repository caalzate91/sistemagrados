package co.edu.javerianacali.grados.interfaces.persistence;

import java.util.List;

import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.entities.TipoParametro;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.persistence.mappers.TipoParametroMapper;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ITipoParametroDao
 * @description
 * @date 9/07/2015
 */
public interface ITipoParametroDao extends
		IDao<TipoParametro, TipoParametroMapper> {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description
	 * @param searchCriteria
	 * @return
	 * @throws DaoException
	 */
	List<TipoParametro> autocompleteSearch(SearchCriteria searchCriteria)
			throws DaoException;

}
