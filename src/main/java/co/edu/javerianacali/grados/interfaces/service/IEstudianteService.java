package co.edu.javerianacali.grados.interfaces.service;

import co.edu.javerianacali.grados.entities.Estudiante;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class IParametroService
 * @description
 * @date 8/07/2015
 */
public interface IEstudianteService {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 */
	PagedList<Estudiante> consultarEstudiantes(PagingCriteria criteria)
			throws ServiceException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 10/07/2015
	 * @description
	 * @param parametro
	 * @throws ServiceException
	 */
	void guardarEstudiante(Estudiante estudiante) throws ServiceException;
	
	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés Rodríguez</a> 
	 * @date 28/08/2015
	 * @description 
	 * @param parametro
	 * @throws ServiceException
	 */
	void actualizarEstudiante(Estudiante estudiante) throws ServiceException;

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrés Rodríguez</a> 
	 * @date 28/08/2015
	 * @description 
	 * @param parametro
	 * @throws ServiceException
	 */
	public Estudiante consultarEstudiante(String empleidEstudiante) throws ServiceException;


}

