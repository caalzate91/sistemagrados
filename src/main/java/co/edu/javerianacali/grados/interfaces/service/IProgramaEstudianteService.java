package co.edu.javerianacali.grados.interfaces.service;

import java.util.List;
import co.edu.javerianacali.grados.entities.ProgramaEstudiante;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class IParametroService
 * @description
 * @date 8/07/2015
 */
public interface IProgramaEstudianteService {

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 13/10/2015
	 * @description
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 */
	PagedList<ProgramaEstudiante> consultarProgramaEstudiante(PagingCriteria criteria)
			throws ServiceException;

	public List<ProgramaEstudiante> consultarProgramaEstudiante(String empleidEstudiante) throws ServiceException;


}

