package co.edu.javerianacali.grados.interfaces.persistence;

import co.edu.javerianacali.grados.entities.Parametro;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.persistence.mappers.ParametroMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class IParametroDao
 * @description
 * @date 3/07/2015
 */
public interface IParametroDao extends IDao<Parametro, ParametroMapper> {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param parametro
	 * @throws DaoException
	 */
	void guardarParametro(Parametro parametro) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param parametro
	 * @throws DaoException
	 */
	void editarParametro(Parametro parametro) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param id
	 * @throws DaoException
	 */
	void eliminarParametroById(Long id) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param pagingCriteria
	 * @return
	 * @throws DaoException
	 */
	PagedList<Parametro> consultarParametros(PagingCriteria pagingCriteria)
			throws DaoException;

}
