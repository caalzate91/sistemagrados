package co.edu.javerianacali.grados.interfaces.persistence;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.edu.javerianacali.grados.entities.Estudiante;
import co.edu.javerianacali.grados.entities.ProgramaEstudiante;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.persistence.mappers.ProgramaEstudianteMapper;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache 
 *         MOntoya</a>
 * @project SistemaGrados
 * @class IProgramaEstudiante
 * @description
 * @date 13/10/2015 
 */

public interface IProgramaEstudianteDao extends IDao<ProgramaEstudiante, ProgramaEstudianteMapper> {

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 13/10/2015
	 * @param rs
	 * @param rowNum
	 * @return
	 * @throws SQLException
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */

	public List<ProgramaEstudiante> consultarProgramaEstudiante(String empleID) throws DaoException;

}
