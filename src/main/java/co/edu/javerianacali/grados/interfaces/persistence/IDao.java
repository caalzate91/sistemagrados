/**
 * 
 */
package co.edu.javerianacali.grados.interfaces.persistence;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.PagedList;
import co.edu.javerianacali.grados.utils.namedquery.NamedTable;

/**
 * 
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class IDao
 * @description
 * @date 6/07/2015
 */
public interface IDao<T, M extends RowMapper<T>> {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param entity
	 * @throws DaoException
	 */
	void save(T entity) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sql
	 * @param entity
	 * @throws DaoException
	 */
	void save(String sql, T entity) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sql
	 * @param parameters
	 * @throws DaoException
	 */
	void save(String sql, SqlParameterSource parameters) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description Actualiza una entidad de tipo {@link T}.
	 * @param entity
	 * @throws DaoException
	 */
	void update(T entity) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sqlUpdate
	 * @param entity
	 * @throws DaoException
	 */
	void update(String sqlUpdate, T entity) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param entity
	 * @throws DaoException
	 */
	void delete(T entity) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sqlUpdate
	 * @param parameters
	 * @throws DaoException
	 */
	void update(String sqlUpdate, SqlParameterSource parameters)
			throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sqlDelete
	 * @param entity
	 * @throws DaoException
	 */
	void delete(String sqlDelete, T entity) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param sqlDelete
	 * @param parameters
	 * @throws DaoException
	 */
	void delete(String sqlDelete, SqlParameterSource parameters)
			throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/07/2015
	 * @description
	 * @param entity
	 * @param mapper
	 * @return
	 * @throws DaoException
	 */
	T getSimple(T entity, M mapper) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param query
	 * @param parameters
	 * @param mapper
	 * @return
	 * @throws DaoException
	 */
	T get(String query, SqlParameterSource parameters, M mapper)
			throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param query
	 * @param parameters
	 * @param mapper
	 * @return
	 * @throws DaoException
	 */
	<E extends RowMapper<T>> T getEntity(String query,
			SqlParameterSource parameters, E mapper) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/07/2015
	 * @description
	 * @param query
	 * @param namedTables
	 * @param pagingCriteria
	 * @param mapper
	 * @return
	 * @throws DaoException
	 */
	PagedList<T> getPagedList(String query, List<NamedTable> namedTables,
			PagingCriteria pagingCriteria, M mapper) throws DaoException;

}
