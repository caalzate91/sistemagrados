package co.edu.javerianacali.grados.interfaces.service;

import java.util.List;
import co.edu.javerianacali.grados.entities.ProcesosGrado;
import co.edu.javerianacali.grados.exception.core.ServiceException;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
 *         Montoya</a>
 * @project SistemaGrados
 * @class IProcesosGradoService
 * @description
 * @date 16/10/2015
 */
public interface IProcesosGradoService {

	/**
	 * 
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache
	 *         Montoya</a>
	 * @date 16/10/2015
	 * @description
	 * @param criteria
	 * @return
	 * @throws ServiceException
	 */
	List<ProcesosGrado> consultarProcesosGrado()
			throws ServiceException;

}
