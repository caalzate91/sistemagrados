package co.edu.javerianacali.grados.interfaces.persistence;

import co.edu.javerianacali.grados.entities.Estudiante;
import co.edu.javerianacali.grados.exception.core.DaoException;
import co.edu.javerianacali.grados.persistence.mappers.EstudianteMapper;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.gridview.PagedList;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jonathan Almache 
 *         Montoya</a>
 * @project SistemaGrados
 * @class IEstudianteDao
 * @description
 * @date 07/10/2015
 */
public interface IEstudianteDao extends IDao<Estudiante, EstudianteMapper> {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @description
	 * @param parametro
	 * @throws DaoException
	 */
	void guardarEstudiante(Estudiante estudiante) throws DaoException;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @description
	 * @param parametro
	 * @throws DaoException
	 */
	void editarEstudiante(Estudiante estudiante) throws DaoException;


	/**
	 * @author <a href="mailto:jonathan.almache@premize.com">Jonathan Almache 
	 *         Montoya</a>
	 * @date 07/10/2015
	 * @description
	 * @param emplID
	 * @throws DaoException
	 */
	public Estudiante consultarEstudiante(String empleID) throws DaoException;
	


}



