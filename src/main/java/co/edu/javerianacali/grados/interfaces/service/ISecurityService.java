package co.edu.javerianacali.grados.interfaces.service;

import java.util.List;

import co.edu.javerianacali.entities.UsuarioAutenticado;
import co.edu.javerianacali.grados.security.PortalEstudiante;



public interface ISecurityService {

    String procesar(String usuario, int sec);

	PortalEstudiante procesar(String p_idEntrada);

    List<String> Roles(String usuario);

    public UsuarioAutenticado usuarioAutenticado(String usuario);
}
