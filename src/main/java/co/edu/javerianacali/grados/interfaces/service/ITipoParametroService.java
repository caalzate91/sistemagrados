package co.edu.javerianacali.grados.interfaces.service;

import java.util.List;

import co.edu.javerianacali.grados.entities.SearchCriteria;
import co.edu.javerianacali.grados.entities.TipoParametro;
import co.edu.javerianacali.grados.exception.core.ServiceException;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ITipoParametroService
 * @description
 * @date 9/07/2015
 */
public interface ITipoParametroService {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @description
	 * @param searchCriteria
	 * @return
	 * @throws ServiceException
	 */
	List<TipoParametro> buscarTipoParametros(SearchCriteria searchCriteria)
			throws ServiceException;
}
