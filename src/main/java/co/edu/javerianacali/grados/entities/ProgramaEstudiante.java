package co.edu.javerianacali.grados.entities;

/**
 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache 
 * Montoya</a>
 * @project Grados
 * @class Estudiante
 * @description
 * @date 07/10/2015
 */
public class ProgramaEstudiante {


	private ProgramaEstudiante programaEstudiante;
	private String emplId;
	private String nombrePrograma;		
	private Integer creditosAprobados;
	private Integer creditosRequeridos;
	
	private Boolean flagInscripcion;

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */
	public ProgramaEstudiante getProgramaEstudiante() {
		return programaEstudiante;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public void setProgramaEstudiante(ProgramaEstudiante programaEstudiante) {
		this.programaEstudiante = programaEstudiante;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public String getEmplId() {
		return emplId;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public void setEmplId(String emplId) {
		this.emplId = emplId;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public String getNombrePrograma() {
		return nombrePrograma;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public Integer getCreditosAprobados() {
		return creditosAprobados;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public void setCreditosAprobados(Integer creditosAprobados) {
		this.creditosAprobados = creditosAprobados;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public Integer getCreditosRequeridos() {
		return creditosRequeridos;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 13/10/2015
	 */	
	public void setCreditosRequeridos(Integer creditosRequeridos) {
		this.creditosRequeridos = creditosRequeridos;
	}
	
	public Boolean getFlagInscripcion() {
		return flagInscripcion;
	}
	
	public void setFlagInscripcion(Boolean flagInscripcion) {
		this.flagInscripcion = flagInscripcion;
	}
	
	
	
}
