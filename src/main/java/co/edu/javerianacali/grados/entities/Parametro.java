package co.edu.javerianacali.grados.entities;

import co.edu.javerianacali.grados.enums.EstadoParametroEnum;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Andrs Rodriguez
 *         Arango</a>
 * @project HomologacionAsignaturas
 * @class Parametro
 * @description
 * @date 27/11/2014
 */
public class Parametro {

	private Long consecutivo;
	private String clave;
	private String valor;
	private String descripcion;
	private Integer ordenamiento;
	private String estado;
	private EstadoParametroEnum estadoEnum;
	private Long tipoParametro;
	private TipoParametro entityTipoParametro;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 */
	public Parametro() {
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param clave
	 *            the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/11/2014
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez </a>
	 * @date 19/02/2015
	 * @return the consecutivo
	 */
	public Long getConsecutivo() {
		return consecutivo;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com"> </a>
	 * @date 19/02/2015
	 * @param consecutivo
	 *            the consecutivo to set
	 */
	public void setConsecutivo(Long consecutivo) {
		this.consecutivo = consecutivo;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez </a>
	 * @date 19/02/2015
	 * @return the ordenamiento
	 */
	public Integer getOrdenamiento() {
		return ordenamiento;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com"> </a>
	 * @date 19/02/2015
	 * @param ordenamiento
	 *            the ordenamiento to set
	 */
	public void setOrdenamiento(Integer ordenamiento) {
		this.ordenamiento = ordenamiento;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 3/07/2015
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 3/07/2015
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 3/07/2015
	 * @return the tipoParametro
	 */
	public Long getTipoParametro() {
		return tipoParametro;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 3/07/2015
	 * @param tipoParametro
	 *            the tipoParametro to set
	 */
	public void setTipoParametro(Long tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @return the entityTipoParametro
	 */
	public TipoParametro getEntityTipoParametro() {
		return entityTipoParametro;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/07/2015
	 * @param entityTipoParametro
	 *            the entityTipoParametro to set
	 */
	public void setEntityTipoParametro(TipoParametro entityTipoParametro) {
		this.entityTipoParametro = entityTipoParametro;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @return the estadoEnum
	 */
	public EstadoParametroEnum getEstadoEnum() {
		return estadoEnum;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @param estadoEnum
	 *            the estadoEnum to set
	 */
	public void setEstadoEnum(EstadoParametroEnum estadoEnum) {
		this.estadoEnum = estadoEnum;
	}

}
