package co.edu.javerianacali.grados.entities;


/**
 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache 
 * Montoya</a>
 * @project Grados
 * @class Estudiante
 * @description
 * @date 07/10/2015
 */
public class ProcesosGrado {

	private int idProcesoGrado;
	private String descripcion;		
	
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 19/10/2015
	 */
	public int getIdProcesoGrado() {
		return idProcesoGrado;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 19/10/2015
	 */	
	public void setIdProcesoGrado(int idProcesoGrado) {
		this.idProcesoGrado = idProcesoGrado;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 19/10/2015
	 */	
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 19/10/2015
	 */	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
