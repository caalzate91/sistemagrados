package co.edu.javerianacali.grados.entities;

import java.io.Serializable;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class SearchCriteria
 * @description
 * @date 9/07/2015
 */
public class SearchCriteria implements Serializable {

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 */
	private static final long serialVersionUID = 6283559876654815948L;

	private String value;
	private String limit;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 */
	public SearchCriteria() {
		super();
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @return the limit
	 */
	public String getLimit() {
		return limit;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 9/07/2015
	 * @param limit
	 *            the limit to set
	 */
	public void setLimit(String limit) {
		this.limit = limit;
	}

}
