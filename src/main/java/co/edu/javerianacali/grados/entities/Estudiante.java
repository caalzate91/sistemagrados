package co.edu.javerianacali.grados.entities;
import java.io.Serializable;

/**
 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache 
 * Montoya</a>
 * @project Grados
 * @class Estudiante
 * @description
 * @date 07/10/2015
 */
public class Estudiante implements Serializable {

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	private static final long serialVersionUID = 792002521422607692L;

	private Estudiante estudiante;
	private String emplId;
	private String nombres;
	private String apellidos;
	private String documento;
	private String tipoDocumento;
	private String correo;
	private String telefonoFijo;
	private String direccion;
	private String celular;
	private String empresa;
	private String cargo;
	private String correoEmpresa;
	private String telefonoOficina;
	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public Estudiante() {
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public String getEmplId() {
		return emplId;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public void setEmplId(String emplId) {
		this.emplId = emplId;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public String getNombres() {
		return nombres;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public String getDocumento() {
		return documento;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public Estudiante getEstudiante() {
		return estudiante;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 07/10/2015
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */	
	public void setCorreo(String correo) {
		this.correo = correo;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getEmpresa() {
		return empresa;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getCargo() {
		return cargo;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getCorreoEmpresa() {
		return correoEmpresa;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setCorreoEmpresa(String correoEmpresa) {
		this.correoEmpresa = correoEmpresa;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public String getTelefonoOficina() {
		return telefonoOficina;
	}

	/**
	 * @author <a href="mailto:jonathan.almache@premize.com"> Jonathan Almache
	 *  Montoya</a>
	 * @date 15/10/2015
	 */
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

}
