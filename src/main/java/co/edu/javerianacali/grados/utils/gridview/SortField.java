package co.edu.javerianacali.grados.utils.gridview;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class SortField
 * @description
 * @date 31/12/2014
 */
public class SortField {

	private String property;
	private String direction;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @param property
	 * @param direction
	 */
	public SortField(String property, String direction) {
		this.property = property;
		this.direction = direction;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

}
