/**
 * 
 */
package co.edu.javerianacali.grados.utils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import co.edu.javerianacali.grados.enums.CryptoEnum;

import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;

/**
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
 * 
 *         </a>
 * @project HomologacionAsignaturas
 * @class CryptoUtil
 * @description
 * @date 4/03/2015
 *
 */
public class CryptoUtil implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2952876669571901337L;
	private static Cipher encrypt;

	private static SecretKey key;

	static{
		try {
			key = KeyGenerator.getInstance(
					CryptoEnum.CRYPTO_ENCRYPTION_KEY.getCrypto()).generateKey();

			encrypt = Cipher.getInstance(CryptoEnum.CRYPTO_ENCRYPTION_ALGORITHM
					.getCrypto());
			encrypt.init(Cipher.ENCRYPT_MODE, key);
		} catch (Exception e) {
			
		}
		
	}
	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 4/03/2015
	 * @description Metodo para incriptar un String se usa la clase CipHer de
	 *              Java pero el arreglo de bytes que devuelve CipHer se
	 *              encriptan de nuevo con Base64 para proveer un poco m�s de
	 *              seguridad
	 * @param string
	 * @param encryptWithBase64
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String encryptString(String string)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {

		byte[] utf8 = string.getBytes(CryptoEnum.CRYPTO_CHARSET_CODIFICATION
				.getCrypto());
		byte[] enc = encrypt.doFinal(utf8);
		String encryptString = null;
		enc = BASE64EncoderStream.encode(enc);
		encryptString = new String(enc);

		return encryptString.replace("=", "Ea");

	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 4/03/2015
	 * @description Metodo que desencripa un string previamente encriptado con
	 *              el metodo encryptString de esta clase
	 * @param string
	 * @return decrypt
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String decryptString(String string)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		encrypt = Cipher.getInstance(CryptoEnum.CRYPTO_ENCRYPTION_ALGORITHM.getCrypto());
		encrypt.init(Cipher.DECRYPT_MODE, key);
		string = string.replace("Ea", "=");
		byte[] dec = BASE64DecoderStream.decode(string
				.getBytes(CryptoEnum.CRYPTO_CHARSET_CODIFICATION.getCrypto()));

		byte[] decUtf8 = encrypt.doFinal(dec);
		String decrypt = new String(decUtf8,
				CryptoEnum.CRYPTO_CHARSET_CODIFICATION.getCrypto());

		return decrypt;

	}

	

}
