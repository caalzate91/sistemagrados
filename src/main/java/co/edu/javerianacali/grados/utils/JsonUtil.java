package co.edu.javerianacali.grados.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class JsonUtil
 * @description
 * @date 30/12/2014
 */
public final class JsonUtil {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 */
	private JsonUtil() {
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 * @description
	 * @param value
	 * @return
	 */
	public static String toJson(Object value) {
		Gson gson = new Gson();
		return gson.toJson(value);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/01/2015
	 * @description
	 * @param value
	 * @param dateFormat
	 * @return
	 */
	public static String toJson(Object value, String dateFormat) {
		Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
		return gson.toJson(value);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/02/2015
	 * @description
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T> T fromJson(String json, Class<T> type) {
		Gson gson = new Gson();
		return gson.fromJson(json, type);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/02/2015
	 * @description
	 * @param json
	 * @param dateFormat
	 * @param type
	 * @return
	 */
	public static <T> T fromJson(String json, String dateFormat, Class<T> type) {
		Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
		return gson.fromJson(json, type);
	}
}
