/**
 * 
 */
package co.edu.javerianacali.grados.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**		
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
  
</a>	
 * @project HomologacionAsignaturas	
 * @class DateUtil	
 * @description	
 * @date 27/03/2015	
 *	
 */
public final class DateUtil {

	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 27/03/2015		
	 */
	private DateUtil() {
		super();
		
	}
	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 27/03/2015	
	 * @description	
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToFormat(Date date, String format){
		DateFormat df = new SimpleDateFormat(format);
		String string = df.format(date);
		
		return string.toUpperCase();
	}
	
	
}
