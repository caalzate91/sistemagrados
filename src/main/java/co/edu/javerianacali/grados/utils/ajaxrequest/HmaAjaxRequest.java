/**
 * 
 */
package co.edu.javerianacali.grados.utils.ajaxrequest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**		
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
  
</a>	
 * @project HomologacionAsignaturas	
 * @class HmaAjaxRequest	
 * @description	
 * @date 23/02/2015	
 *	
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.PARAMETER})  
public @interface HmaAjaxRequest {

}
