package co.edu.javerianacali.grados.utils.gridview;

import java.util.List;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class PagedList
 * @description
 * @date 31/12/2014
 */
public class PagedList<T> {

	private final List<T> records;
	private final Integer totalRecords;
	private final Long totalCount;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @param records
	 * @param totalRecords
	 * @param totalCount
	 */
	public PagedList(List<T> records, Integer totalRecords, Long totalCount) {
		this.records = records;
		this.totalRecords = totalRecords;
		this.totalCount = totalCount;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the records
	 */
	public List<T> getRecords() {
		return records;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the totalRecords
	 */
	public Integer getTotalRecords() {
		return totalRecords;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the totalCount
	 */
	public Long getTotalCount() {
		return totalCount;
	}

}
