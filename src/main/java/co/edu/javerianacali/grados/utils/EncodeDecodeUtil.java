package co.edu.javerianacali.grados.utils;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class EncodeDecodeUtil
 * @description
 * @date 5/02/2015
 */
public final class EncodeDecodeUtil {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/02/2015
	 */
	private EncodeDecodeUtil() {
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/02/2015
	 * @description
	 * @param data
	 * @return
	 */
	public static String decodeData(String data) {
		StringBuilder ddata = new StringBuilder();
		if (null != data) {
			if (data.equals("-1")) {
				return data;
			}
			char[] chars = data.toCharArray();
			for (int i = 0; i < chars.length; i++) {
				int v = chars[i];
				ddata.append(Character.toChars((v-i)));
			}
		}
		return ddata.toString();
	}

}
