package co.edu.javerianacali.grados.utils;

import java.util.Collections;
import java.util.List;

import co.edu.javerianacali.grados.utils.gridview.SearchFilter;
import co.edu.javerianacali.grados.utils.gridview.SortField;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class PagingCriteria
 * @description
 * @date 31/12/2014
 */
public class PagingCriteria {

	private final String globalSearch;
	private final List<SearchFilter> searchFilters;
	private final List<SortField> sortFields;
	private Integer start;
	private final Integer limit;
	private final Integer pageNumber;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @param globalSearch
	 * @param searchFilters
	 * @param sortFields
	 * @param start
	 * @param limit
	 * @param pageNumber
	 */
	public PagingCriteria(String globalSearch,
			List<SearchFilter> searchFilters, List<SortField> sortFields,
			Integer start, Integer limit, Integer pageNumber) {
		this.globalSearch = globalSearch;
		this.searchFilters = searchFilters;
		this.sortFields = sortFields;
		this.start = start;
		this.limit = limit;
		this.pageNumber = pageNumber;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the globalSearch
	 */
	public String getGlobalSearch() {
		return globalSearch;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the searchFilters
	 */
	public List<SearchFilter> getSearchFilters() {
		return Collections.unmodifiableList(searchFilters);
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the sortFields
	 */
	public List<SortField> getSortFields() {
		return Collections.unmodifiableList(sortFields);
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the start
	 */
	public Integer getStart() {
		return start;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the limit
	 */
	public Integer getLimit() {
		return limit;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the pageNumber
	 */
	public Integer getPageNumber() {
		return pageNumber;
	}
	
	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 13/03/2015	
	 * @description	
	 * @param start
	 */
	public void setStart(Integer start){
		this.start = start;
	}

}
