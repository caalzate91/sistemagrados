package co.edu.javerianacali.grados.utils.namedquery;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class QueryElement
 * @description
 * @date 12/01/2015
 */
public class QueryElement {

	private final String name;
	private final String type;

	private final Map<String, String> query;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @param name
	 * @param type
	 * @param query
	 */
	public QueryElement(String name, String type, Map<String, String> query) {
		this.name = name;
		this.type = type;
		this.query = query;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param name
	 * @return
	 */
	public String getNamedQuery(String name) {
		if (NamedQueryEnum.DEFAULT_QUERY.getValue().equals(type)) {
			return query.get(NamedQueryEnum.ALL_CLAUSE.getValue())
					.replaceAll("\t", "").replaceAll("\n", " ");
		} else if (NamedQueryEnum.SFW_CLAUSE_QUERY.getValue().equals(type)) {
			StringBuilder sql = new StringBuilder();
			String queryClause = query.get(NamedQueryEnum.SELECT_CLAUSE
					.getValue());
			if (queryClause != null) {
				sql.append(queryClause);
				sql.append(" ");
			}
			queryClause = query.get(NamedQueryEnum.FROM_CLAUSE.getValue());
			if (queryClause != null) {
				sql.append(queryClause);
				sql.append(" ");
			}
			queryClause = query.get(NamedQueryEnum.WHERE_CLAUSE.getValue());
			if (queryClause != null) {
				sql.append(queryClause);
				sql.append(" ");
			}
			return sql.toString().replaceAll("\t", "").replaceAll("\n", " ");
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param name
	 * @param namedTables
	 * @return
	 */
	public String getNamedQuery(String name, List<NamedTable> namedTables) {
		return replaceNamedTables(getNamedQuery(name), namedTables);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param name
	 * @param queryClause
	 * @return
	 */
	public String getNamedQuery(String name, NamedQueryEnum queryClause) {
		String nquery = query.get(queryClause.getValue());
		if (nquery == null) {
			nquery = "";
		}
		return nquery;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param query
	 * @param namedTableList
	 * @return
	 */
	private String replaceNamedTables(String query,
			List<NamedTable> namedTableList) {
		for (NamedTable namedTable : namedTableList) {
			if (namedTable.getNamedTable() != null
					&& !namedTable.getNamedTable().trim().equals("")) {
				query = query.replaceAll(namedTable.getNamedTable(),
						namedTable.getTable());
			}

			// StringBuilder realTable = new StringBuilder();
			// realTable.append(namedTable.getTable());
			// realTable.append(" ");
			if (namedTable.getNamedAlias() != null
					&& !namedTable.getNamedAlias().trim().equals("")) {
				// realTable.append(namedTable.getAlias());
				// realTable.append(" ");
				query = query.replaceAll(namedTable.getNamedAlias(),
						namedTable.getAlias());
			}

		}
		return query;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/01/2015
	 * @return the query
	 */
	public Map<String, String> getQuery() {
		return query;
	}

}
