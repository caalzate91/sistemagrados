package co.edu.javerianacali.grados.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import co.edu.javerianacali.grados.enums.RegexEnum;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class StringUtil
 * @description
 * @date 7/01/2015
 */
public final class StringUtil {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/01/2015
	 */
	private StringUtil() {
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/01/2015
	 * @description retorna <code>true</code> si el parametro
	 *              <code>string</code> es igual a <code>null</code> o es una
	 *              cadena vacia, de lo contrario retorna <code>false</code>.
	 * @param string
	 * @return
	 */
	public static Boolean isEmpty(String string) {
		if (string == null || string.trim().equals("")) {
			return true;
		}
		return false;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez </a>
	 * @date 8/01/2015
	 * @description Metodo que se encargada de convertir en una lista las llaves compuestas
	 * generadas en un Mapper
	 * @param string
	 * @param index
	 * @return
	 */

	public static List<String> compositeKeyToList(String string, String Regex) {
		
		List<String> list = new ArrayList<String>();
		list = Arrays.asList(string.split(Regex));
		
		return list;
		
	}
	
	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
	</a>	
	 * @date 22/01/2015	
	 * @description	
	 * @param enumValues
	 * @return
	 */
	public static List<Object> enumToList(Object[] enumValues){
		
		List<Object> list = new ArrayList<Object>();
		list = Arrays.asList(enumValues);
		
		return list;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez </a>
	 * @date 14/01/2015
	 * @description
	 * @param string
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static Date toDate(String string, String format)
			throws ParseException {
		Date fecha = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		fecha = dateFormat.parse(string);

		return fecha;

	}
	
	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 26/02/2015	
	 * @description	
	 * @param date
	 * @param format
	 * @return
	 */
	public static String toString(Date date, String format){
		DateFormat formatDate = new SimpleDateFormat(format);
		
		String toString = formatDate.format(date);
		
		return toString;
		
	}
	
	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>	
	 * @date 26/02/2015	
	 * @description	
	 * @param value
	 * @param string
	 * @return
	 */
	public static String replaceString(String value, String string){
		string = string.replaceAll(RegexEnum.ROLE_SEPARATOR.getRegex(), value);
		
		return string;
	}

}
