package co.edu.javerianacali.grados.utils.namedquery;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class NamedQuery
 * @description
 * @date 12/01/2015
 */
public class NamedQuery implements INamedQuery {

	private Map<String, QueryElement> queryMap;
	private final String xmlLocation;

	private static final Logger LOG = Logger.getLogger(NamedQuery.class);

	public NamedQuery(String xmlLocation) throws Exception {
		this.xmlLocation = xmlLocation;
		loadNamedQuery();
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @return the xmlLocation
	 */
	public String getXmlLocation() {
		return xmlLocation;
	}

	private void loadNamedQuery() {
		Map<String, QueryElement> map = new HashMap<String, QueryElement>();
		try {
			InputStream in = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(xmlLocation);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(in);
			doc.getDocumentElement().normalize();

			NodeList nodeList = doc.getElementsByTagName("sql-query");

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					String name = element.getAttribute("name");
					String type = element.getAttribute("type");
					Map<String, String> query = new HashMap<String, String>();
					if (NamedQueryEnum.DEFAULT_QUERY.getValue().equals(type)) {
						query.put(NamedQueryEnum.ALL_CLAUSE.getValue(),
								element.getTextContent());
					} else {
						query.put(NamedQueryEnum.SELECT_CLAUSE.getValue(),
								element.getElementsByTagName("select-clause")
										.item(0).getTextContent());
						query.put(NamedQueryEnum.FROM_CLAUSE.getValue(),
								element.getElementsByTagName("from-clause")
										.item(0).getTextContent());
						query.put(NamedQueryEnum.WHERE_CLAUSE.getValue(),
								element.getElementsByTagName("where-clause")
										.item(0).getTextContent());
					}
					map.put(name, new QueryElement(name, type, query)); 
				}
			}

			queryMap = Collections.unmodifiableMap(map);

		} catch (ParserConfigurationException ex) {
			LOG.error(ex.getMessage(), ex);
		} catch (SAXException ex) {
			LOG.error(ex.getMessage(), ex);
		} catch (IOException ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @param name
	 * @return
	 * @see co.edu.javerianacali.grados.hma.utils.namedquery.INamedQuery#getNamedQuery(java.lang.String)
	 */
	public String getNamedQuery(String name) {
		QueryElement qe = queryMap.get(name);
		if (qe != null) {
			return qe.getNamedQuery(name).trim();
		}
		return "";
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/01/2015
	 * @param name
	 * @param format
	 * @return
	 * @see co.edu.javerianacali.grados.hma.utils.namedquery.INamedQuery#getNamedQuery(java.lang.String,
	 *      boolean)
	 */
	public String getNamedQuery(String name, boolean format) {
		QueryElement qe = queryMap.get(name);
		String query = "";
		if (qe != null) {
			if (NamedQueryEnum.DEFAULT_QUERY.getValue().equals(qe.getType())) {
				query = qe.getQuery().get(NamedQueryEnum.ALL_CLAUSE.getValue());
			} else if (NamedQueryEnum.SFW_CLAUSE_QUERY.getValue().equals(
					qe.getType())) {
				StringBuilder sql = new StringBuilder();
				String queryClause = qe.getQuery().get(
						NamedQueryEnum.SELECT_CLAUSE.getValue());
				if (queryClause != null) {
					sql.append(queryClause);
					sql.append(" ");
				}
				queryClause = qe.getQuery().get(
						NamedQueryEnum.FROM_CLAUSE.getValue());
				if (queryClause != null) {
					sql.append(queryClause);
					sql.append(" ");
				}
				queryClause = qe.getQuery().get(
						NamedQueryEnum.WHERE_CLAUSE.getValue());
				if (queryClause != null) {
					sql.append(queryClause);
					sql.append(" ");
				}
				query = sql.toString().replaceAll("\t", "")
						.replaceAll("\n", " ");
			}

			if (!format) {
				query = query.replaceAll("\t", "").replaceAll("\n", " ");
			}
		}

		return query;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @param name
	 * @param namedTableList
	 * @return
	 * @see co.edu.javerianacali.grados.hma.utils.namedquery.INamedQuery#getNamedQuery(java.lang.String,
	 *      java.util.List)
	 */
	public String getNamedQuery(String name, List<NamedTable> namedTableList) {
		QueryElement qe = queryMap.get(name);
		if (qe != null) {
			return qe.getNamedQuery(name, namedTableList).trim();
		}
		return "";
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 14/01/2015
	 * @description
	 * @param query
	 * @param namedTableList
	 * @return
	 */
	public String getQueryFromNamedQuery(String query,
			List<NamedTable> namedTableList) {
		return replaceNamedTables(query, namedTableList);
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @param name
	 * @param queryClause
	 * @return
	 * @see co.edu.javerianacali.grados.hma.utils.namedquery.INamedQuery#getNamedQuery(java.lang.String,
	 *      co.edu.javerianacali.grados.hma.utils.namedquery.NamedQueryEnum)
	 */
	public String getNamedQuery(String name, NamedQueryEnum queryClause) {
		QueryElement qe = queryMap.get(name);
		// String nquery = "";
		StringBuilder nquery = new StringBuilder();
		if (qe != null) {
			if (qe.getType().equals(NamedQueryEnum.SFW_CLAUSE_QUERY.getValue())) {
				if (NamedQueryEnum.SELECT_FROM_CLAUSE.equals(queryClause)) {
					nquery.append(qe.getNamedQuery(name,
							NamedQueryEnum.SELECT_CLAUSE));
					nquery.append(qe.getNamedQuery(name,
							NamedQueryEnum.FROM_CLAUSE));
				} else {
					nquery.append(qe.getNamedQuery(name, queryClause));
				}
			}
		}
		return nquery.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param query
	 * @param namedTableList
	 * @return
	 */
	private String replaceNamedTables(String query,
			List<NamedTable> namedTableList) {
		for (NamedTable namedTable : namedTableList) {
			if (namedTable.getNamedTable() != null
					&& !namedTable.getNamedTable().trim().equals("")) {
				query = query.replaceAll(namedTable.getNamedTable(),
						namedTable.getTable());
			}
			if (namedTable.getNamedAlias() != null
					&& !namedTable.getNamedAlias().trim().equals("")) {
				query = query.replaceAll(namedTable.getNamedAlias(),
						namedTable.getAlias());
			}

		}
		return query;
	}

}
