package co.edu.javerianacali.grados.utils.namedquery;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana Cardona</a>
 * @project HomologacionAsignaturas
 * @class NamedQueryEnum
 * @description
 * @date 12/01/2015
 */
public enum NamedQueryEnum {
	
	DEFAULT_QUERY("DEFAULT"),
	SFW_CLAUSE_QUERY("SFW_CLAUSE"),
	SELECT_CLAUSE("SELECT"),
	FROM_CLAUSE("FROM"),
	WHERE_CLAUSE("WHERE"),
	ALL_CLAUSE("ALL"),
	SELECT_FROM_CLAUSE("SELECT_FROM_CLAUSE");
	
	private String value;
	
	private NamedQueryEnum(String value) {
		this.value = value;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
