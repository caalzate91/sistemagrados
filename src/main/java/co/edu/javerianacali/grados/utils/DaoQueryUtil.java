package co.edu.javerianacali.grados.utils;

import java.util.List;

import co.edu.javerianacali.grados.utils.gridview.SortField;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class DaoQueryUtil
 * @description
 * @date 6/01/2015
 */
public final class DaoQueryUtil {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/01/2015
	 */
	private DaoQueryUtil() {
	}

	/**
	 * 
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 6/01/2015
	 * @description
	 * @param sortFields
	 * @return
	 */
	public static String getSortFields(List<SortField> sortFields) {
		StringBuilder querySort = new StringBuilder();
		if (sortFields != null && !sortFields.isEmpty()) {
			querySort.append(" ORDER BY ");
			for (int i = 0; i < sortFields.size(); i++) {
				querySort.append(sortFields.get(i).getProperty());
				querySort.append(" ");
				querySort.append(sortFields.get(i).getDirection());
				if ((i + 1) != sortFields.size()) {
					querySort.append(", ");
				}
			}
		}
		return querySort.toString();
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez </a>
	 * @date 8/01/2015
	 * @description
	 * @param sequencesEnum
	 * @return
	 */
	public static String getSequenceNextVal(String sequencesEnum) {
		StringBuilder sequencesString = new StringBuilder();
		sequencesString.append(sequencesEnum);
		sequencesString.append(".NEXTVAL");

		return sequencesString.toString();

	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 14/01/2015
	 * @description
	 * @param query
	 * @param start
	 * @param limit
	 * @return
	 */
	public static String createPagedQuery(String query, Integer start,
			Integer limit, Boolean forFilter) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT * FROM ");
		queryString.append(" (SELECT A.*, ROWNUM rnum FROM (");
		queryString.append(query);
		queryString.append(") A WHERE ROWNUM <= ");
		queryString.append(limit);
		queryString.append(") ");
		queryString.append(" WHERE (rnum > ");
		queryString.append(start);
		if (forFilter.equals(true)) {
			queryString.append(" OR ");
		} else {
			queryString.append(" AND ");
		}
		queryString.append(" rnum<=");
		queryString.append(limit);
		queryString.append(")");
		return queryString.toString();
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 14/01/2015
	 * @description
	 * @param query
	 * @return
	 */
	public static String createCountQuery(String query) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT COUNT(*) FROM (");
		queryString.append(query);
		queryString.append(") ");
		return queryString.toString();
	}

}
