package co.edu.javerianacali.grados.utils;

import java.io.Serializable;
import java.util.Map;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class JsonResponse
 * @description
 * @date 30/12/2014
 */
public class JsonResponse<T> implements Serializable {

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 */
	private static final long serialVersionUID = -8333726329594367256L;

	public static final Integer STATUS_SUCCESS = 200;
	public static final Integer STATUS_ERROR = 500;

	private Integer status;
	private T result;
	private Boolean success;
	private Object exceptionMessage;
	private Object severity;
	private Map<String, Object> additionalData;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 */
	public JsonResponse() {
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 * @param status
	 * @param result
	 */
	public JsonResponse(Integer status, T result) {
		super();
		this.status = status;
		this.result = result;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 * @return the result
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 30/12/2014
	 * @param result
	 *            the result to set
	 */
	public void setResult(T result) {
		this.result = result;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/01/2015
	 * @return the success
	 */
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 5/01/2015
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 14/01/2015
	 * @return the exceptionMessage
	 */
	public Object getExceptionMessage() {
		return exceptionMessage;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 14/01/2015
	 * @param exceptionMessage
	 *            the exceptionMessage to set
	 */
	public void setExceptionMessage(Object exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/02/2015
	 * @return the additionalData
	 */
	public Map<String, Object> getAdditionalData() {
		return additionalData;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 27/02/2015
	 * @param additionalData
	 *            the additionalData to set
	 */
	public void setAdditionalData(Map<String, Object> additionalData) {
		this.additionalData = additionalData;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @return the severity
	 */
	public Object getSeverity() {
		return severity;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/07/2015
	 * @param severity
	 *            the severity to set
	 */
	public void setSeverity(Object severity) {
		this.severity = severity;
	}

}
