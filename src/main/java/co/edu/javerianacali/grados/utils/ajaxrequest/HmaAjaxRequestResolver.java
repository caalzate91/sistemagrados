/**
 * 
 */
package co.edu.javerianacali.grados.utils.ajaxrequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import co.edu.javerianacali.grados.enums.RequestEnum;

/**
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
 * 
 *         </a>
 * @project HomologacionAsignaturas
 * @class HmaAjaxRequestResolver
 * @description
 * @date 23/02/2015
 *
 */
public class HmaAjaxRequestResolver implements HandlerMethodArgumentResolver {

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 23/02/2015
	 * @description
	 * @param parameter
	 * @return
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getMethodAnnotation(HmaAjaxRequest.class) != null;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 23/02/2015
	 * @description
	 * @param parameter
	 * @param mavContainer
	 * @param webRequest
	 * @param binderFactory
	 * @return
	 * @throws Exception
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		HmaAjaxRequest ajaxRequest = parameter
				.getMethodAnnotation(HmaAjaxRequest.class);
		
		if(ajaxRequest != null){
			if(webRequest.getHeader(RequestEnum.REQUEST_AJAX.getRequest()) == null){
				return null;
				
			}
		}
		return WebArgumentResolver.UNRESOLVED;
	}

}
