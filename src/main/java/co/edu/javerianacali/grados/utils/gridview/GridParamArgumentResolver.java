package co.edu.javerianacali.grados.utils.gridview;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import co.edu.javerianacali.grados.enums.RequestEnum;
import co.edu.javerianacali.grados.utils.PagingCriteria;
import co.edu.javerianacali.grados.utils.StringUtil;
import co.edu.javerianacali.grados.utils.ajaxrequest.HmaAjaxRequest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class GridParamArgumentResolver
 * @description
 * @date 31/12/2014
 */
@EnableWebMvc
public class GridParamArgumentResolver implements HandlerMethodArgumentResolver {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/01/2015
	 * @param parameter
	 * @return
	 * @see org.springframework.web.method.support.HandlerMethodArgumentResolver#supportsParameter(org.springframework.core.MethodParameter)
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.getParameterAnnotation(GridParam.class) != null
				|| parameter.getMethodAnnotation(HmaAjaxRequest.class) != null;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 7/01/2015
	 * @param parameter
	 * @param mavContainer
	 * @param webRequest
	 * @param binderFactory
	 * @return
	 * @throws Exception
	 * @see org.springframework.web.method.support.HandlerMethodArgumentResolver#resolveArgument(org.springframework.core.MethodParameter,
	 *      org.springframework.web.method.support.ModelAndViewContainer,
	 *      org.springframework.web.context.request.NativeWebRequest,
	 *      org.springframework.web.bind.support.WebDataBinderFactory)
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {

		GridParam gridParamAnnotation = parameter
				.getParameterAnnotation(GridParam.class);
		HmaAjaxRequest ajaxMethodAnnotation = parameter
				.getMethodAnnotation(HmaAjaxRequest.class);
		if (gridParamAnnotation != null) {
			String strPage = webRequest.getParameter("page");
			String strLimit = webRequest.getParameter("limit");
			String strStart = webRequest.getParameter("start");
			String strFilters = webRequest.getParameter("filter");
			String strGridFilters = webRequest.getParameter("gridFilters");
			String strSorts = webRequest.getParameter("sort");
			String globalSearch = webRequest.getParameter("globalSearch");

			Integer page = 1;
			Integer start = 1;
			Integer limit = 1;

			Gson gson = new Gson();

			if (strPage != null && !strPage.trim().equals("")) {
				page = Integer.parseInt(strPage);
			}

			if (strStart != null && !strStart.trim().equals("")) {
				start = Integer.parseInt(strStart);
			}

			if (strLimit != null && !strLimit.trim().equals("")) {
				limit = Integer.parseInt(strLimit) * page;
			}

			List<SortField> sortFields = new ArrayList<SortField>();
			if (strSorts != null && !strSorts.trim().equals("")) {
				Type sortFieldType = new TypeToken<List<SortField>>() {
				}.getType();
				sortFields = gson.fromJson(strSorts, sortFieldType);
			}

			List<SearchFilter> searchFilters = new ArrayList<SearchFilter>();
			if (strGridFilters != null && !StringUtil.isEmpty(strGridFilters)) {
				Type searchFilterType = new TypeToken<List<SearchFilter>>() {
				}.getType();
				searchFilters = gson.fromJson(strGridFilters, searchFilterType);
			}

			PagingCriteria pagingCriteria = new PagingCriteria(globalSearch,
					searchFilters, sortFields, start, limit, page);

			return pagingCriteria;

		} else if(ajaxMethodAnnotation!= null){
			if(ajaxMethodAnnotation != null){
				if(webRequest.getHeader(RequestEnum.REQUEST_AJAX.getRequest()) == null){
					return null;
					
				}
			}
		}

		return WebArgumentResolver.UNRESOLVED;
	}

}
