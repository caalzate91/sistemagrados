package co.edu.javerianacali.grados.utils.namedquery;

import java.util.List;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class INamedQuery
 * @description
 * @date 12/01/2015
 */
public interface INamedQuery {

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param name
	 * @return
	 */
	String getNamedQuery(String name);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 20/01/2015
	 * @description Retorna el query nombrado.
	 *              <p>
	 *              Si el parametro <code>format</code> es igual
	 *              <code>true</code> entonces retorna el query tal como se
	 *              definio en el archivo xml, de lo contrario retorna el query
	 *              sin tabulaciones y saltos de linea.
	 *              </p>
	 * @param name
	 * @param format
	 *            se utiliza para decidir si mantene el formato definido en el
	 *            xml.
	 * @return
	 */
	String getNamedQuery(String name, boolean format);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param name
	 * @param namedTableList
	 * @return
	 */
	String getNamedQuery(String name, List<NamedTable> namedTableList);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @param name
	 * @param queryClause
	 * @return
	 */
	String getNamedQuery(String name, NamedQueryEnum queryClause);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 14/01/2015
	 * @description
	 * @param query
	 * @param namedTableList
	 * @return
	 */
	String getQueryFromNamedQuery(String query, List<NamedTable> namedTableList);

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 13/01/2015
	 * @description
	 * @return
	 */
	String getXmlLocation();

}
