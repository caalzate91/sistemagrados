package co.edu.javerianacali.grados.utils.gridview;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class SearchFilter
 * @description
 * @date 31/12/2014
 */
public class SearchFilter {

	private String property;
	private String value;
	private String comparison;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 */
	public SearchFilter(String property, String value) {
		this.property = property;
		this.value = value;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/07/2015
	 * @param property
	 * @param value
	 * @param comparison
	 */
	public SearchFilter(String property, String value, String comparison) {
		super();
		this.property = property;
		this.value = value;
		this.comparison = comparison;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 31/12/2014
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/07/2015
	 * @return the comparison
	 */
	public String getComparison() {
		return comparison;
	}

}
