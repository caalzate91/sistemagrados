package co.edu.javerianacali.grados.utils.namedquery;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class NamedTable
 * @description
 * @date 12/01/2015
 */
public class NamedTable {

	private String namedTable;
	private String table;
	private String namedAlias;
	private String alias;

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @param namedTable
	 * @param table
	 */
	public NamedTable(String namedTable, String table) {
		this.namedTable = namedTable;
		this.table = table;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @param namedTable
	 * @param table
	 * @param namedAlias
	 * @param alias
	 */
	public NamedTable(String namedTable, String table, String namedAlias,
			String alias) {
		this.namedTable = namedTable;
		this.table = table;
		this.namedAlias = namedAlias;
		this.alias = alias;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @return the namedTable
	 */
	public String getNamedTable() {
		return namedTable;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @return the namedAlias
	 */
	public String getNamedAlias() {
		return namedAlias;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @param namedAlias
	 *            the namedAlias to set
	 */
	public void setNamedAlias(String namedAlias) {
		this.namedAlias = namedAlias;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/01/2015
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

}
