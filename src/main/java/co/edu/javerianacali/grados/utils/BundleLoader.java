package co.edu.javerianacali.grados.utils;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana Cardona</a>
 * @project HomologacionAsignaturas
 * @class BundleLoader
 * @description
 * @date 30/12/2014
 */
public final class BundleLoader {
	
	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana Cardona</a> 
	 * @date 30/12/2014
	 */
	private BundleLoader() {		
	}
	
	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana Cardona</a> 
	 * @date 30/12/2014
	 * @description 
	 * @param resourceBundle
	 * @param code
	 * @param params
	 * @param locale
	 * @return
	 */
	public static final String getMessage(String resourceBundle, String code, Object[] params, Locale locale) {
		if(null == code || code.trim().equals("")) {
			throw new IllegalArgumentException("bundleKey es nulo");
		}
		
		if(null == resourceBundle || resourceBundle.trim().equals("")) {
			throw new IllegalArgumentException("resourceBundle es nulo");
		}
		
		if(null == locale) {
			throw new IllegalArgumentException("BundleLoader locale es nulo");
		}
		
		ResourceBundle bundle =  ResourceBundle.getBundle(resourceBundle, locale);
		if(!bundle.containsKey(code)) {
			return code;
		}
		
		String message = bundle.getString(code);
		if(null == message || message.trim().equals("")) {
			return code;
		}
		
		if(params != null) {
			for(int i = 0; i < params.length; i++) {
				StringBuilder regex = new StringBuilder();
				regex.append("{");
				regex.append(i);
				regex.append("}");
				message = message.replace(regex, params[i].toString());
			}
		}
		
		return message;
	}
	
}
