package co.edu.javerianacali.grados.enums;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class PatternEnum
 * @description
 * @date 8/01/2015
 */
public enum PatternEnum {

	DATE_EFFDT("dd/MM/yyyy-HH:mm:ss"),
	DATE_EFFDT_FOR_CRITERIA_QUERY("DD/MM/YYYY-HH24:MI:SS"),
	DEFAULT_TOKEN_CHARS("abcdefghijklmnopqrstuvwxyz0123456789"),
	DATE_FORMAT_FOR_PDF("dd_MMM_yyy");

	private String pattern;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/01/2015
	 * @param pattern
	 */
	private PatternEnum(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/01/2015
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/01/2015
	 * @param pattern
	 *            the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
