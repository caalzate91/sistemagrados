/**
 * 
 */
package co.edu.javerianacali.grados.enums;

/**		
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
  
</a>	
 * @project HomologacionAsignaturas	
 * @class RequestEnum	
 * @description	
 * @date 20/02/2015	
 *	
 */
public enum RequestEnum {
	REQUEST_AJAX("x-requested-with");
	
	private String request;

	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
	</a>	
	 * @date 20/02/2015	
	 * @param request	
	 */	
	  
	private RequestEnum(String request) {
		this.request = request;
	}

	/**	
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
	</a>	
	 * @date 20/02/2015	
	 * @returnthe request	
	 */
	
	public String getRequest() {
		return request;
	}

	/**	
	 *@author <a href="mailto:jordy.rodriguez@premize.com">
	</a>	
	 * @date 20/02/2015	
	 * @param request	
	 the request to set	
	 */
	
	public void setRequest(String request) {
		this.request = request;
	}
	
	
}

