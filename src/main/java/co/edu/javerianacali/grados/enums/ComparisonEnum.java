package co.edu.javerianacali.grados.enums;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class ComparisonEnum
 * @description
 * @date 12/07/2015
 */
public enum ComparisonEnum {

	/**
	 * EQUALS
	 */
	EQ("="),

	/**
	 * LIKE
	 */
	LQ("LIKE"),

	/**
	 * GREATER THAN
	 */
	GT(">"),

	/**
	 * LESS THAN
	 */
	LT("<");

	private String value;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/07/2015
	 * @param value
	 */
	private ComparisonEnum(String value) {
		this.value = value;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 12/07/2015
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
