package co.edu.javerianacali.grados.enums;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project HomologacionAsignaturas
 * @class RegexEnum
 * @description Enumerador para almacenar expresiones regulares o caracteres
 *              especiales para uso en general de la aplicacion.
 * @date 8/01/2015
 */
public enum RegexEnum {
	
	COMPOSITE_PK("&"),
	COMPOSITE_DESCRIPTION("-"),
	ROLE_SEPARATOR("_");

	private String regex;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/01/2015
	 * @param regex
	 */
	private RegexEnum(String regex) {
		this.regex = regex;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 8/01/2015
	 * @return the regex
	 */
	public String getRegex() {
		return regex;
	}

}
