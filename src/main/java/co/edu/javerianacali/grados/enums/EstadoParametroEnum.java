package co.edu.javerianacali.grados.enums;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana
 *         Cardona</a>
 * @project SistemaGrados
 * @class EstadoParametroEnum
 * @description
 * @date 3/07/2015
 */
public enum EstadoParametroEnum {
	ACTIVO("Activo"), INACTIVO("Inactivo");

	private String label;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 3/07/2015
	 * @param label
	 */
	private EstadoParametroEnum(String label) {
		this.label = label;
	}

	/**
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 3/07/2015
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

}
