/**
 * 
 */
package co.edu.javerianacali.grados.enums;

/**		
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez
  
</a>	
 * @project HomologacionAsignaturas	
 * @class QueryPartEnum	
 * @description	
 * @date 14/01/2015	
 *	
 */
public enum QueryPartEnum {
   FROM,
   SELECT
}
