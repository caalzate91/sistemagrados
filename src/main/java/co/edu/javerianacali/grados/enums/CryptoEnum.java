/**
 * 
 */
package co.edu.javerianacali.grados.enums;

/**
 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
 * @project HomologacionAsignaturas
 * @class CryptEnum
 * @description
 * @date 4/03/2015
 * 
 */
public enum CryptoEnum {

	CRYPTO_ENCRYPTION_KEY("DES"), CRYPTO_ENCRYPTION_ALGORITHM(
			"DES/ECB/PKCS5Padding"), CRYPTO_CHARSET_CODIFICATION("UTF8");
	private String crypto;

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 4/03/2015
	 * @param crypto
	 */
	private CryptoEnum(String crypto) {
		this.crypto = crypto;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com">Jordy Rodriguez</a>
	 * @date 4/03/2015
	 * @returnthe crypto
	 */
	public String getCrypto() {
		return crypto;
	}

	/**
	 * @author <a href="mailto:jordy.rodriguez@premize.com"></a>
	 * @date 4/03/2015
	 * @param crypto
	 *            the crypto to set
	 */
	public void setCrypto(String crypto) {
		this.crypto = crypto;
	}

}
