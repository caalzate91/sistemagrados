package co.edu.javerianacali.grados.enums;

/**
 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander Filigrana Cardona</a>
 * @project SistemaGrados
 * @class DatabaseEnum
 * @description
 * @date 7/07/2015
 */
public enum DatabaseEnum {
	
	TABLE_PARAMETRO("PARAMETRO"), 
	TABLE_TIPO_PARAMETRO("TIPO_PARAMETRO"), 
	SEQUENCE_PARAMETRO("SEQ_CONS_PARAMETRO");

	private String value;

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 26/12/2014
	 * @param name
	 */
	private DatabaseEnum(String value) {
		this.value = value;
	}

	/**
	 * 
	 * @author <a href="mailto:jhona.filigrana@premize.com">Jhon Alexander
	 *         Filigrana Cardona</a>
	 * @date 26/12/2014
	 * @description
	 * @return
	 */
	public String getValue() {
		return value;
	}
}
