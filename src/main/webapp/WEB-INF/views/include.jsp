<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Variables CSS -->
<spring:url value="/resources/ext/resources/css/ext-all.css" var="extAllCSS" />
<spring:url value="/resources/css/menu.css" var="menuCSS" />
<spring:url value="/resources/css/pages.css" var="pagesCSS" />
<spring:url  value="/resources/ext/ux/grid/css/RangeMenu.css" var="RangeMenuCSS"/>
<spring:url  value="/resources/ext/ux/css/CheckHeader.css" var="checkHeaderCSS"/>
<spring:url  value="/resources/css/plantilla.css" var="plantilla"/>
<spring:url  value="/resources/css/soft_button.css" var="button"/>

<!-- Variables JS -->
<spring:url value="/resources/ext/ext-all.js" var="extAllJS" />
<spring:url value="/resources/ext/ext-all-debug.js" var="extAllDebugJS" />
<spring:url value="/resources/ext/ux/CheckColumn.js" var="checkColumnJS" /> 
<spring:url value="/resources/ext/ux/form/MultiSelect.js" var="MultiSelectJS" />
<spring:url value="/resources/ext/ux/ComponentColumn.js" var="componentColumnJS" />
<spring:url value="/resources/ext/MessageBox.js" var="messageBoxJS" />
<spring:url value="/resources/ext/ext-lang-es.js" var="extLangEsJS" />
<spring:url value="/resources/js/views/app.js" var="appJS" />
<spring:url value="/resources/js/jquery-1.6.2.js" var="jQueryJS" />
<spring:url value="/resources/js/menu.js" var="menuJS" />

<!-- CSS -->
<link href="${plantilla}" rel="stylesheet" type="text/css" />
<link href="${extAllCSS}" rel="stylesheet" type="text/css" />
<link href="${menuCSS}" rel="stylesheet" type="text/css" />
<link href="${pagesCSS}" rel="stylesheet" type="text/css" />
<link href="${RangeMenuCSS}" rel="stylesheet" type="text/css" />
<link href="${checkHeaderCSS}" rel="stylesheet" type="text/css" />
<link href="${button}" rel="stylesheet" type="text/css" />
<!-- JS -->
<%-- <script src="${extAllJS}" type="text/javascript" charset="utf-8"></script> --%>
<script src="${extAllDebugJS}" type="text/javascript" charset="utf-8"></script>
<script src="${checkColumnJS}" type="text/javascript" charset="utf-8"></script>
<script src="${componentColumnJS}" type="text/javascript" charset="utf-8"></script>
<script src="${MultiSelectJS}" type="text/javascript" charset="utf-8"></script>
<script src="${messageBoxJS}" type="text/javascript" charset="utf-8"></script>
<script src="${extLangEsJS}" type="text/javascript" charset="utf-8"></script>
<script src="${appJS}" type="text/javascript" charset="utf-8"></script>
<script src="${jQueryJS}" type="text/javascript" charset="utf-8"></script>
<script src="${menuJS}" type="text/javascript" charset="utf-8"></script>

</head>

</html>