<%@include file="/WEB-INF/views/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	src='<c:url value="/core/logic/parametros/parametros.js"></c:url>'></script>
<title><spring:message code="pageTitle1" /></title>
</head>
<body>
	<%-- --%>	
	<input readonly type="hidden" id="pageStatus" value="${pageStatus}" />
	<input type="hidden" id="data" value="${data}" />
			<div id="header" class="bloque_cabecera">
					 <h:menu />
		 </div>
	<div class="pagina" id="container">
		<div id="panelCentral" class="bloque_central">  
			<div id="header" class="header">		
			<div id="panelInformativo" class="content-panel" style="width:400px; margin:0 auto; text-align:middle;"></div>
			<div id="panelGrid" class="content-panel"></div>	
			<div id="cabeceraActualizacion"  class="labelTop-top"></div>	
			<div id="contenedor" style="width: 100%;">
				<div id="panelActualizacionPersonal"  class="content-panel"  style="display: table-cell;"></div>
				<div id="panelActualizacionLaboral"  class="content-panel" style="display: table-cell;"></div>	
			</div>		
			<div id="panelAnexos"  class="content-panel"></div>				
	      </div>
	 
		</div>
		<div id="footer" class="bloque_pie"></div>		
	</div> 
</body>
</html>