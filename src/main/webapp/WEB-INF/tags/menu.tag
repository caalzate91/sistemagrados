<!-- Menu de la aplicacion -->
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="h" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div>

<spring:url value="/actas/principal.htm" var="urlActaPrincipal"></spring:url>
<spring:url value="/solicitudes/principal.htm" var="urlSolicitudes"></spring:url>
<spring:url value="/home" var="urlHome"></spring:url>
<spring:url value="/resources/images/icons/home.png" var="homeIcon"></spring:url>
<spring:url value="/parametros/programas.htm" var="urlProgramas"></spring:url>
<spring:url value="/logout" var="urlCerrarSesion"></spring:url>
<spring:url value="/parametros/asignaturas.htm" var="urlAsignaturas"></spring:url>
<spring:url value="/aprobaciones/principal.htm" var="urlAprobaciones"></spring:url>
<div class="menu-header">
	<div class="banner-page">
		<img src="http://www.javerianacali.edu.co/sites/ujc/files/img-logoHeader.png">
	</div>
	<ul>
		<li><span></span><a href="${urlAprobaciones}"> <spring:message code="registrarGrado" /> </a></li>
		<li class="logout-item"><span></span><a href="${urlFechas}"> <spring:message code="fechasActividades"/></a></li>
		<li class="logout-item"><span></span><a href="${urlPazYSalvo}"> <spring:message code="pazYSalvo"/></a></li>
	</ul>
</div>

</div>