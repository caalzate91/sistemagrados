/**
 * 
 */
/*Ext.Loader.setConfig({
	enabled : true,
	paths: {
		'Ext.ux': 'resources/ext',
//		'HA': 'core/app'
	}
});*/
//Ext.Loader.setPath('Ext.ux.grid', '/resources/ext/grid');
Ext.Loader.setConfig({enabled : true});
Ext.Loader.setPath('Ext.ux', '../resources/ext/ux');
Ext.Loader.setPath('Ext.ux.grid', '../resources/ext/ux/grid');
Ext.Loader.setPath('Ext.ux.CheckColumn', '../resources/ext/ux/CheckColumn');
Ext.require([
     		'Ext.selection.CellModel',
     		'Ext.grid.*',
     		'Ext.data.*',
     		'Ext.util.*',
     		'Ext.state.*',
     		'Ext.form.*',
     		'Ext.Action',
     		'Ext.ux.grid.FiltersFeature' ,
     		'Ext.ux.CheckColumn'
]);

var comboTipoParametro = null;
var formParametro = null;
var view = null;
var store2 = null;
var storePrincipal = null;
var panelActualizacionPersonal = null;
var panelActualizacionLaboral = null;
function initComponents() {
	
	comboProgramaInterno = Ext.create('APP.view.comp.CbxProcesosGrado',{
		name : 'cmbProcesosGrado',
		fieldLabel : '',
		autoload: true,
		url : 'grados/procesosGrado',
		beforeLabelTextTpl: '',
		anchor : '100%',
		valueField : 'consecutivo',
		displayField : 'descripcion', 
		listeners: {
			select: function(combo, record) { 
			//	onComboProgramaInternoSelect(combo, record);
			}
		}		
	});
	
	Ext.Ajax.request({
		url : 'estudiante/consultarEstudiante',
		method : 'GET',
		params : {
			empleidEstudiante : '0068750',
		},
		success : function(response) {
			var jresponse = JSON.parse(response.responseText);

			console.log(jresponse);
			infoBasica = Ext.create('Ext.form.Panel', {
			    title: 'InsCripción a Grados',
			    width: 400,
			    bodyPadding: 10,
			    renderTo: 'panelInformativo',
			    layout: {
			        type: 'vbox',
			        align: 'middle' 
			    },
			    items: [ {
			        xtype: 'label',
			        forId: 'myFieldId1',        
			        text: 'Nombre:   ' + jresponse.result.nombres  + " " + jresponse.result.apellidos,
			        margins: '0 0 0 10'
			    },
			    { 
			        xtype: 'label',
			        forId: 'myFieldId2',
			        text: 'Tipo de doc:' + jresponse.result.tipoDocumento,
			        margins: '10 0 0 10' 
			    },
			    {
			        xtype: 'label',
			        forId: 'myFieldId3',
			        text: 'Número de doc:' + jresponse.result.documento,
			        margins: '10 0 0 10'
			    }
			    ]
			});
			
			
		    var newPanel = new Ext.Panel({
					    renderTo: 'cabeceraActualizacion',
		    	        border: false, 
		    	        items: [{
                            xtype: 'displayfield',
                            fieldLabel: '',
                            value: '<span style="color:blue;font-size:20px">Actualización de datos</span>'
                        }
                        ]
		    	    });

		    
			panelActualizacionPersonal =  Ext.create('Ext.form.Panel', {
			    title: 'Información Personal',
			    width: 460, 
			    bodyPadding: 10,
			    renderTo: 'panelActualizacionPersonal',
			    layout: {
			        type: 'vbox',
			        align: 'middle' 
			    },
			    items: [{
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Correo",
					      value: jresponse.result.correo,
					      maxLength : 120,
					      allowBlank: true
				      },
				      {
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Teléfono fijo",
					      value: jresponse.result.telefonoFijo,
					      maxLength : 120,
					      allowBlank: true
					 },
					 {
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Dirección residencia",
					      value: jresponse.result.direccion,
					      maxLength : 120,
					      allowBlank: true
					},
					 {
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Celular",
					      value: jresponse.result.celular,
					      maxLength : 120,
					      allowBlank: true
					}]
			});
			
			panelActualizacionLaboral =  Ext.create('Ext.form.Panel', {
			    title: 'Información Laboral',
			    width: 460, 
			    bodyPadding: 10,
			    renderTo: 'panelActualizacionLaboral',
			    layout: {
			        type: 'vbox',
			        align: 'middle' 
			    },
			    items: [{
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Empresa",
					      value: jresponse.result.empresa,
					      maxLength : 120,
					      allowBlank: true
				      },
				      {
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Cargo",
					      value: jresponse.result.cargo,
					      maxLength : 120,
					      allowBlank: true
					 },
					 {
					      xtype: 'textfield',
					      name:'',
					      fieldLabel: "Correo empresa",
					      value: jresponse.result.correoEmpresa,
					      allowBlank: true
					},
					 {
					      xtype: 'textfield',
					      name:'codigoSnies',
					      fieldLabel: "Tel.Oficina",
					      value: jresponse.result.telefonoOficina,
					      maxLength : 120,
					      allowBlank: true
					}] 
			});
		}
	});

	storePrincipal = Ext.create('Ext.data.Store', {
		storeId: 'storeProgramaEstudiante',
		model: 'APP.model.ProgramaEstudianteModel',
		autoLoad: true,
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: 'estudiante/consultarProgramaEstudiante',
			extraParams: {
				empleidEstudiante : '0068750',
			},
			reader: {
				type: 'json',
				root: 'result'
			},
			encodeFilters: APP.util.GridUtil.encodeFilters,
			listeners: {
				exception: function(proxy, response, operation) {
					APP.util.AlertMessage.showExceptionMessage('Error', response, true);
				}
			}
		},
		listeners : {
			beforeLoad : function(store,operation,options) {
//				store.proxy.extraParams = {globalSearch: searchFilter.getValue()};
				return true;
			}
		},
//		filters: [{property: 'clave', data:{comparison:'lq'}}],
		remoteSort: true,
		remoteFilter: true,
	});
	
	/*Ext.Ajax.request({
		url : 'estudiante/consultarProgramaEstudiante',
		method : 'GET',
		params : {
			empleidEstudiante : '0036378',
		},
		success : function(response) {
			var jresponse = JSON.parse(response.responseText);

			console.log(jresponse);
	
		}
	});*/

	var myStore = Ext.create('Ext.data.Store',{
	    fields:['Id','Name'],
	    data:[
	        {Id:0,Name:'Yes'},
	        {Id:1,Name:'No'},
	        {Id:2,Name:'Maybe'}
	    ]
	});


	view = Ext.create('APP.view.genericViewCmp.GenericGridPanel', {
			title : 'Panel informativo',
			store : storePrincipal,
			renderTo: 'panelGrid',
			columns : [ {
				xtype : 'gridcolumn',
				text : 'Programas',
				dataIndex : 'nombrePrograma',
				width : 100,
				filterable : true,
				autoEditor : true
			}, {
				xtype : 'gridcolumn',
				text : 'Créditos aprobados',
				dataIndex : 'creditosAprobados',
//				flex : 1,
				width : 200,
				filterable : true,
				autoEditor : true
			}, {
				xtype : 'gridcolumn', 
				text : 'Créditos requeridos',
				dataIndex : 'd',
				width : 200,
				sortable : false,
				autoEditor : true
			}, {
				xtype: 'checkcolumn',
				text : 'Inscribir a grado',
				dataIndex : 'flagInscripcion'

			},
		        {header: 'Name',  dataIndex: 'creditosRequeridos',
		            editor: {
		                xtype: 'combo',
		                store: ['First', 'Second']
		            }}
		           ],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: [comboProgramaInterno]
            }],
			cls : 'content-panel' 
	    });
	
	var myForm =  Ext.create('Ext.form.Panel', {
		title: '',
		renderTo : 'panelAnexos',
		bodyPadding : 10,
		border: false, 
		buttonAlign: 'center',
//		width: 800,
//		heigth : 500,
		url: "estudiante/guardarEstudiante",
		fieldDefaults : { margin: '5 5 5 5'},
		layout : {
			 type: 'hbox', 
			 pack: 'center', 
			 align: 'center' 
		},
		layoutConfig: {
            padding: 10
        },
		items : [
		     {
                xtype: 'displayfield',
                fieldLabel: '',
                value: '<span style="color:black;font-size:14px">Documento identificación </span>'
               },
		      { xtype : "button", text : "Buscar ",   cls:'button'},
		      {
                xtype: 'displayfield',
                fieldLabel: '',
                value: '<span style="color:black;font-size:14px">Foto para carné de egresado </span>'
                },
		      { xtype : "button", text : "Buscar ",   cls:'button'}		      
		],
		buttons : [
		      
		      {
		    	  text:'Guardar',
		    	  handler: function(){
		    		  var form = this.up('form').getForm();
		    		//  var comboIsValid = HA.util.ComboBoxUtil.isValid(comboUniversidad,'universidad');
		    		  if(form.isValid() /*&& comboIsValid == true*/){
		    			  form.submit({
		    				  success: function(form,action){
		    						    					  
		    				  },
		    				  failure : function(form,action){
		    					  Ext.Msg.alert('Error al Guardar el Programa',action.result.exceptionMessage);
		    				  }
		    			  });
		    		  }
		    	  },
		    	  cls:'button'
		    	  
		      },
			      {
			    	  text: "Finalizar inscripción",
			    	  handler : function(){
			    		  this.up('form').getForm().reset();
			    	  },
			    	  cls:'button'
			      }
		]
	});
	
	
}

Ext.application({
	name: 'APP',
	appFolder: '/SistemaGrados/core/app',
	requires: ['APP.model.ProgramaEstudianteModel',
	           'APP.store.GridStore',
	           'APP.view.comp.SearchFilter',
	           'APP.view.genericViews.RowEditingView',
	           'APP.view.genericViewCmp.GenericFormPanel',
	           'APP.view.genericViewCmp.GenericGridPanel',
	           'APP.util.AppConstants',
	           'APP.util.AlertMessage',
	           'APP.util.GridUtil',
	           'APP.util.ComboBoxUtil'],
	launch: function() {
//		getLocaleFile();
		initComponents();
	}
});

function onParametroSave() {
	  var form = this.up('form').getForm();
	  var cbxTipoParametro = this.up('cbxTipoParametro');
	  var comboIsValid = APP.util.ComboBoxUtil.isValid(comboTipoParametro,'Tipo Parametro');
	  if(form.isValid() && comboIsValid){
		  form.submit({
			  success: function(form,action){
				  Ext.data.StoreManager.lookup('storeParametros').load({
					  callback : function(records, operation,success){
						  form.reset();
					  }
				  });
			  },
			  failure : function(form,action){
				  Ext.Msg.alert('Error al guardar el Parametro',action.result.exceptionMessage);
			  }
		  });
	  }
}

Ext.onReady(function () {
	console.log("App is Ready");
	
});