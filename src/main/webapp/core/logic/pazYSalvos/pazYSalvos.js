/**
 *  JONATHAN ALMACHE MONTOYA - Pantalla de paz y salvos
 */

/**
 * 
 */

Ext.Loader.setConfig({enabled : true});
Ext.Loader.setPath('Ext.ux', '../resources/ext/ux');
Ext.Loader.setPath('Ext.ux.grid', '../resources/ext/ux/grid');
Ext.Loader.setPath('Ext.ux.CheckColumn', '../resources/ext/ux/CheckColumn');
Ext.require([
     		'Ext.selection.CellModel',
     		'Ext.grid.*',
     		'Ext.data.*',
     		'Ext.util.*',
     		'Ext.state.*',
     		'Ext.form.*',
     		'Ext.Action',
     		'Ext.ux.grid.FiltersFeature' ,
     		'Ext.ux.CheckColumn'
]);

var comboTipoParametro = null;
var formParametro = null;
var view = null;
var store2 = null;
var storePrincipal = null;
var panelActualizacionPersonal = null;
var panelActualizacionLaboral = null;

function initComponents() {			
		
	storePrincipal = Ext.create('Ext.data.Store', {
		storeId: 'storeProgramaEstudiante',
		model: 'APP.model.ProgramaEstudianteModel',
		autoLoad: false,
		pageSize: 5,
		proxy: {
			type: 'ajax',
			url: 'estudiante/consultarProgramaEstudiante',
			extraParams: {
				empleidEstudiante : '0068750',
			},
			reader: {
				type: 'json',
				root: 'result'
			},
			encodeFilters: APP.util.GridUtil.encodeFilters,
			listeners: {
				exception: function(proxy, response, operation) {
					APP.util.AlertMessage.showExceptionMessage('Error', response, true);
				}
			}
		},
		listeners : {
			beforeLoad : function(store,operation,options) {
//				store.proxy.extraParams = {globalSearch: searchFilter.getValue()};
				return true;
			}
		},
//		filters: [{property: 'clave', data:{comparison:'lq'}}],
		remoteSort: true,
		remoteFilter: true,
	});
	
	/*Ext.Ajax.request({
		url : 'estudiante/consultarProgramaEstudiante',
		method : 'GET',
		params : {
			empleidEstudiante : '0036378',
		},
		success : function(response) {
			var jresponse = JSON.parse(response.responseText);

			console.log(jresponse);
	
		}
	});*/


	view = Ext.create('APP.view.genericViewCmp.GenericGridPanel', {
			title : 'Panel de paz y salvos',
			store : storePrincipal,
			renderTo: 'panelGrid',
			width: 750,
			columns : [ {
				xtype : 'gridcolumn',
				text : 'Instancias de aprobación',
				dataIndex : 'instancias',
				flex : 1,
				width : 250
			}, {
				xtype : 'gridcolumn',
				text : 'Estado paz y salvo',
				dataIndex : 'estado',
				flex : 1,
				width : 250
			}, {
				xtype : 'gridcolumn', 
				text : 'Observaciones',
				dataIndex : 'observaciones',
				flex : 1,
				width : 250
			}], 
			cls : 'content-panel' 
	    });
	
	
}

Ext.application({
	name: 'APP',
	appFolder: '/SistemaGrados/core/app',
	requires: ['APP.model.ProgramaEstudianteModel',
	           'APP.store.GridStore',
	           'APP.view.comp.SearchFilter',
	           'APP.view.genericViews.RowEditingView',
	           'APP.view.genericViewCmp.GenericFormPanel',
	           'APP.view.genericViewCmp.GenericGridPanel',
	           'APP.util.AppConstants',
	           'APP.util.AlertMessage',
	           'APP.util.GridUtil',
	           'APP.util.ComboBoxUtil'],
	launch: function() {
//		getLocaleFile();
		initComponents();
	}
});

Ext.onReady(function () {
	console.log("App is Ready");
	
});