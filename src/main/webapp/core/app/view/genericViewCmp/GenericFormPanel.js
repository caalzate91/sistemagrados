Ext.define('APP.view.genericViewCmp.GenericFormPanel',{
	extend : 'Ext.form.Panel',
	alias  : 'widget.genericFormPanel',
	config : {
		
	},
	constructor : function(config){
		this.initConfig(config);
		Ext.apply(this,{
			title : this.title,
			url   : this.url,
			width : this.width,
			bodyPadding : this.bodyPadding,
			height: this.height,
			renderTo : this.renderTo,
			cls : this.cls,
			fieldDefaults : {labelAlign : 'left',labelWidth: 150, anchor : '90%'},
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			items : this.items,
			buttons : this.buttons
		});
		this.callParent(arguments);
	}
});