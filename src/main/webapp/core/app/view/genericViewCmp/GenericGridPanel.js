Ext.define('APP.view.genericViewCmp.GenericGridPanel',{
	extend : 'Ext.grid.Panel',
	alias  : 'widget.genericGridPanel',
	config : {
		
	},
	constructor : function(config){
		this.initConfig(config);
		Ext.apply(this,{
			listeners : {
//				afterrender : function(thiz,eOpts){
//					thiz.filters.createFilters();
//				}
			}
			
		});
		
		this.callParent(arguments);
	}
});