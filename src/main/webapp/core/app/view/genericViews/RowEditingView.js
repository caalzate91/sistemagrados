
Ext.define('APP.view.genericViews.RowEditingView',{
	extend : 'Ext.container.Container',
	alias  : 'widget.genericView',
	config : {
		viewName : '',
		createUrl  : null,
		updateUrl  : null,
		deleteUrl  : null,
		form : {
			fields : null,
			buttons: null,
			title  : null,
			width  : null,
			heigth : null,
			renderTo : null,
			bodyPadding : null,
			cls : null,
			renderTo : null,
		},
		gridPanel : {
			title  : null,
			width  : null,
			heigth : null,
			columns: null,
			plugins : null,
			dockedItems : null,
			features : null,
			store   : null,
			cls : null,
			renderTo : null,
		},
		rowEditing : {
			updateBtnText : null,
			cancelBtnText: null,
			clicksToEdit : null,
			params : null
		},
		deleteRow : {
			icon : null,
			params : null
		}
	},
	constructor : function(config){
		this.initConfig(config);
		var view = this;
		this.setEditors();
		this.addDeleteColumn();
		
		var rowEdit =  Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToEdit: this.rowEditing.clicksToEdit,
			saveBtnText : this.rowEditing.updateBtnText,
			cancelBtnText :  this.rowEditing.cancelBtnText,
			listeners : {
				edit : function(editor, context, eOpts){
					view.onEdit(editor, context, eOpts);
				}
			}
        });
		
		var grid = Ext.create('APP.view.genericViewCmp.GenericGridPanel',{
			xtype : 'genericGridPanel',
			title : this.gridPanel.title,
			columns : this.gridPanel.columns,
			plugins : this.gridPanel.plugins,
			dockedItems : this.gridPanel.dockedItems,
			cls : this.gridPanel.cls,
			heigth :  this.gridPanel.heigth,
			store : this.gridPanel.store,
			renderTo : this.gridPanel.renderTo,
			features : this.gridPanel.features
		});
		
		
		rowEdit.init(grid);
		Ext.apply(this,{
			renderTo : 'container',
			items : [
			     {
			    	xtype : 'genericFormPanel',
			    	url : this.createUrl,
			    	title : this.form.title,
			    	width : this.form.width,
			    	heigth: this.form.heigth,
			    	items : this.form.fields,
			    	buttons: this.form.buttons,
			    	cls : this.form.cls,
			    	renderTo : this.form.renderTo,
			    	bodyPadding : this.form.bodyPadding
			     },
			    grid
			]
		});
		this.callParent(arguments);
	},
	setEditors : function(){
		var columns = this.gridPanel.columns;
		var fields = this.gridPanel.store.model.getFields();
		console.log(this.gridPanel.store.model);
		for(var indiceColumns = 0 ; indiceColumns < columns.length; indiceColumns++){
			column = columns[indiceColumns];
			for(var indiceFields = 0; indiceFields < fields.length; indiceFields++){
				field = fields[indiceFields];
				if(field.name == column.dataIndex){
					if(column.autoEditor){
						var xtype = 'textfield';
						if(field.type.type == 'int')
						{
							xtype = 'numberfield';
						}
						var editor = {
							xtype : xtype,
							name : field.name,
							id : field.id,
							allowBlank : false
						};
						column.editor = editor;
					}
				}
			}
		}
	},
	addDeleteColumn: function(){
		var rowEditingView = this;
		var column = {
			xtype : 'actioncolumn',
			width : 50,
			icon : this.deleteRow.icon,
			handler : function(view, rowIndex, colIndex, item, e, record){
				var params = rowEditingView.getParamsByRecord(rowEditingView.deleteRow.params, record);
				Ext.Ajax.request({
					url : rowEditingView.deleteUrl,
					params : params,
					success : function(response){
						var resData = Ext.JSON.decode(response.responseText);
						if(resData.status == APP.util.AppConstants.JSON_RESPONSE_STATUS_SUCCESS){
							rowEditingView.gridPanel.store.load({
								  callback : function(records, operation,success){
									  APP.util.AlertMessage.displayMessage(
										'Eliminar '+rowEditingView.viewName, 
										'El '+rowEditingView.viewName+' se elimino correctamente',
										APP.util.AppConstants.SEVERITY_INFO,
										true);
								  }
							  });
						}else{
							APP.util.AlertMessage.displayMessage('Error', resData.exceptionMessage, resData.severity, true);
						}
					},
					failure : function(response){
						var resData = Ext.JSON.decode(response.responseText);
						APP.util.AlertMessage.displayMessage('Error', resData.exceptionMessage, resData.severity, true);
					}
				});
			}
		};
		this.gridPanel.columns.push(column);
	},
	onEdit : function(editor, context, eOpts){
		var view = this;
		var record = context.record;
		var ajaxParams = this.getParamsByRecord(view.rowEditing.params,record);
		
		Ext.Ajax.request({
			url : this.updateUrl,
			method : 'POST',
			params : ajaxParams,
			success : function(response){
				var resData = Ext.JSON.decode(response.responseText);
				if(resData.status == APP.util.AppConstants.JSON_RESPONSE_STATUS_SUCCESS){
					view.gridPanel.store.load({
						  callback : function(records, operation,success){
							  APP.util.AlertMessage.displayMessage(
								'Actualizar '+view.viewName, 
								'El '+view.viewName+' se actualizo correctamente',
								APP.util.AppConstants.SEVERITY_INFO,
								true);
						  }
					  });
				}else{
					APP.util.AlertMessage.displayMessage('Error', resData.exceptionMessage, resData.severity, true);
					view.gridPanel.store.load();
				}
			},
			failure : function(response){
				var resData = Ext.JSON.decode(response.responseText);
				APP.util.AlertMessage.displayMessage('Error', resData.exceptionMessage, resData.severity, true);
			}
		});
	},
	getParamsByRecord : function(params,record){
		var ajaxParams = new Object();
		for(var indiceParams = 0; indiceParams < params.length; indiceParams++){
			param = params[indiceParams];
			ajaxParams[param] = record.get(param);
		}
		return ajaxParams;
	}
	
	
	
	
});