/**
 * 
 */
Ext.define('APP.view.comp.CbxProcesosGrado', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.cbxProcesosGrado',
	requires: ['APP.model.ProcesosGradoModel'],
	config: {
		url: null,
		fieldLabel: null
	},
	constructor: function(config) {
		this.initConfig(config);
		Ext.apply(this, {
			fieldLabel: this.fieldLabel,
			beforeLabelTextTpl: lblRequiredBefore,
			forceSelection: true,
			pageSize: false,
			queryMode: 'remote',	 
			editable: true, 
			allowBlank: false,
			anchor: '50%',
			width: 260,
			maxLength: 50,
			minChars: 3,
			hideTrigger: true,
			store: {
				model: 'APP.model.ProcesosGradoModel', 
				autoLoad: false,
				pageSize: 100,
				proxy:{
					type: 'ajax', 
					url: this.url,
					reader: {
						type: 'json',
						root: 'result'
					},
					extraParams:{query:""},
					listeners: {
						exception: function (proxy, response, operation) {
							APP.util.AlertMessage.showExceptionMessage("Error", response, true);
						}
					}
				}
			}
		});
		this.callParent(arguments);
	}
});