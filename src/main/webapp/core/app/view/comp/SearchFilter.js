/**
 * 
 */
Ext.define('APP.view.comp.SearchFilter', {
	extend: 'Ext.form.field.Text',
	alias: 'widget.searchfilter',
	config: {
		store: null, 
		searchFilterFn: null,
		minChars: 3,
		searchDelay: 500,
	},
	constructor: function(config) {
		this.initConfig(config);
		var task = Ext.create('Ext.util.DelayedTask', this.searchFilterFn);
		Ext.apply(this, {
			allowBlank: true,
			width: 250,
			enableKeyEvents: true,
			minChars : 3,
			listeners: {
				change: function(textfield, event, options) {
					if(this.minChars) {						
						if(event.length >= this.minChars) {
							task.delay(this.searchDelay, Ext.bind(this.searchFilterFn, this,
									[event], false));						
						} else if(event.length == 0) {
							this.searchFilterFn('');
						}
					} else {
//						this.searchFilterFn(event);
					}
					
				}
			}
		});
		this.callParent(arguments);
	},
	searchFilterFn: function(value) {
//		console.log("value: "+this.value);
//		console.log("arguments-0: "+arguments[0]);
	},
	getMinChars: function() {
		return this.minChars;
	}
});