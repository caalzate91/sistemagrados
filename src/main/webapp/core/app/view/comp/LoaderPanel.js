/**
 * 
 */
Ext.define('APP.view.comp.LoaderPanel',{
	extend: 'Ext.panel.Panel',
	alias: 'widget.loaderPanel',
	frame: false,
	config: {
		urlPage: null,
		pageParams: null
	},
	loader: {},
	constructor: function(config) {
		this.initConfig(config);
		this.callParent(arguments);
	},
	
	load: function() {
		this.getLoader().load({
			url: this.urlPage,
			params: this.pageParams
		});
	}
})