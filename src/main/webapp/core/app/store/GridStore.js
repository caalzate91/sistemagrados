/**
 * 
 */
Ext.define('APP.store.GridStore',{
	extend: 'Ext.data.Store',
	config: {
		url: null,
		extraParams: {}
	},
	constructor: function(config) {
		this.initConfig(config);
		Ext.apply(this, {
			autoLoad: true,
			proxy: {
				type: 'ajax',
				url: this.url,
				reader: {
					type: 'json',
					root: 'result.records',
					totalProperty: 'result.totalCount'
				},
				encodeFilters: APP.util.GridUtil.encodeFilters,
				listeners: {
					exception: function(proxy, response, operation) {
						APP.util.AlertMessage.showExceptionMessage('Error', response, true);
					}
				}
			},
			listeners : {
				beforeLoad : function(store,operation,options) {
					store.proxy.extraParams = store.extraParams;
					return true;
				}
			},
			remoteSort: true,
			remoteFilter: true,
		});
//		this.callParent(arguments);
	}
});