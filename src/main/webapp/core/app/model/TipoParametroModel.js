/**
 * 
 */
Ext.define('APP.model.TipoParametroModel',{
	extend: 'Ext.data.Model',
	idProperty: 'consecutivo',
	fields: [{name: 'consecutivo', type: 'numeric'},
	         {name: 'nombre', type: 'string'},
	         {
	        	 name: 'comboName',
	        	 convert: function(value, record) {
	        		 return Ext.String.format('{0} - {1}',
	        				 record.get('consecutivo'), record.get('nombre'));
	        	 }
	         }]
});