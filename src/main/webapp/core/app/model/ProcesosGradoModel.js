/**
 * 
 */
Ext.define('APP.model.ProcesosGradoModel', {
	extend: 'Ext.data.Model',
	idProperty: 'consecutivo',
	fields: [{name: 'consecutivo', type : 'int'}, //type Long
	         {name: 'descripcion', type: 'string'}	       
	]
});