/**
 * 
 */
Ext.define('APP.model.ProgramaEstudianteModel', {
	extend: 'Ext.data.Model',
	idProperty: 'consecutivo',
	fields: [{name: 'nombrePrograma', type : 'string'}, //type Long
	         {name: 'creditosAprobados', type: 'int'},
	         {name: 'creditosRequeridos', type: 'int'},
	         {name: 'flagInscripcion', type: 'bool'},
	         {name: 'id', type: 'string'},
	]
});