/**
 * 
 */
Ext.define('APP.model.ParametroModel', {
	extend: 'Ext.data.Model',
	idProperty: 'consecutivo',
	fields: [{name: 'consecutivo', type : 'int'}, //type Long
	         {name: 'clave', type: 'string'},
	         {name: 'valor', type: 'string'},
	         {name: 'descripcion', type: 'string'},
	         {name: 'ordenamiento', type: 'int'},
	         {name: 'estado', type: 'string'},
	         {name: 'tipoParametro'}, //type Long
	         {name: 'entityTipoParametro'}, 
	         {name: 'tpConsecutivo', mapping: "entityTipoParametro.consecutivo"}, 
	         {name: 'tpNombre', mapping: "entityTipoParametro.nombre"}
	]
});