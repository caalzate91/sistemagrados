/**
 * 
 */
Ext.define('APP.util.AppConstants', {
	statics: {
		ACTA_INTERNA: 1,
		ACTA_EXTERNA: 2,
		SAE_WINDOWS_CREATE : 'CREATE',
		SAE_WINDOW_EDIT : 'EDIT',
		JSON_RESPONSE_STATUS_SUCCESS: 200,
		JSON_RESPONSE_STATUS_ERROR: 500,
		
		/**
		 * Estados del Acta:
		 */
		EA_NO_ENVIADA: 1,
		EA_PENDIENTE_APROBACION: 2,
		EA_CANCELADA: 3,
		EA_NO_APROBADA: 4,
		EA_FINALIZADA: 5,
		
		LOADER_GIF_BAR: '../resources/images/loaders/loader1.gif',
		
		DEFAULT_DATE_FORMAT: 'd/m/Y-H:i:s',
		
		/**
		 * Tipo de Homologacion:
		 */
		HOMOLOGACION: 1,
		RECONOCIMIENTO: 2,
		
		/**
		 * Exception Severities
		 */
		SEVERITY_INFO: "INFORMATION",
		SEVERITY_WARNING: "WARNING",
		SEVERITY_ERROR: "ERROR",
		SEVERITY_FATAL: "FATAL"
	}
});