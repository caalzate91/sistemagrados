/**
 * Jordy Andrés Rodríguez
 * Utilidad para implementar la internacionalización. 
 */
Ext.define('APP.util.TranslateUtil',{
	i18nFile : null,
	config : {
		locale : '',
		bundleFolder : null
	},
	constructor : function(config){
		var me = this;
		this.initConfig(config);
		var localeFile = null;
		var me = this;
		var language;
		if(me.bundleFolder!=null){
			language = navigator.language;
			if(typeof language == "undefined"){
				language = 'es';
			}
			if(language == null || language == ""){
				language = 'es';
			}
			if(language != "es" && language != "en"){
				var en =language.search('en');
				var es = language.search('es');
				if(en != -1){
					language = 'en';
				}else{
					language = 'es';
				}
			}
			me.locale = 'APP.i18n.'+me.bundleFolder+'.msg_'+language;
			//console.log(me.locale);
			//conso
			Ext.require([
			     me.locale
			]);
			me.i18nFile = Ext.create(me.locale);
			
		}
				
		this.callParent(arguments);
	}
});