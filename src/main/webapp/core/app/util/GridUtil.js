/**
 * 
 */
Ext.define('APP.util.GridUtil', {
	statics: {
		setFilters : function(filters, property, value){
			filters.each(function(item, index, len) {
				if(item.property == property) {
					item.value = value;
					return;
				}
			});
		},
		
		buildQueryFilters: function(filters) {
			var p = {}, i, f, root, dataPrefix, key, tmp,
			len = filters.length;

			if (!this.encode){
				for (i = 0; i < len; i++) {
					f = filters[i];
					root = [this.paramPrefix, '[', i, ']'].join('');
					p[root + '[field]'] = f.field;

					dataPrefix = root + '[data]';
					for (key in f.data) {
						p[[dataPrefix, '[', key, ']'].join('')] = f.data[key];
						console.log(p[[dataPrefix, '[', key, ']'].join('')] = f.data[key]);
					}
				}
			} else {
				tmp = [];
				for (i = 0; i < len; i++) {
					f = filters[i];
					if(f.data.type == "string"){
						f.data['comparison'] = 'lq';
					}
					tmp.push(Ext.apply(
							{},
							{property: f.field},
							f.data
					));
				}
				// only build if there is active filter
				if (tmp.length > 0){
					p[this.paramPrefix] = Ext.JSON.encode(tmp);
				}
			}
			
			return p;
		},
		
		encodeFilters: function(filters) {
			var min = [],
			length = filters.length,
			i = 0;

			for (; i < length; i++) {
				min[i] = {
						property: filters[i].property,
						comparison: filters[i].comparison,
						value: filters[i].value
				};
			}
			console.log(Ext.encode(min));
			return Ext.encode(min);			
		}
	}
});
