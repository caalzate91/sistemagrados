/**
 * 
 */
Ext.define('APP.util.ComboBoxUtil', {
	statics: {
		isValid : function(combo, typeObject){
			var value = combo.getValue();
			if(value) {
				var record = combo.getStore().getById(value);
				if(record) {
					return true;
				}
			}
			
			APP.util.AlertMessage.showErrorMessage('Error',
					'Por favor seleccione'+typeObject+'de la lista',
					true);
			combo.reset();
			combo.getStore().loadData([], true);
			return false;
		}
	}
});