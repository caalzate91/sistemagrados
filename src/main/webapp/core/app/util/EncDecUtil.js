/**
 * 
 */
Ext.define('APP.util.EncDecUtil', {
	statics: {
		encodeData: function(data) {
			var ecd = '-1';
			if(data) {
				ecd = '';
				var strData = data.toString();
				for(var i = 0; i < strData.length; i++) {
					ecd += String.fromCharCode((strData.charCodeAt(i) + i));
				}
			}
			return ecd;
		}
	}
});