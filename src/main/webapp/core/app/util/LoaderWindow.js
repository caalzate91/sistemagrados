/**
 * 
 */
Ext.define('APP.util.LoaderWindow', {
	statics: {
		
		showLoaderModal : Ext.create('Ext.window.Window', {
				title: '',
				closable : '',
				id : 'loaderModal',
				resizable : false,
				closeAction :'hide',
				width : 300,
				height : 120,
				modal : '',
				layout : {
					type : 'vbox',
					align: 'center'
				},
				items : [
				    {
				    	xtype : 'container',
				    	items : [{
				    		xtype : 'image',
				    		src : '',
				    		style : {
				    			marginTop : '20px',
				    			backgroundColor : 'white'
				    		}
				    	},
				    	{
				    		xtype : 'label',
				    		text : ''
				    	}
				    	]
				    }
				]
				
				
		}),
		show : function(title, loader, closable, modal, message){
			this.showLoaderModal.setTitle(title);
			this.showLoaderModal.closable = closable;
			this.showLoaderModal.modal = modal;
			this.showLoaderModal.items.items[0].items.items[0].setSrc(loader);
			this.showLoaderModal.items.items[0].items.items[1].setText(message);
			this.showLoaderModal.show();
		},
		hide : function(){
			this.showLoaderModal.hide();
		},
		hideModalWindow : function(id){
			var modal = Ext.getCmp(id);
			modal.hide();
		},
		showSuccessModal : function(title,closable, modal, message){
			Ext.create('Ext.window.Window',{
				title : title,
				width : 400,
				height : 120,
				modal : modal,
				closable : closable,
				closeAction: 'hide',
				resizable : false,
			    region : 'center',
			    draggable : false,
//			    layout: {
//			    	type: 'table',
//			    	columns: 2
//			    },
			    listeners :{
			    	close : function(panel, eOpts){
			    		window.location = '/HomologacionAsignaturas/actas/principal';
			    	}
			    },
			    defaults: {
//			    	xtype: 'container',
//			    	bodyStyle: 'padding:20px !important; text-align: center',
			    	style: {
			    		textAlign: 'center',
			    	},
			    },
			    items :[{
			    	xtype: 'label',
		    		text: message,
			    }]
			}).show();
		}
	}
});