/**
 * 
 */
Ext.define('APP.util.AlertMessage', {
	statics: {
		APROBACION_ACTA_EXITOSA: "La aprobación fue registrada con éxito",
		APROBACION_INGRESAR_COMENTARIO_MSG: 'Debe ingresar un comentario cuando No Aprueba el Acta.',
		
		showMessage: function(title, message, modal, icon) {
			Ext.Msg.show({
				title: title,
				msg: message,
				modal: modal,
				icon: icon,
				closeAction : 'hide'
			});
		},
		
		displayMessage: function(title, message, severity, modal) {
			switch(severity) {
			case APP.util.AppConstants.SEVERITY_ERROR:
				this.showMessage(title, message, modal, Ext.MessageBox.ERROR);
				break;
			case APP.util.AppConstants.SEVERITY_WARNING:
				this.showMessage(title, message, modal, Ext.MessageBox.WARNING);
				break;
			case APP.util.AppConstants.SEVERITY_INFO: 
				this.showMessage(title, message, modal, Ext.MessageBox.INFO);
				break;
//			case APP.util.AppConstants.SEVERITY_FATAL:
//				this.showMessage(title, message, modal, Ext.MessageBox.ERROR);
//				break;
			default:
				this.showMessage(title, message, modal, Ext.MessageBox.ERROR);
			}
		},
		
		showErrorMessage: function(title, message, modal) {
			this.showMessage(title,message,modal,Ext.MessageBox.ERROR);
		},
		
		showInfoMessage: function(title, message, modal) {
			this.showMessage(title,message,modal,Ext.MessageBox.INFO);
		},
		
		showWarningMessage: function(title, message, modal) {
			this.showMessage(title,message,modal,Ext.MessageBox.WARNING);
		},
		
		showExceptionMessage: function(title, response, modal) {
			if(response) {
				if(response.timedout) {
					this.showErrorMessage(title, 'Error en la comunicación con el servidor', modal);
				} else {
					if(response.responseText) {
						var resData = Ext.JSON.decode(response.responseText);
						if(resData) {
							this.displayMessage(title,
									resData.exceptionMessage,
									resData.severity, modal);
						}
					}
				}
			}
		}
	}
});