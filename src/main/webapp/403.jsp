<%@include file="/WEB-INF/views/include.jsp"%>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<title>Acceso Denegado</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/error.css"></c:url>">
</head>
<body>
	<div class="container-error">
		<div class="title title-center">
			<img alt="Error" class="title-img" height="206"  src="<c:url value="/resources/images/error.jpg"></c:url>">
		</div>
		<div class="title title-center">
			<h4>Acceso Denegado</h4>
		</div>
		<div class="message">
			<p>Usted esta tratando de ingresar a una p�gina sobre la<br /> que no
			posee permisos, por favor contacte al administrador.</p>
		</div>
		<div class="footer">
				<a href="javascript:history.back();"><button class="btn btn-blue">Regresar</button></a>
		</div>
	</div>
</body>
</html>