<%@include file="/WEB-INF/views/include.jsp"%>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<title>Acceso Denegado</title>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/logout.css"></c:url>">
</head>
<body>
	<div class="container">
		<div class="title title-center">
<%-- 			<img alt="Error" class="title-img" src="<c:url value="/resources/images/error.png"></c:url>"> --%>
			<img alt="Error" class="title-img" src="http://www.javerianacali.edu.co/sites/ujc/files/img-logoHeader.png">
		</div>
		<div class="title title-center">
			<h1>Sesi�n Finalizada</h1>
		</div>
		<div class="message">
			<p>Muchas Gracias por utilizar los servicios que brinda la <b>Pontificia Universidad Javeriana</b> <br>
			Hasta Pronto!</p>
		</div>
		<!-- <div class="footer">
				<a href="#"><button class="btn btn-yellow">Continuar</button></a>
		</div> -->
	</div>
</body>
</html>