/**
 * Alexander
 */
Ext.onReady(function () {
//	loadStorePrincipal();
	
//	storePrincipal.on('write', function (store, records, operation, eOpts){
//		loadStorePrincipal();
//		Ext.MessageBox.hide();
//		Ext.example.msg(guardadoTitulo, guardadoMensaje);
//	});
	console.log("parametro.js Ready!");
	displayParametrosGrid();
});

/*
 * Stores
 */
var storePrincipal = new Ext.create('Ext.data.Store', {
    fields: ['clave', 'valor', 'descripcion'],
    data: [{"clave":"PARAMETRO_1", "valor":"Parametro 1", "descripcion":"Desc Parametro 1"}, 
           {"clave":"PARAMETRO_2", "valor":"Parametro 2", "descripcion":"Desc Parametro 2"},
           {"clave":"PARAMETRO_3", "valor":"Parametro 3", "descripcion":"Desc Parametro 3"},
           {"clave":"PARAMETRO_4", "valor":"Parametro 4", "descripcion":"Desc Parametro 4"},
           {"clave":"PARAMETRO_5", "valor":"Parametro 5", "descripcion":"Desc Parametro 5"}
    ]//,
//    sorters: {
//    	field: 'clave',
//    	direction: 'DESC'
//    }
});

var gridPrincipal;
function displayParametrosGrid(){
	gridPrincipal = new Ext.create('Ext.grid.Panel',{
		title: 'Parametros', 
		renderTo: 'grid', 
		store: storePrincipal, 
		selType: 'cellmodel', 
		plugins: [Ext.create('Ext.grid.plugin.CellEditing', {clicksToEdit: 1})],
		height: 400,
		width: 800,
		bbar: Ext.create('Ext.PagingToolbar', {store : storePrincipal, displayInfo : true, displayMsg : 'Mostrando registros del {0} al {1} . Total {2}', emptyMsg : "No hay información"}),
		columns: [
		          {
		        	  xtype: 'gridcolumn',
		        	  text: 'Clave', dataIndex: 'clave', width: 50, sortable: true, hideable: false, draggable: false, resizable: true,
		        	  
		          },
		          {
		        	  xtype: 'gridcolumn',
		        	  header: 'Valor', dataIndex: 'valor', width: 250,
		        	  sortable: false, hideable: false, draggable: false, resizable: true		        	  
		          }, 
				  {
		        	  xtype: 'gridcolumn',text: 'Descripcion', dataIndex: 'descripcion', width: 220, 
		        	  sortable: false, hideable: false, draggable: false, resizable: true 
				  }				  
		]}
	);
}

function loadStorePrincipal() {
	
}