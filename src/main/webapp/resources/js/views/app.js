//CONFIGURACIONES

//VALIDACIONES
Ext.apply(Ext.form.field.VTypes, {
	daterange: function(val, field) {
		var date = field.parseDate(val);
		
		if (!date) {return false;}
		
		if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
			var start = field.up('form').down('#' + field.startDateField);
			start.setMaxValue(date);
			start.validate();
			this.dateRangeMax = date;
		}else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
			var end = field.up('form').down('#' + field.endDateField);
			end.setMinValue(date);
			end.validate();
			this.dateRangeMin = date;
        }
        return true;
    },
    daterangeText: 'La Fecha de Inicio debe ser menor que la Fecha Final.',
    
    timerange : function (val, field) {
		var time = field.parseDate(val);
		if (time) {
			if (field.startTimeField && (!field.timeRangeMax || (time.getTime() != field.timeRangeMax.getTime()))) {
				var start = Ext.getCmp(field.startTimeField);
				if (start) {
					start.setMaxValue(time);
					field.timeRangeMax = time;
					start.validate();
				}
			} else if (field.endTimeField && (!field.timeRangeMin || (time.getTime() != field.timeRangeMin.getTime()))) {
				var end = Ext.getCmp(field.endTimeField);
				if (end) {
					end.setMinValue(time);
					field.timeRangeMin = time;
					end.validate();
				}
			}
		}
		return true;
	}, 
    
    password: function(val, field) {
        if (field.initialPassField) {
            var pwd = field.up('form').down('#' + field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },
    passwordText: 'Las Contraseñas no coinciden.'
});

/*
 * CONSTATNTES
 */
var ACTA_INTERNA_CREAR = "AI_CREATE";
var ACTA_INTERNA_EDITAR = "AI_EDIT";

//VARIABLES
var errorCurrentStore;
var evaluacionWin;
var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
var lblRequiredBefore = '<span>(*)</span>';
var guardandoMensaje = 'Guardando cambios...';
var guardandoTitulo = 'Por favor Espere';
var guardadoMensaje = '¡Los cambios fueron guardados!';
var guardadoTitulo = '¡Hecho!';
var borrarMensaje = '¿Está seguro(a) que desea eliminar?</br>Los datos se borrarán al guardar los cambios.';
var borrarTitulo = 'Confirmar Eliminación';
var eliminandoTitulo = 'Por favor Espere';
var eliminandoMensaje = '¡Eliminación completa!';
var errorTitulo = 'Error';
var errorMensaje = 'Error en la aplicación';
var alertaAsignaturaTitulo = 'Alerta';
var alertaAsignaturaMensaje = 'Por favor seleccione otra asignatura. Ya existe ';


// FUNCIONES
// Funciones Globales
function disableRenderer(val) {
	return '<span style="color:gray;">' + val + '</span>';
}

function campoObligatorio() {
	return '<span style="color:red" data-qtip="Requerido">&nbsp;*</span>';
}

function disableRendererMoney(val) {
	return '<span style="color:gray;">' + rendererMoney(val) + '</span>';
}

function rendererMoney(v) {
	return Ext.util.Format.currency(v, '$', 0);
}

function rendererSummary(v) {
	return '<span style="color:blue;">' + v + '</span>';
}

function rendererMoneySummary(v) {
	return '<span style="color:blue;">' + rendererMoney(v) + '</span>';
}

function renderTextArea(valor) {
	return '<textarea style="width: 100%;color:gray;background:WhiteSmoke" readonly class="x-form-textarea x-form-field">'
			+ valor + '</textarea>';
}

function rendererSummaryCount(v) {
	return '<span style="color:blue;"> Total: ' + v + '</span>';
}

function renderEstado(estadoFormulario) {
	if (estadoFormulario == 1) {
		return '<span style="color:green">ABIERTO</span>';
	} else {
		return '<span style="color:red">APROBADO</span>';
	}
}

function Cleanup() {
    window.clearInterval(idTmr);
    CollectGarbage();
};

function functionMaker(propertyName, objRef) {
	var x;
	// if we are dealing with an array, recursively iterate
	if (objRef instanceof Array) {
		for (i = 0; i < objRef.length; i++) {
			functionMaker(propertyName, objRef[i]);
		}
	}
	for (x in objRef) {
		if ((x != undefined) && (x == propertyName)) {
			objRef[x] = eval(objRef[x]); // fixed ya!
		} else if (objRef[x] instanceof Array) { // if we are dealing with an
			// array, recursively
			// iterate
			for (i = 0; i < objRef[x].length; i++) {
				functionMaker(propertyName, objRef[x][i]);
			}
		}
	}
}

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "dd/mm/yyyy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab",
		"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"
	],
	monthNames: [
		"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic",
		"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

function showErrorMessage(store, response) {
	errorCurrentStore = store;
	Ext.MessageBox.hide();
	var responseValues = JSON.parse(response.responseText);
	Ext.MessageBox.show({
		title : 'Error Guardando la información',
		msg : '<b>' + responseValues.values[0] + '</b>  <br><br>'
				+ responseValues.values[1] + '<br><br><b>'
				+ "¿Desea deshacer los cambios?" + '<b></br>',
		buttons : Ext.MessageBox.YESNO,
		fn : errorFunction,
		icon : Ext.MessageBox.ERROR
	});
}

function showError(response) {
	var responseValues = JSON.parse(response.responseText);
	Ext.MessageBox.show({
		title : 'Error en la aplicación',
		msg : responseValues.values,
		buttons : Ext.MessageBox.OK,
		fn : errorFunction,
		icon : Ext.MessageBox.ERROR
	});
}

function errorFunction(btn) {
	if (btn == 'yes') {
		errorCurrentStore.rejectChanges();
	}
}

var formRetroalimentacion;
function mostrarFormularioRetroalimentacion() {
	var tipoComentario = Ext.create('Ext.data.Store', {
		fields : [ 'valor', 'nombre' ],
		data : [ {
			"valor" : "Felicitación",
			"nombre" : "Felicitación"
		}, {
			"valor" : "Sugerencia",
			"nombre" : "Sugerencia"
		}, {
			"valor" : "Consulta",
			"nombre" : "Consulta"
		}, {
			"valor" : "Queja",
			"nombre" : "Queja"
		}, {
			"valor" : "Reclamo",
			"nombre" : "Reclamo"
		}]
	});

	var listaFormularios = new Ext.data.Store({
		storeId : 'listaFormularios', 
		autoLoad : true, 
		model : 'VMUFormulario', 
		proxy : {
			type : 'ajax', 
			url : '/MedioUniversitarioPUJ/programacion/listaFormularios'
		}
	});

	if (!evaluacionWin) {
		formRetroalimentacion = Ext.widget('form', {
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			url : '/MedioUniversitarioPUJ/retroalimentacion/addRetroalimentacion',
			border : false,
			bodyPadding : 10,
			fieldDefaults : {
				labelAlign : 'left',
				labelWidth : 120,
				labelStyle : 'font-weight:bold',
				margin : '0 0 20 0'
			},
			items : [ {
				xtype : 'combobox',
				fieldLabel : 'Formulario',
				store : listaFormularios,
				afterLabelTextTpl : required,
				queryMode : 'local',
				name : 'id_formulario',
				valueField : 'codigo',
				displayField : 'descripcion',
				editable: false, 
			    emptyText: '...', 
			    allowBlank: false
			}, {
				xtype : 'combobox',
				fieldLabel : 'Tipo Comentario',
				store : tipoComentario,
				afterLabelTextTpl : required,
				queryMode : 'local',
				name : 'tipo_comentario',
				valueField : 'valor',
				displayField : 'nombre',
				editable: false, 
			    emptyText: '...', 
			    allowBlank: false
			}, {
				xtype : 'textareafield',
				fieldLabel : 'Comentario',
				name : 'comentario',
				flex : 1,
				margins : '0',
				afterLabelTextTpl : required,
				allowBlank : false
			} ],
			buttons : [ {
				text : 'Aceptar',
				handler : function() {
					if (this.up('form').getForm().isValid()) {
						Ext.MessageBox.show({title: 'Por favor espere', msg: 'Procesando...', width: 300, progress: true, closable: false});
						this.up('form').getForm().submit({
							success : function(response) {
								Ext.MessageBox.hide();
								formRetroalimentacion.getForm().reset();
								Ext.example.msg(guardadoTitulo, guardadoMensaje);
								evaluacionWin.hide();
							}
						});
					}
				}
			}, {
				text : 'Cancelar',
				handler : function() {
					formRetroalimentacion.getForm().reset();
					evaluacionWin.hide();
				}
			} ]
		});

		evaluacionWin = Ext.widget('window', {
			title : 'Medio Universitario > Retroalimentación', 
			closeAction : 'hide', 
			width : 500, height : 400,  
			x: 500, y: 250, 
			layout : 'fit', 
			resizable : true, 
			modal : true, 
			items : formRetroalimentacion
		});
	}
	evaluacionWin.show();
}

var mainPanel;
function displayUtilPanel() {
	mainPanel = Ext.widget('panel',{
		renderTo : 'utils-form', 
		cls : 'ribbonPanel',
		header : false,
		width : 115,
		bodyPadding : 0,
		items : [ {
			xtype : 'button',
			width : 115,
			text : 'Cuéntenos su <br> <span style="font-weight:bold; color:#0061A5;">Experiencia</span>',
			handler : mostrarFormularioRetroalimentacion
		} ]
	});
}

// Metodo para consultar y refrescar los parametros
function refrescarParametros() {
	Ext.Ajax.request({
		url : '/Presupuesto/indicadores',
		success : function(response) {
			var jresponse = JSON.parse(response.responseText);

			if (jresponse.values != null) {
				var indicador1 = jresponse.values.ingreso;
				var indicador2 = jresponse.values.gastos;
				var indicador3 = jresponse.values.inversion;

				$("#indicador1").text(rendererMoney(indicador1));
				$("#indicador2").text(rendererMoney(indicador2));
				$("#indicador3").text(rendererMoney(indicador3));
				$("#indicador4").text(rendererMoney(indicador1 - indicador2));
			}
		}
	});
};

// Para traducir mensajes
function translateMessage(message) {
	if (message == "must be present") {
		return "Debe ingresar un valor";
	}
	return message;
}

// Metodo para validar un Grid
function validateEditorGridPanel(grid) {
	store = grid.getStore();
	view = grid.getView();
	error = false;
	columnLength = grid.columns.length;

	store.each(function(record, idx) {
		for ( var i = 0; i < columnLength; i++) {
			cell = view.getCellByPosition({row : idx, column : i});
			cell.removeCls("x-form-invalid-field");
			cell.set({'data-errorqtip' : ''});
			fieldName = grid.columns[i].dataIndex;
			if (!record.dirty && !record.phantom) {break;}
			var errors = record.validate();
			var fieldErrors = errors.getByField(fieldName);
			if (fieldErrors.length > 0) {
				cell.addCls("x-form-invalid-field");
				cell.set({'data-errorqtip' : translateMessage(fieldErrors[0].message)});
				error = true;
			}
		}
	});
	return error;
}

// Metodo para validar el grid con funciones personalizadas
function validateCustomEditorGridPanel(grid, customValidate) {
	store = grid.getStore();
	view = grid.getView();
	error = false;
	columnLength = grid.columns.length;

	store.each(function(record, idx) {
		for ( var i = 0; i < columnLength; i++) {
			cell = view.getCellByPosition({row : idx, column : i});
			cell.removeCls("x-form-invalid-field");
			cell.set({'data-errorqtip' : ''});
			fieldName = grid.columns[i].dataIndex;
			if (!record.dirty && !record.phantom) {break;}
			var errors = record.validate();
			var fieldErrors = errors.getByField(fieldName);
			if (fieldErrors.length > 0) {
				cell.addCls("x-form-invalid-field");
				cell.set({'data-errorqtip' : translateMessage(fieldErrors[0].message)});
				error = true;
			}else {
				var customfieldErrors = customValidate(record, fieldName);
				if (customfieldErrors.length > 0) {
					cell.addCls("x-form-invalid-field");
					cell.set({'data-errorqtip' : customfieldErrors[0]});
					error = true;
				}
			}
		}
	});
	return error;
}

var storeParametros;
var gridParametros;
function displayParametrosGrid(urlStore, cantidadParametros, divName) {
	var height;
	if (divName == null) {
		divName = 'parametros';
	}

	if (cantidadParametros != null) {
		height = cantidadParametros * 22;
	} else {
		height = 100;
	}

	storeParametros = new Ext.data.Store({
		storeId : 'storeParametros',
		autoLoad : true,
		model : 'Parametro',
		proxy : {
			type : 'ajax',
			url : urlStore,
			listeners : {
				exception : function(proxy, response, operation) {
					showErrorMessage(storeParametros, response);
				}
			}
		}
	});

	gridParametros = Ext.create('Ext.grid.Panel', {
		header : false,
		hideHeaders : true,
		disabled : true,
		disabledCls : 'disabledGrid',
		title : '> Par�metros',
		renderTo : divName,
		store : storeParametros,
		selType : 'cellmodel',
		height : height,
		width : '100%',
		columns : [ {
			header : 'Parametro',
			dataIndex : 'nombre'
		}, {
			header : 'Valor',
			dataIndex : 'value',
			flex : 1
		} ]
	});
}

// Function para ir a pagina principal
function goHome() {
	var newLocation = location.protocol + '//' + location.host + '/' + 'MedioUniversitarioPUJ';
	window.location = newLocation;
}

// Metodo que Pinta el Menu
function pintarMenu() {
	Ext.Ajax.request({
		url : '/MedioUniversitarioPUJ/activeTab',
		success : function(response) {
			var jresponse = JSON.parse(response.responseText);

			if (jresponse.values != null) {
				if (jresponse.values == 2) {
					$('#ribbon-tab-header-0').removeClass('sel');
					$('#formularios-tab').css('display', 'none');

					$('#ribbon-tab-header-1').addClass('sel');
					$('#aprobaciones-tab').css('display', 'block');
				}else {
					$('#ribbon-tab-header-1').removeClass('sel');
					$('#aprobaciones-tab').css('display', 'none');

					$('#ribbon-tab-header-0').addClass('sel');
					$('#formularios-tab').css('display', 'block');

				}
			}
		}
	});
	
	//Muestra el boton de Retroalimentacion
	displayUtilPanel();
}

// PopUp Con contenido
var popUpWin;
var navegador;
var rv;
function displayPopUp(item) {
	rv = -1; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}
	if (item != null) {
		if (popUpWin != null) {

			if (popUpWin.isVisible()) {
				popUpWin.hide();
			}

			else {
				popUpWin.destroy();
                                var newItem;
                                newItem = item.cloneConfig();
                                if( navegador ){
                                    popUpWin = Ext.widget('window', {
                                        closeAction : function() {
                                        },
                                        width : '95%',
                                        height : '95%',
                                        layout : 'fit',
                                        resizable : true,
                                        modal : true,
                                        items : newItem
                                    });
                                }else{
                                    popUpWin = Ext.widget('window', {
                                        closeAction : function() {
                                        },
                                        width : (document.documentElement.offsetWidth * 90 / 100),
                                        height : (document.documentElement.offsetHeight * 90 / 100),
                                        layout : 'fit',
                                        resizable : true,
                                        modal : true,
                                        items : newItem
                                    });
                                }
				popUpWin.show();
			}
		}

		else {
			var newItem;
			newItem = item.cloneConfig();
			if( navegador ){
                                    popUpWin = Ext.widget('window', {
                                        closeAction : function() {
                                        },
                                        width : '95%',
                                        height : '95%',
                                        layout : 'fit',
                                        resizable : true,
                                        modal : true,
                                        items : newItem
                                    });
                                }else{
                                    popUpWin = Ext.widget('window', {
                                        closeAction : function() {
                                        },
                                        width : (document.documentElement.offsetWidth * 95 / 100),
                                        height : (document.documentElement.offsetHeight * 95 / 100),
                                        layout : 'fit',
                                        resizable : true,
                                        modal : true,
                                        items : newItem
                                    });
                                }

			popUpWin.show();

		}
	}
}

function getInternetExplorerVersion()
// Returns the version of Windows Internet Explorer or a -1
// (indicating the use of another browser).
{
	var rv = -1; // Return value assumes failure.
	if (navigator.appName == 'Microsoft Internet Explorer') {
		var ua = navigator.userAgent;
		var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		if (re.exec(ua) != null)
			rv = parseFloat(RegExp.$1);
	}
	
	
	
	return rv;
}
function checkIEVersion() {
	var ver = getInternetExplorerVersion();
	if (ver > -1) {
		if (ver < 8.0) {
                        navegador = false;
			Ext.MessageBox
					.show({
						title : 'Navegador no soportado',
						msg : '<b>'
								+ navigator.appName
								+ ver
								+ '.</b> </br></br>'
								+ 'Para garantizar un �ptimo funcionamiento del sistema recomendamos actualizar la versi�n de su navegador.'
								+ '<br><br> <a class="file-link" href="https://www.google.com/chrome?hl=es" target="_blank">Google Chrome</a>'
								,
						buttons : Ext.MessageBox.OK
					});
                            
		}else if( ver < 9.0){
                    navegador = false;
                }else{
                    navegador = true;
                }
	}
}


